<?php 
//error_reporting(-1);
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class VoucherCredit extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('voucher_model');
		$this->load->helper('url');
		$this->load->library('../controllers/Mailsend');
	}


	public function voucher()
	{ 
		include (FCPATH."Mpdf/mpdf.php") ;
		$mpdf = new mPDF('c',    // mode - default ''
						'L',    // format - A4, for example, default ''
						12,     // font size - default 0
						'',    // default font family
						5,    // margin_left
						5,    // margin right
						5,     // margin top
						0,    // margin bottom
						0,     // margin header
						0,     // margin footer
						'L');  // L - landscape, P - portrait 
		//pr_repo($_REQUEST);die('adfds');
		if(isset($_REQUEST['debug']) && $_REQUEST['debug'] == 1)
		{
			$data['user_id']= $user_id = $_REQUEST['user_id'];
			$data['v_credit']= $_REQUEST['v_credit']; 
		}
		else
		{
			$data['user_id']=$user_id= $this->input->post('user_id');
			$data['v_credit']= $this->input->post('v_credit');  
		}
		//pr_repo($data);die;
		$data['date']= date('Y-m-d');
		$insert = $this->voucher_model->insertVoucher($data);
		$userdetail=$this->voucher_model->voucherGetdata($user_id);
		
		$email=$userdetail->email;
		$pdf_file_name = $user_id."FeelGood-Voucher.pdf";
		if($insert!='')
		{  
			$mpdf->list_indent_first_level = 0;
			$mpdf->SetHTMLFooter('<div style="text-align:center;"><img style="width:60%;" src="image/footerimage.png"/></div>');
			$mpdf->WriteHTML($this->load->view('voucher',$data,TRUE));	
			$mpdf->Output(FCPATH."/pdfs/".$pdf_file_name, 'F');  
			
			$baseurl = base_url();
			$link = $baseurl."pdfs/".$pdf_file_name;
		
			$mailData['mail_keys']['{RECIVER_NAME}'] = trim($userdetail->first_name);
			$mailData['mail_keys']['{DOWNLOAD_LINK}'] = $link;
			$mailData['company_id'] = $userdetail->associated_company_id;
			$to = $userdetail->email;
			
			if($this->mailsend->sendMail($to,$mailData,'voucher_credit_pdf'))
			{
				$msg="Report has been sent";
				$status=1;
			}
			else
			{
				$msg= "Report not sent.";
			    $status=0;
			}
		}
		else
		{
				$msg= "Report not sent.";
				$status=0;
		}
		echo json_encode(array('message'=>$msg,'status'=>$status));
	}
			
			public function saveCredits()
			{
				
				$data['move_val'] = $this->input->post('move_val');
				$data['eat_val'] = $this->input->post('eat_val');
				$data['mind_val'] = $this->input->post('mind_val');
				$data['total_val'] = $this->input->post('total_val');
				$data['total_credits'] = $this->input->post('total_credits');
				
				/* if(!empty($this->input->post('move_val')) {
					$data['move_val'] = $this->input->post('move_val');
				}
				if(!empty($this->input->post('eat_val'))){
					$data['eat_val'] = $this->input->post('eat_val');
				}
				if(!empty($this->input->post('mind_val'))){
					$data['mind_val'] = $this->input->post('mind_val');
				}
				if(!empty($this->input->post('total_val'))){
					$data['total_val'] = $this->input->post('total_val');
				}
				if(!empty($this->input->post('total_credits'))){
				$data['total_credits'] = $this->input->post('total_credits');
				} */
				
				$data['test_date'] = $test_date =  date('Y-m-d');
				 $previous_date= date('Y-m-d', strtotime('-3 month', strtotime($test_date)));
			
				$user_id = $this->input->post('user_id');
				
				
				
				$record = $this->voucher_model->get_save_credits($user_id);
				$record_date = $record->test_date;
				 $record_id=$record->id; 
			 if(!empty($record))
				{
				
				/*if($previous_date<=$record_date)
				{
					$up['move_val'] = $this->input->post('move_val');
				$up['eat_val'] = $this->input->post('eat_val');
				$up['mind_val'] = $this->input->post('mind_val');
				$up['total_val'] = $this->input->post('total_val');
				$up['total_credits'] = $this->input->post('total_credits');
				$up['test_date'] = $test_date =  date('Y-m-d');
					
					$affected = $this->voucher_model->update_credit($up,$record_id);
						//echo $affected; 
						
					
						$msg = "crdeits updated";
						$status = 1;
						
				
				}
				else{*/
					
						
					$data['user_id'] = $user_id;
					$data['type'] = 'followup';
					if($data['move_val']>0 && $data['eat_val']>0 && $data['mind_val'] && $data['total_val'] && $data['total_credits']){
					$ins_id = $this->voucher_model->save_credit($data);
					}
					if($ins_id!='')
					{
						$msg = "crdeits saved";
						$status = 1;
					}
					else{
						$msg = "crdeits not saved";
						$status = 0;
					}
							
				//}
		}
		else{
				
					$data['user_id'] = $user_id;
					$data['type'] = 'first_consult';
					if($data['move_val']>0 && $data['eat_val']>0 && $data['mind_val'] && $data['total_val'] && $data['total_credits']){
					$ins_id = $this->voucher_model->save_credit($data);
					}
		}
				
				echo json_encode(array('message' => $msg,'status' => $status));
			}
			
			public function get_points_per_activity(){
				
				$user_id = $this->input->post('userId');
				$time=$this->input->post('ttime');
				$f_level=$this->input->post('fitness_level');
				$activityid=$this->input->post('activityid');
				$points_rate=$this->voucher_model->get_points($user_id,$f_level,$activityid);
				$points_per_activity = ($points_rate/10)*$time;
				echo json_encode(array('point'=>$points_per_activity,'status'=>1));
			}
			
			
			
			
			
			
			
			
 }
 ?>
