<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$rootParentFolder = realpath(__DIR__ . '/../../../');
$movesmartConfigFilePath = $rootParentFolder.'/backoffice/service/config.php';
//echo $movesmartConfigFilePath;die;
include($movesmartConfigFilePath);
class Mailsend  {
	
	public $CI;
	public function __construct()
	{
		$this->CI =& get_instance();
	}

	public function sendMail($to,$params,$slug)
	{
		$this->CI->load->model('mail_send_model');
		$data = $this->CI->mail_send_model->getMailTemplate($slug);
		$res = 1;
		if($data)
		{
			$content = $data->content;
			$subject = $data->subject;
			
			$constants = $this->getConstants();
			
			$params['mail_keys']['{TOP_LOGO}'] = $constants['base_url'].'images/'.$constants['top_logo'];
			$params['mail_keys']['{BOTTOM_LOGO}'] = $constants['base_url'].'images/'.$constants['bottom_logo'];
			$params['mail_keys']['{COMPANY_PHONE_NUMBER}'] = $constants['company_phone_number'];
			$params['mail_keys']['{COMPANY_NAME}'] = $constants['company_name'];
			$params['mail_keys']['{COMPANY_ADDRESS}'] = $constants['company_address'];	
			
			if($params['company_id'] != 0)
			{
				$companyData = $this->CI->mail_send_model->getCompanyData($params['company_id']);
				if(!empty($companyData))
				{
					$params['mail_keys']['{TOP_LOGO}'] = $constants['company_theme_folder_path'].$params['company_id'].'/thumb/'.$companyData->logo;
					$params['mail_keys']['{BOTTOM_LOGO}'] = $constants['company_theme_folder_path'].$params['company_id'].'/thumb/'.$companyData->logo;
					$params['mail_keys']['{COMPANY_PHONE_NUMBER}'] = $companyData->company_phone_number;
					$params['mail_keys']['{COMPANY_NAME}'] = $companyData->company_name;
					$params['mail_keys']['{COMPANY_ADDRESS}'] = $companyData->company_address;
				}
			}
			$params['mail_keys']['{SUPPORT_MAIL}'] = $constants['company_support_mail'];
			$params['mail_keys']['{COMPANY_SITE_URL}'] = $constants['company_site'];
			foreach ($params['mail_keys'] as $key => $value) 				
			{						
				$keys[]   = $key;							
				$values[] = $value;				
			}	
			
			$topcontent = "<div style='width:100%'><img src='{TOP_LOGO}' alt='Logo'/></div><br/><br/>";
			$bottomcontent = '<br/><br/><div style="width:100%"><img alt="{BOTTOM_LOGO}" src="{BOTTOM_LOGO}" /></div><br/><div style="width:100%">{COMPANY_ADDRESS}</div><div style="width:100%">M = <a href="http://{COMPANY_SITE_URL}">{COMPANY_SITE_URL}</a></div>';
			
			$finalContent = $topcontent.$content.$bottomcontent;
			$msg = str_replace($keys,$values,$finalContent);	
			
			$this->CI->load->library('PHPMailer');

			$this->CI->phpmailer->SMTPDebug = true;  // debugging: 1 = errors and messages, 2 = messages only
			$this->CI->phpmailer->SMTPAuth = true;  // authentication enabled
			
			$this->CI->phpmailer->Host = $constants['smtp_host'];
			$this->CI->phpmailer->Port = $constants['smtp_port'];
			$this->CI->phpmailer->Username = $constants['smtp_username'];
			$this->CI->phpmailer->Password = $constants['smtp_password']; 
			$this->CI->phpmailer->setFrom($constants['from_mail']);
			$this->CI->phpmailer->FromName = $params['mail_keys']['{COMPANY_NAME}'];
			$this->CI->phpmailer->isHTML(true);
			$this->CI->phpmailer->addAddress($to);
			if($constants['developer_bcc'])
			{
				$this->CI->phpmailer->addBCC($constants['developer_mail']);
			}
			$this->CI->phpmailer->Subject = $subject;
			$this->CI->phpmailer->msgHTML($msg);
			if (!$this->CI->phpmailer->send()) 
			{
				$res = 0;
			}
		}
		else
		{
			$res = 0;
		}
		return $res;
	}
	
	public function getConstants()
	{
		
		$constants['company_name']	= MOVESMART_COMPANY_NAME;
        $constants['base_url']		= SERVICE_BASEURL;
        $constants['top_logo']		= MOVESMART_TOP_LOGO;
		$constants['bottom_logo']	= MOVESMART_BOTTOM_LOGO;
		$constants['company_theme_folder_path'] = UPLOAD_COMPANY_THEME_OPTION_FOLDER;
		$constants['company_address'] = MOVESMART_ADDRESS;
		$constants['company_phone_number'] = MOVESMART_PHONENUMBER;
		$constants['company_support_mail'] = MOVESMART_SUPPORTEMAIL;
		$constants['company_site'] = MOVESMART_SITE;
		$constants['smtp_host'] = SMTP_HOST;
		$constants['smtp_port'] = SMTP_PORT;
		$constants['smtp_username'] = SMTP_USERNAME;
		$constants['smtp_password'] = SMTP_PASSWORD;
		$constants['from_mail'] = FROM_MAIL;
		$constants['developer_bcc'] = ADD_DEVELOPER_BCC;
		$constants['developer_mail'] = DEVELOPER_MAIL;
		return $constants;
	}
	
}
