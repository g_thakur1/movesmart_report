<?php
//require 'PHPMailerAutoload.php';
   
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
//error_reporting(E_ALL);   
  ini_set('memory_limit', '1024M');
 
 //$path= realpath ( string "./Mpdf/mpdf.php" );
//include($path); 
class Reportpdf extends CI_Controller {

	function __construct()  
    {
		parent::__construct();
        $this->load->helper('url');
        $this->load->model('report_model');
		$this->load->model('report_pdf_model');
		$this->load->model('voucher_model');
		$this->load->library('../controllers/Mailsend');
    }

    private function checkFile($file, $defaultFile){
    	if(file_exists(FCPATH. $file)) {
        	return $file;
        } else {
        	return $defaultFile;
        }
    }

	public function index()
	{
		include (FCPATH."Mpdf/mpdf.php");
		$mpdf = new mPDF('c',    // mode - default ''
					array(300,250),    // format - A4, for example, default ''
					0,     // font size - default 0
					'',    // default font family
					17,    // margin_left
					17,    // margin right
					10,     // margin top
					0,    // margin bottom
					10,     // margin header
					9,     // margin footer
					'L');  // L - landscape, P - portrait 
	   
		$data = array();
		if(isset($_REQUEST['debug']) && $_REQUEST['debug'] == 1)
		{
			$user_id = $_REQUEST['user_id'];
		}
		else
		{
			$user_id = $this->input->post('user_id');
		}
		
		$data['latest_test_date']= $latest_test_date =  $this->report_pdf_model->latest_test_date($user_id);
		$data['user_detail']=$user_detail = $this->report_pdf_model->user_detail($user_id);
		$gender = $user_detail->gender;
		//for credits per activity//
		
		$age_weight_on_test = $this->report_pdf_model->user_age($user_id);
		$age_on_test=$age_weight_on_test->age_on_test; //age of user
		$weight_on_test=$age_weight_on_test->weight_on_test; //weight of user
		$fitness= $this->report_model->get_fitness($user_id);
		
		$data['target_credits'] = $target =  $this->report_pdf_model->target_credits($user_id);
		$point_speed = $this->report_pdf_model->get_point_speed($age_on_test,$fitness,$gender);
		
	
		$data['hr_run'] =$this->report_pdf_model->get_heart_range($user_id,'Running'); 
		$data['hr_cycle'] =$this->report_pdf_model->get_heart_range($user_id,'Cycling / MTB');
		$data['hr_swim'] =$this->report_pdf_model->get_heart_range($user_id,'Swimming');
		$data['hr_walk'] =$this->report_pdf_model->get_heart_range($user_id,'Walking');
		$data['hr_row'] =$this->report_pdf_model->get_heart_range($user_id,'Rowing');
	
		//for credit score//
		
		$user_email = $user_detail->email;
		$club_id=$user_detail->r_club_id;
		$data['club']= $club = $this->report_pdf_model->club_detail($club_id);

		$club_name= $club->club_name;
		$clubs = explode('',$club_name);
		$coach_id=$this->report_pdf_model->get_coach_id($user_id);
		$data['coach_detail'] = $this->report_pdf_model->user_detail($coach_id);
		$data['coach_image'] = $this->report_pdf_model->coach_image($coach_id);
		$goal= $this->report_pdf_model->client_goal($user_id);
		
		/*****for target text**/
		$target_text = 	array('1'=>'Ik wil fit worden',
							'2'=>'Ik wil afvallen en/of mijn buikomvang verminderen',
							'3'=>'Ik wil meer energie',
							'4'=>'Ik wil vitaal worden'
						);
		$data['goal'] = $target_text[$goal];	
		/*end*/						
		$data['iand_70'] =$iand_70 = round($target['iant_p']*70/100);
		$data['iand_80'] =$iand_80 = round($target['iant_p']*80/100);
		$data['iand_90'] =$iand_90 = round($target['iant_p']*90/100);
		$data['iand_97'] =$iand_97 = round($target['iant_p']*97/100);
		
		$data['cycle_heart_zone_70'] = round($target['iant_hr']*70/100);
		$data['cycle_heart_zone_80'] = round($target['iant_hr']*80/100);
		$data['cycle_heart_zone_90'] = round($target['iant_hr']*90/100);
		$data['cycle_heart_zone_97'] = round($target['iant_hr']*97/100);
	  
		$test_type = $this->report_pdf_model->user_test_type($user_id);
		
		if($test_type > 1)
		{
			$data['user_test_type']=	$user_test_type = "FollowUp";
			if($clubs[0]=='FeelGoodClub')
			{
				$pdf_file_name = $user_id."FeelGood-Consult-eerste-consult-LEEG.pdf";
			}
			elseif(strpos(strtolower($club_name),'vitality') == true)
			{
				$pdf_file_name = $user_id."MyVitality-Consult-eerste-consult-LEEG.pdf";
			}
			else
			{
				$pdf_file_name = $user_id."Movesmart-Consult-eerste-consult-LEEG.pdf";
			}
		}
		else
		{
			$data['user_test_type'] =	$user_test_type = "First";
			if($clubs[0]=='FeelGoodClub')
			{
				$pdf_file_name = $user_id."FeelGood-Consult-vervolg-consult-LEEG.pdf";
			}
			elseif(strpos(strtolower($club_name),'vitality') == true)
			{
				$pdf_file_name = $user_id."MyVitality-Consult-vervolg-consult-LEEG.pdf";
			}
			else
			{
				$pdf_file_name = $user_id."Movesmart-Consult-vervolg-consult-LEEG.pdf";
			}
		
		}

		// Global common variables for PDF starts
		$data['dynamic']['circle1']   		= base_url(). 'images/imagecircle1.png';
        $data['dynamic']['circle2']   		= base_url(). 'images/imagecircle2.png';
        $data['dynamic']['circle3']   		= base_url(). 'images/imagecircle3.png';
        $data['dynamic']['circle4']   		= base_url(). 'images/imagecircle4.png';
        $data['dynamic']['circle5']   		= base_url(). 'images/imagecircle5.png';
        $data['dynamic']['circle6']   		= base_url(). 'images/imagecircle6.png';
        $data['dynamic']['circle7']   		= base_url(). 'images/imagecircle7.png';
        $data['dynamic']['bottomcircle']  	= base_url(). 'images/bottomcircle.png';
        $data['dynamic']['bottomcircle1']  	= base_url(). 'images/bottomcircle1.png';
        $data['dynamic']['bottomcircle2']  	= base_url(). 'images/bottomcircle2.png';
        $data['dynamic']['bottomcircle3']  	= base_url(). 'images/bottomcircle3.png';
        $data['dynamic']['bottomcircle4']  	= base_url(). 'images/bottomcircle4.png';
        $data['dynamic']['default_coach']   = base_url(). 'images/coach.png';

		
		$movesmartConstants = $this->mailsend->getConstants();
		//pr_repo($movesmartConstants);die;
		$companyName 						= $movesmartConstants['company_name'];
        $companyLogo 						= $movesmartConstants['base_url'].'images/'.$movesmartConstants['top_logo'];
		$companyInnerLogo 					= $movesmartConstants['base_url'].'images/'.$movesmartConstants['bottom_logo'];
		$companyBanner 						= base_url(). $this->checkFile('images/banner.png', 'images/banner.png');
		
		$this->load->model('mail_send_model');
		$companyData = $this->mail_send_model->getCompanyData($user_detail->associated_company_id);
		//pr($companyData);die;
		if(!empty($companyData))
		{
			$companyName = $companyData->company_name;
			$companyLogo = $movesmartConstants['company_theme_folder_path'].$user_detail->associated_company_id.'/thumb/'.$companyData->logo;
			$companyInnerLogo = $movesmartConstants['company_theme_folder_path'].$user_detail->associated_company_id.'/thumb/'.$companyData->logo;
			$companyBanner 	= base_url(). $this->checkFile('images/banner.png', 'images/banner.png');
		}
		
        // Company movesmart, eatfresh and headswitch
		$headSmart							= base_url(). $this->checkFile('images/-1_movesmart.png', 'images/movesmart.png');
		$headFresh							= base_url(). $this->checkFile('images/-1_eatfresh.png', 'images/eatfresh.png');
		$headSwitch							= base_url(). $this->checkFile('images/-1_mindswitch.png', 'images/mindswitch.png');


        $companyColor 		= '#0095ac';
		$homeBgColor 		= 'rgb(0, 149, 172)';
		$homeBoxColor 		= '#68C4E3';
		$homePColor 		= '#0095ac;';
		$homeH2Color 		= '#68C4E3';


		$data['dynamic']['company_name']   	= $companyName;
		$data['dynamic']['company_color']	= $companyColor;
		$data['dynamic']['logo_image']     	= $companyLogo;
        $data['dynamic']['inner_logo']   	= $companyInnerLogo;
        $data['dynamic']['banner_image']	= $companyBanner;

        $data['dynamic']['move_smart']		= $headSmart;
		$data['dynamic']['fresh']			= $headFresh;
		$data['dynamic']['mindswitch'] 		= $headSwitch;

        $data['dynamic']['bg_color']       	= $homeBgColor;
        $data['dynamic']['box_color']      	= $homeBoxColor;
        $data['dynamic']['box_p_color']    	= $homePColor;
        $data['dynamic']['box_h2_color']   	= $homeH2Color;
        // Global common variables for PDF ends here


		if($user_test_type == "First")      
		{
			$data['movesmart'] 	= $this->move_level($user_id);
		   	$data['eat'] 		= $eat = $this->eat_level($user_id);

			$find=array("{bmi_val}","{bmi}","{waist_val}","{waist}","{fat_val}","{fat_percentage}");
			$replace=array($eat['bmi'],$eat['b_level'],$eat['waist'],$eat['waist_level'],$eat['fat_percentage'],$eat['fat_level']);
			$body_string=str_replace($find,$replace,$eat['body_desc']);
			$visceral_string=str_replace("{visceral}",$eat['viscreal_val']."%",$eat['visceral_desc']);
			$glucose_string=str_replace("<<glucose>>",$eat['Glucose_val'],$eat['glucose_desc']);

			$data['body_string']	= $body_string;
			$data['visceral_string']= $visceral_string;
			$data['glucose_string']	= $glucose_string;
			$data['mind'] 			= $this->mind_level($user_id);
	
			/********for credit point**********/
			$credits = $this->report_pdf_model->get_save_credits($user_id);
			$record_id = $credits->id;
			$data['score_image'] 		= $record_id.'highchart.png';
			$data['mov_creditindex'] 	= round($credits->move_val);
			$data['eat_creditindex'] 	= round($credits->eat_val);
			$data['mind_creditindex'] 	= round($credits->mind_val);
			$data['total_creditindex'] 	= round($credits->total_val);
			$data['total_credits'] 		= round($credits->total_credits);

			// Global common variables for PDF starts
            // Page 3
            $data['dynamic']['level']['page3'] 	= base_url(). 'images/movesmart_level' . $data['movesmart']['movesmart_total_level'] . '.png';
        
            // Page 6
            $data['dynamic']['level']['page6'] 	= base_url(). 'images/movesmart_level' . $data['eat']['eat_total_level'] . '.png';

            // Page 7
            $data['dynamic']['level']['page7'] 	= base_url(). 'images/movesmart_level' . $data['mind']['Mind_switch_total_level'] . '.png';

            // Page 9
            $data['dynamic']['level']['page9']	= base_url(). 'images/movesmart_level' . $data['movesmart']['movesmart_total_level'] . '.png';
            // Global common variables for PDF ends here


			/*****for target text**/
			$mpdf->list_indent_first_level = 0;
			$mpdf->SetHTMLFooter('<div style="text-align:center;"><img style="width:60%;" src="images/footerimage.png"/></div>');
			$mpdf->WriteHTML($this->load->view('page1',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('page2',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('page3',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('page4',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('page5',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('page6',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('page7',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('page8',$data,TRUE));
			$mpdf->Output(FCPATH."/pdfs/$pdf_file_name", 'F'); 
		}  
		else if($user_test_type == "FollowUp")
		{  
			$data['movesmart_new'] = $movesmart_new = $this->move_level($user_id);
			$data['movesmart_old'] = $movesmart_old= $this->move_old_level($user_id);
			$data['eat_new'] =$eat_new = $this->eat_level($user_id);
			$find=array("{bmi_val}","{bmi}","{waist_val}","{waist}","{fat_val}","{fat_percentage}");
			$replace=array($eat_new['bmi'],$eat_new['b_level'],$eat_new['waist'],$eat_new['waist_level'],$eat_new['fat_percentage'],$eat_new['fat_level']);
			$body_string=str_replace($find,$replace,$eat_new['body_desc']);
			$visceral_string=str_replace("{visceral}",$eat_new['viscreal_val']."%",$eat_new['visceral_desc']);
			$glucose_string=str_replace("<<glucose>>",$eat_new['Glucose_val'],$eat_new['glucose_desc']);
			$data['body_string']=$body_string;
			$data['visceral_string']=$visceral_string;
			$data['glucose_string']=$glucose_string;
			$data['mind_new'] =$mind_new= $this->mind_level($user_id);
			$data['eat_mind_old'] =$eat_mind_old= $this->eat_mind_old_level($user_id);
			//for the comparision lecvvel description of move//
			$comparisionData = $this->config->item('report_data');
			
			if($movesmart_old['movesmart_total_level']==1 && $movesmart_new['movesmart_total_level']==1)
			{
				$data['move_comparision'] = $comparisionData['movesmart_total_level'][1];
			}
			else if($movesmart_old['movesmart_total_level']==2 && $movesmart_new['movesmart_total_level']==2)
			{
				$data['move_comparision'] = $comparisionData['movesmart_total_level'][2];
			}
			else if($movesmart_old['movesmart_total_level']==3 && $movesmart_new['movesmart_total_level']==3)
			{
				$data['move_comparision'] = $comparisionData['movesmart_total_level'][3];
			}
			else if($movesmart_old['movesmart_total_level']==4 && $movesmart_new['movesmart_total_level']==4)
			{
				$data['move_comparision'] = $comparisionData['movesmart_total_level'][4];
			}
			else if($movesmart_old['movesmart_total_level']==5 && $movesmart_new['movesmart_total_level']==5)
			{
				$data['move_comparision'] = $comparisionData['movesmart_total_level'][5];
			}
			else if($movesmart_old['movesmart_total_level']==6 && $movesmart_new['movesmart_total_level']==6)
			{
				$data['move_comparision'] = $comparisionData['movesmart_total_level'][6];
			}
			else if($movesmart_old['movesmart_total_level']==7 && $movesmart_new['movesmart_total_level']==7)
			{
				$data['move_comparision'] = $comparisionData['movesmart_total_level'][7];
			}
			else 
			{
				$data['move_comparision'] = $comparisionData['movesmart_total_level'][8];
			}

			if($eat_new['eat_total_level']==1 && $eat_mind_old['total_eat_level']==1)
			{
				$data['eat_comparision']  = $comparisionData['eat_total_level'][1];
			}
			else if($eat_new['eat_total_level']==2 && $eat_mind_old['total_eat_level']==2)
			{
				$data['eat_comparision']  = $comparisionData['eat_total_level'][2];
			}
			else if($eat_new['eat_total_level']==3 && $eat_mind_old['total_eat_level']==3)
			{
				$data['eat_comparision']  = $comparisionData['eat_total_level'][3];
			}
			else if($eat_new['eat_total_level']==4 && $eat_mind_old['total_eat_level']==4)
			{
				$data['eat_comparision']  = $comparisionData['eat_total_level'][4];
			}
			else if($eat_new['eat_total_level']==5 && $eat_mind_old['total_eat_level']==5)
			{
				$data['eat_comparision']  = $comparisionData['eat_total_level'][5];
			}
			else if($eat_new['eat_total_level']==6 && $eat_mind_old['total_eat_level']==6)
			{
				$data['eat_comparision']  = $comparisionData['eat_total_level'][6];
			}
			else if($eat_new['eat_total_level']==7 && $eat_mind_old['total_eat_level']==7)
			{
				$data['eat_comparision']  = $comparisionData['eat_total_level'][7];
			}
			else{
				$data['eat_comparision']  = $comparisionData['eat_total_level'][8];
			}
			
			if($mind_new['Mind_switch_total_level']==1 && $eat_mind_old['Mind_switch_total_level']==1)
			{
				$data['mind_comparision']  = $comparisionData['mind_switch_total_level'][1];
			}
			else if($mind_new['Mind_switch_total_level']==2 && $eat_mind_old['Mind_switch_total_level']==2)
			{
				$data['mind_comparision']  = $comparisionData['mind_switch_total_level'][2];
			}
			else if($mind_new['Mind_switch_total_level']==3 && $eat_mind_old['Mind_switch_total_level']==3)
			{
				$data['mind_comparision']  = $comparisionData['mind_switch_total_level'][3];
			}
			else if($mind_new['Mind_switch_total_level']==4 && $eat_mind_old['Mind_switch_total_level']==4)
			{
				$data['mind_comparision']  = $comparisionData['mind_switch_total_level'][4];
			}
			else if($mind_new['Mind_switch_total_level']==5 && $eat_mind_old['Mind_switch_total_level']==5)
			{
				$data['mind_comparision']  = $comparisionData['mind_switch_total_level'][5];
			}
			else if($mind_new['Mind_switch_total_level']==6 && $eat_mind_old['Mind_switch_total_level']==6)
			{
				$data['mind_comparision']  = $comparisionData['mind_switch_total_level'][6];
			}
			else if($mind_new['Mind_switch_total_level']==7 && $eat_mind_old['Mind_switch_total_level']==7)
			{
				$data['mind_comparision']  = $comparisionData['mind_switch_total_level'][7];
			}
			else
			{
				$data['mind_comparision']  = $comparisionData['mind_switch_total_level'][8];
			}
    
			//$data['score'] = $this->old_new_score($user_id);
			$credits = $this->report_pdf_model->get_save_credits($user_id);
			$old_credits = $this->report_pdf_model->get_save_old_credits($user_id);
			$record_id = $credits->id;
			$old_record_id = $old_credits->id;
			$data['follow_score_image'] = $record_id.'highchart.png';
			$data['old_score_image'] = $old_record_id.'highchart.png';

			// Global common variables for PDF start
            // Page 3
            $data['dynamic']['level']['page3'] 		= base_url(). 'images/movesmart_level' . $data['movesmart_new']['movesmart_total_level'] . '.png';
        
            // Page 6
            $data['dynamic']['level']['page6'] 		= base_url(). 'images/movesmart_level' . $data['eat_new']['eat_total_level'] . '.png';

            //Page 7
            $data['dynamic']['level']['page7'] 		= base_url(). 'images/movesmart_level' . $data['mind_new']['Mind_switch_total_level'] . '.png';

            // Page 9
            $data['dynamic']['level_old']['page9'] 	= base_url(). 'images/movesmart_level' .$data['movesmart_old']['movesmart_total_level'].'.png';
            $data['dynamic']['level_new']['page9'] 	= base_url(). 'images/movesmart_level' .$data['movesmart_new']['movesmart_total_level'].'.png';

            // Page 10
            $data['dynamic']['level']['page10'] 	= base_url(). 'images/movesmart_level' . $data['movesmart_new']['movesmart_total_level'] . '.png';

            // Page 11
            $data['dynamic']['level_old']['page11'] = base_url(). 'images/movesmart_level' .$data['eat_mind_old']['total_eat_level'].'.png';
            $data['dynamic']['level_new']['page11'] = base_url(). 'images/movesmart_level' .$data['eat_new']['eat_total_level'].'.png';

            // Page 12
            $data['dynamic']['level_old']['page12'] = base_url(). 'images/movesmart_level' .$data['eat_mind_old']['Mind_switch_total_level'].'.png';
            $data['dynamic']['level_new']['page12'] = base_url(). 'images/movesmart_level' .$data['mind_new']['Mind_switch_total_level'].'.png';
            // Global common variables for PDF ends 


			$mpdf->list_indent_first_level = 0;     
			$mpdf->SetHTMLFooter('<div style="text-align:center;"><img style="width:60%;" src="images/footerimage.png"/></div>');
			$mpdf->WriteHTML($this->load->view('multi/page1',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('multi/page2',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('multi/page3',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('multi/page4',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('multi/page5',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('multi/page6',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('multi/page7',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('multi/page8',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('multi/page9',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('multi/page11',$data,TRUE));
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('multi/page12',$data,TRUE));
			$mpdf->Output(FCPATH."/pdfs/$pdf_file_name", 'F');  
		}
		
		$baseurl = base_url();
		$link = $baseurl."pdfs/".$pdf_file_name; 
		
		$mailData['mail_keys']['{RECIVER_NAME}'] = trim($user_detail->first_name);
		$mailData['mail_keys']['{CLUB_NAME}'] = trim($data['club']->club_name);
		$mailData['mail_keys']['{COACH_NAME}'] = trim($data['coach_detail']->first_name)." ".trim($data['coach_detail']->last_name);
		$mailData['mail_keys']['{DOWNLOAD_LINK}'] = $link;
		$mailData['company_id'] = $user_detail->associated_company_id;
		$to = $user_detail->email;
		
		$this->mailsend->sendMail($to,$mailData,'analyse_report_pdf');
		
		echo json_encode(array('status'=>1,'message'=>'Report has been sent.'));
	}
	
	public function old_new_score($user_id)
	{
		
		$move_score = $this->report_pdf_model->activity_score($user_id,1,'follow');
		
		foreach($move_score as $value){
			$move_old_sum=$move_old_sum+$value['old_score'];
			$move_new_sum=$move_new_sum+$value['new_score'];
			
		}
		$old_mscore=round($move_old_sum*4/100);
		$new_mscore=round($move_new_sum*4/100);
		$data['actual_old_move']=$old_mscore*10;
		$data['actual_new_move']=$new_mscore*10;
		$eat_score = $this->report_pdf_model->activity_score($user_id,2,'follow');
		
		foreach($eat_score as $value){
			$eat_old_sum=$eat_old_sum+$value['old_score'];
			$eat_new_sum=$eat_new_sum+$value['new_score'];
			
		}
		$old_escore=round($eat_old_sum*4/100);
		$new_escore=round($eat_new_sum*4/100);
		$data['actual_old_eat']=$old_escore*10;
		$data['actual_new_eat']=$new_escore*10;
		$mind_score = $this->report_pdf_model->activity_score($user_id,3,'follow');
		
		foreach($mind_score as $value){
			$mind_old_sum=$mind_old_sum+$value['old_score'];
			$mind_new_sum=$mind_new_sum+$value['new_score'];
			
		}
		$old_mindscore=round($mind_old_sum*4/100);
		$new_mindscore=round($mind_new_sum*4/100);
		$data['actual_old_mind']=$old_mindscore*10;
		$data['actual_new_mind']=$new_mindscore*10;
		$data['average_old_score']=($data['actual_old_move']+$data['actual_old_eat']+$data['actual_old_mind'])/3;
		$data['average_new_score']=($data['actual_new_move']+$data['actual_new_eat']+$data['actual_new_mind'])/3;
		
		return $data;
	}
	
	
	public function move_level($user_id)
	{
		
		$fitness= $this->report_model->get_fitness($user_id);
		if($fitness<=1)
		{
			$fitness=1;
		}
		else if($fitness>=7)
		{
			$fitness=7;
		}
		$data['fitness'] = $fitness;
		$data['fitness_desc'] = $this->report_model->get_level_description('1','75',$fitness);
		$flexbility = $this->report_model->get_flexibility($user_id);
		 if($flexbility<=1)
		{
			$flexbility=1;
		}
		else if($flexbility>=7)
		{
			$flexbility=7;
		}
		$data['flexbility'] = $flexbility;
		$data['flexbility_desc'] = $this->report_model->get_level_description('1','76',$flexbility);
		$strength = $this->report_model->get_strength($user_id);
		if($strength<=1)
		{
			$strength=1;
		}
		else if($strength>=7)
		{
			$strength=7;
		}
		$data['strength'] = $strength;
		$data['strength_desc'] = $this->report_model->get_level_description('1','77',$strength);

		if($fitness !=0 && $fitness !='')
		{			
		$fitness = ($fitness*80)/100;
		}
		if($flexbility !=0 && $flexbility !='')
		{
		$flexbility = ($flexbility*10)/100;
		}
		if($strength !=0 && $strength !='' )
		{
		$strength = ($strength*10)/100;
		}
		
			
		$move=round($fitness+$flexbility+$strength);
		if($move<=1)
		{
			$move=1;
		}
		else if($move>7)
		{
			$move=7;
		}
		$data['movesmart_total_level']=$move;
		$data['movesmart_total_level_desc'] = $this->report_model->get_level_description('1','78',$move);
		//$result['movesmart'] = $data;

		return $data;
		
	}
	
	public function move_old_level($user_id)
	{
		$fitness= $this->report_pdf_model->get_fitness_old($user_id);
		if($fitness<=1)
			{
				$fitness=1;
			}
		 else if($fitness>7)
			{
				$fitness=7;
			}		
		$data['fitness'] = $fitness;
		$data['fitness_desc'] = $this->report_model->get_level_description('1','75',$data['fitness']);
		$flexbility = $this->report_pdf_model->get_flexibility_old($user_id);
		if($flexbility<=1)
		{
			$flexbility=1;
		}
		else if($flexbility>7)
		{
			$flexbility=7;
		}
		$data['flexbility'] = $flexbility;
		$data['flexbility_desc'] = $this->report_model->get_level_description('1','76',$data['flexbility']);
		$strength = $this->report_pdf_model->get_strength_old($user_id);
		if($strength<=1)
		{
			$strength=1;
		}
		else if($strength>7)
		{
			$strength=7;
		}
		$data['strength'] = $strength;
		$data['strength_desc'] = $this->report_model->get_level_description('1','77',$data['strength']);
		if($fitness !=0 && $fitness !='')
		{			
		$fitness = ($fitness*80)/100;
		}
		if($flexbility !=0 && $flexbility !='')
		{
		$flexbility = ($flexbility*10)/100;
		}
		if($strength !=0 && $strength !='' )
		{
		$strength = ($strength*10)/100;
		}
					 
		$move=round($fitness+$flexbility+$strength);
		if($move<=1)
		{
			$move=1;
		}
		else if($move>7)
		{
			$move=7;
		}
		$data['movesmart_total_level']=$move;
		$data['movesmart_total_level_desc'] = $this->report_model->get_level_description('1','78',$move);
		//$result['movesmart'] = $data;
		return $data;
	}
	
public function eat_level($user_id)
 {
  $user_info = $this->report_model->get_personal_info($user_id);
   $user = $this->report_model->get_user($user_id);
			$gender = $user->gender;
			$birthDate = $user_info->dob; //(y-m-d)
		    $birthDate = explode("-", $birthDate);
		    $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[1], $birthDate[2], $birthDate[0]))) > date("md")
			? ((date("Y") - $birthDate[0]) - 1)
			: (date("Y") - $birthDate[0]));
	
	//$body_composition  = $this->report_model->get_body_composition($user_id);
	$b_comp = $this->report_pdf_model->get_body_composition($user_id,$gender,$age);
	$b_compsition_value=$b_comp['bmi'];
	$body_composition = $b_comp['body_composition'];
	$bmi_level = $b_comp['bmi_level'];
	$waist = $b_comp['waist'];
	$w_level = $b_comp['waist_level'];
	$fat_percentage = $b_comp['fat_percentage'];
	$fat_level = $b_comp['fat_level'];
	 if($body_composition<=1)
		{
			$body_composition=1;
		}
		else if($body_composition>=7)
		{
			$body_composition=7;
		}
	$data['body_composition'] = round($body_composition);
	
    $data['body_desc'] = $this->report_model->get_level_description('2','26',round($body_composition));
    $visc_data =$this->report_pdf_model->get_visceral_fat($user_id);
    $visceral=$visc_data['visceral'];
   $vis_val=$visc_data['vis_value'];
     if($visceral<=1)
		{
			
			$visceral=1;
			
		}
		else if($visceral>7)
		{
			$visceral=7;
			
		}
		
	
    $data['viscreal_fat%'] =$visceral;
	$data['visceral_desc'] = $this->report_model->get_level_description('2','67',$visceral);
    $chol_data= $this->report_pdf_model->get_cholestrol($user_id);

   $cholestrol=$chol_data['cholestrol'];
   $cholestrol_val=$chol_data['chol_value'];
 	if($cholestrol<=1)
		{
			$cholestrol=1;
		}
		else if($cholestrol>7)
		{
			$cholestrol=7;
		}
	
   $data['Cholesterol'] = $cholestrol;
   $data['cholestrol_desc'] = $this->report_model->get_level_description('2','8',$cholestrol);
   $glu_data= $this->report_pdf_model->get_glucose($user_id);
 $glucose=$glu_data['glucose'];
   $glucose_val=$glu_data['glu_value'];
   if($glucose<=1)
		{
			$glucose=1;
		}
		else if($glucose>7)
		{
			$glucose=7;
		}
		
   $data['Glucose'] =$glucose;
   $data['glucose_desc'] = $this->report_model->get_level_description('2','68',$glucose);
   
   if($body_composition !=0 && $body_composition !='')
		{			
		$body_compos = ($body_composition*40)/100;
		}
		if($visceral !=0 && $visceral !='')
		{
		$viscera = ($visceral*40)/100;
		}
		if($cholestrol !=0 && $cholestrol !='' )
		{
		$choles = ($cholestrol*10)/100;
		}
		if($glucose !=0 && $glucose !='' )
		{
		$gluco = ($glucose*10)/100;
		}
					 
		 $eat=round($body_compos+$viscera+$choles+$gluco);

   //$eat=round($data['body_composition']+$data['viscreal_fat%']+$data['Cholesterol']+$data['Glucose']);
   if($eat<=1)
		{
			$eat=1;
		}
		else if($eat>7)
		{
			$eat=7;
		}

   $data['eat_total_level'] = $eat;
    $data['eat_total_level_desc'] = $this->report_model->get_level_description('2','79',$eat);
    //$result['Eat'] = $data;
     $data['bmi'] = $b_compsition_value;
    $data['b_level'] = $bmi_level;
    $data['waist'] = $waist;
    $data['waist_level']=$w_level;
    $data['fat_percentage'] = $fat_percentage;
	$data['fat_level'] = $fat_level;
    $data['Glucose_val'] = $glucose_val;
    $data['choles_val'] = $cholestrol_val;
    $data['viscreal_val'] =$vis_val;
  
    return $data;
 }	
 

 
 public function mind_level($user_id)  
 {
  
 
  $dat = $this->report_model->get_vita_sixteeen($user_id);
$vita_16=$dat['vita_16'];
   if($vita_16<=1)
		{
			$vita_16=1;
		}
		else if($vita_16>=7)
		{
			$vita_16=7;
		}
		
$data['vita_16']=round($vita_16);
$data['vita_desc'] = $this->report_model->get_level_description('3','70',round($vita_16));
  $except_vita_16 = $this->report_model->get_stress_sleep_mindfullness_level($user_id);

   $stress= $except_vita_16['stress'];

 if($stress<=1)
		{	
			$stress=1;
		}
		else if($stress>7)
		{
			$stress=7;
		}
		   
		$data['stress'] =$stress;
 $data['stress_desc'] = $this->report_model->get_level_description('3','35',$stress);
 $sleep= $except_vita_16['sleep'];
	if($sleep<=1)
		{
			$sleep=1;
		}
		else if($sleep>7)
		{
			$sleep=7;
		}
$data['sleep'] =$sleep;
$data['sleep_desc'] = $this->report_model->get_level_description('3','73',$sleep);
$mindfull= $except_vita_16['mindful'];
 if($mindfull<=1)
		{
			$mindfull=1;
		}
		else if($mindfull>7)
		{
			$mindfull=7;
		}
 $data['mindful'] =$mindfull;
 $data['mindfull_desc'] = $this->report_model->get_level_description('3','74',$mindfull);
  if($vita_16 !=0 && $vita_16 !='')
		{			
		$vita_16 = ($vita_16*70)/100;
		}
		if($stress !=0 && $stress !='')
		{
		$stress = ($stress*10)/100;
		}
		if($sleep !=0 && $sleep !='' )
		{
		$sleep = ($sleep*10)/100;
		}
		if($mindfull !=0 && $mindfull !='' )
		{
		$mindfull = ($mindfull*10)/100;
		}
					 
		 $mind=round($vita_16+$stress+$sleep+$mindfull);
 
	if($mind<=1)
		{
			$mind=1;
		}
		else if($mind>7)
		{
			$mind=7;
		}
		
 $data['Mind_switch_total_level'] =$mind;
  $data['mind_switch_level_desc'] = $this->report_model->get_level_description('3','80',$mind);
  //$result['Mind Switch'] = $data;

 return $data;  
  
 }
 public function eat_mind_old_level($user_id)  
 {
  
 
  $eat_mind_level= $this->report_pdf_model->EatMind_old_level($user_id);
  if(isset($eat_mind_level) && $eat_mind_level!='0'  )
  {
	  
	  $body_composition_level = round($eat_mind_level->body_composition_level);
	  if($body_composition_level<=1)
		{
			$body_composition_level=1;
		}
		else if($body_composition_level>7)
		{
			$body_composition_level=7;
		}
	  $data['body_composition_level'] =$body_composition_level;
	  $data['body_composition_level_desc'] = $this->report_model->get_level_description('2','26',$data['body_composition_level']);
	   $visceral_fat_level= round($eat_mind_level->visceral_fat_level);
	    if($visceral_fat_level<=1)
		{
			$visceral_fat_level=1;
		}
		else if($visceral_fat_level>7)
		{
			$visceral_fat_level=7;
		}
	   $data['visceral_fat_level']= $visceral_fat_level;
	  $data['visceral_fat_level_desc'] = $this->report_model->get_level_description('2','67',$data['visceral_fat_level']);
	  $cholesterol_level= round($eat_mind_level->cholesterol_level);
	   if($cholesterol_level<=1)
		{
			$cholesterol_level=1;
		}
		else if($cholesterol_level>7)
		{
			$cholesterol_level=7;
		}
	  $data['cholesterol_level']=$cholesterol_level;
	  $data['cholesterol_level_desc'] = $this->report_model->get_level_description('2','8',$data['cholesterol_level']);
	  $glucose_level= round($eat_mind_level->glucose_level);
	    if($glucose_level<=1)
		{
			$glucose_level=1;
		}
		else if($glucose_level>7)
		{
			$glucose_level=7;
		}
	   $data['glucose_level']=$glucose_level;	
	  $data['glucose_level_desc'] = $this->report_model->get_level_description('2','68',$data['glucose_level']);
	  //$eat = $data['body_composition_level']+$data['visceral_fat_level']+$data['glucose_level_desc'];
	$eat = round($eat_mind_level->total_eat_level);
	 if($eat<=1)
		{
			$eat=1;
		}
		else if($eat>7)
		{
			$eat=7;
		}
	 $data['total_eat_level']=$eat;
	  $data['eat_total_level_desc'] = $this->report_model->get_level_description('2','79',$eat);
	  
	  
	  $vita= round($eat_mind_level->vita_16_level);
	  if($vita<=1)
		{
			$vita=1;
		}
		else if($vita>7)
		{
			$vita=7;
		}
	  $data['vita_16']=$vita;
	  $data['vita_desc'] = $this->report_model->get_level_description('3','70',$vita);
	  $stress= round($eat_mind_level->stress_level);
	  if($stress<=1)
		{
			$stress=1;
		}
		else if($stress>7)
		{
			$stress=7;
		}
	  $data['stress'] =$stress;
	  $data['stress_desc'] = $this->report_model->get_level_description('3','35',$stress);
	  $sleep= round($eat_mind_level->sleep_level);
	  if($sleep<=1)
		{
			$sleep=1;
		}
		else if($sleep>7)
		{
			$sleep=7;
		}
	  $data['sleep'] =$sleep;
	  $data['sleep_desc'] = $this->report_model->get_level_description('3','73',$sleep);
	  $mindfull= round($eat_mind_level->mindfullness_level);
	   if($mindfull<=1)
		{
			$mindfull=1;
		}
		else if($mindfull>7)
		{
			$mindfull=7;
		}	
	  $data['mindful'] =$mindfull;
	  $data['mindfull_desc'] = $this->report_model->get_level_description('3','74',$mindfull);
	// $mind= round($data['vita_16']+$data['stress']+$data['sleep']+$data['mindful']);
	   $mind = round($eat_mind_level->total_mind_level);
	   if($mind<=1)
		{
			$mind=1;
		}
		else if($mind>7)
		{
			$mind=7;
		}
	  $data['Mind_switch_total_level'] =$mind;
	  $data['mind_switch_level_desc'] = $this->report_model->get_level_description('3','80',$mind);
	  $data['old_test_date'] = $eat_mind_level->test_date;  
  }
 
  return $data;
  
 }
 /**********for particular activity heart zone********/
 public function get_heart_zone($point,$age_weight_on_test)
 {
	 $weight = $age_weight_on_test->weight_on_test;
	 $reg_obj1=json_decode($age_weight_on_test->reg_info1);
	 $iand_p = $age_weight_on_test->max_max_load;
	
	$B0 = $reg_obj1->B0;
	$B1 = $reg_obj1->B1;
	 //$activitypnt = (($pointspeed * $pcsp) / 100);
	 $activitypnt = $point['activity_point'];
	 $DeflCoef = $point['f_deflcoef'];
	 $minhrcond = $point['f_minhrcond'];
	 $maxhrcond = $point['f_maxhrcond'];
	 //$wattdiflperactivty = round((((($iant_p * 11.35)+320)/0.9)-320)/11.35);
	 $wattdiflperactivty = round((((($iand_p * 11.35)+320)/0.9)-320)/11.35);
	
                $aWATTDeflectionPoint = (($wattdiflperactivty  * $DeflCoef) / 100);   

                $hrmin = $aWATTDeflectionPoint * ($minhrcond / 100);
                $hrmax = $aWATTDeflectionPoint * ($maxhrcond / 100);
			//$data['minHRZone']=round($hrmin);
                $data['minHRZone']  = round(($B1 *(((($hrmin*11.35 + 320)/(0.9 * $weight)) - 4.25 ) / 2.98)) + $B0);
               $data['maxHRZone']  = round(($B1 *(((($hrmax*11.35 + 320)/(0.9 * $weight)) - 4.25 ) / 2.98)) + $B0);

				return $data;
 }
 
 public function week_credits($age_on_test,$weight_on_test,$fitness,$gender)
 {
		
	 $week_1=$this->report_pdf_model->per_week($age_on_test,$fitness,$gender,1);
	$data['week_1'] = $week_1->f_points;
	 $week_2=$this->report_pdf_model->per_week($age_on_test,$fitness,$gender,2);
	 $data['week_2'] = $week_2->f_points;
	 $week_3=$this->report_pdf_model->per_week($age_on_test,$fitness,$gender,3);
	 $data['week_3'] = $week_3->f_points;
	 $week_4=$this->report_pdf_model->per_week($age_on_test,$fitness,$gender,4);
	 $data['week_4'] = $week_4->f_points;
	 $week_5=$this->report_pdf_model->per_week($age_on_test,$fitness,$gender,5);
	 $data['week_5'] = $week_5->f_points;
	 $week_6=$this->report_pdf_model->per_week($age_on_test,$fitness,$gender,6);
	 $data['week_6'] = $week_6->f_points;
	 $week_7=$this->report_pdf_model->per_week($age_on_test,$fitness,$gender,7);
	 $data['week_7'] = $week_7->f_points;
	 $week_8=$this->report_pdf_model->per_week($age_on_test,$fitness,$gender,8);
	 $data['week_8'] = $week_8->f_points;
	 $week_9=$this->report_pdf_model->per_week($age_on_test,$fitness,$gender,9);
	 $data['week_9'] = $week_9->f_points;
	 $week_10=$this->report_pdf_model->per_week($age_on_test,$fitness,$gender,10);
	 $data['week_10'] = $week_10->f_points;
	 $week_11=$this->report_pdf_model->per_week($age_on_test,$fitness,$gender,11);
	 $data['week_11'] = $week_11->f_points;
	 $week_12=$this->report_pdf_model->per_week($age_on_test,$fitness,$gender,12);
	 $data['week_12'] = $week_12->f_points;
	
	 return $data;
 }
 public function training_schema()
 {
			$user_id = $this->input->post('user_id');

			$data['latest_test_date']=$latest_test_date =  $this->report_pdf_model->latest_test_date($user_id);
			$data['user_detail']=$user_detail = $this->report_pdf_model->user_detail($user_id);
			$gender = $user_detail->gender;
			//for credits per activity//  
			$age_weight_on_test = $this->report_pdf_model->user_age($user_id);
			$age_on_test=$age_weight_on_test->age_on_test; //age of user
			$weight_on_test=$age_weight_on_test->weight_on_test; //weight of user
			$fitness= $this->report_model->get_fitness($user_id);
			$data['target_credits'] = $target =  $this->report_pdf_model->target_credits($user_id);
			$data['week_per_credits'] = $week_per_credits = $this->week_credits($age_on_test,$weight_on_test,$fitness,$gender);
			
			$point_speed = $this->report_pdf_model->get_point_speed($age_on_test,$fitness,$gender);
			$data['run_point'] =$run_point= $this->report_pdf_model->get_activity($fitness,'Run',$point_speed);
			$data['run_heart_zone'] = $this->get_heart_zone($run_point,$age_weight_on_test);
			$data['cycle_point'] =$cycle_point= $this->report_pdf_model->get_activity($fitness,'Cycle',$point_speed);
			$data['cycle_heart_zone'] = $this->get_heart_zone($cycle_point,$age_weight_on_test);
			$data['swim_point'] =$swim_point= $this->report_pdf_model->get_activity($fitness,'Swim',$point_speed);
			$data['swim_heart_zone'] = $this->get_heart_zone($swim_point,$age_weight_on_test);
			$data['walk_point'] =$walk_point= $this->report_pdf_model->get_activity($fitness,'Walk',$point_speed);
			$data['walk_heart_zone'] = $this->get_heart_zone($walk_point,$age_weight_on_test);
			$data['rowing_point'] =$rowing_point= $this->report_pdf_model->get_activity($fitness,'Rowing',$point_speed);
			$data['rowing_heart_zone'] = $this->get_heart_zone($rowing_point,$age_weight_on_test);
	
			$data['hr_run'] =$this->report_pdf_model->get_heart_range($user_id,'Running'); 
			$data['hr_cycle'] =$this->report_pdf_model->get_heart_range($user_id,'Cycling / MTB');
			$data['hr_swim'] =$this->report_pdf_model->get_heart_range($user_id,'Swimming');
			$data['hr_walk'] =$this->report_pdf_model->get_heart_range($user_id,'Walking');
			$data['hr_row'] =$this->report_pdf_model->get_heart_range($user_id,'Rowing');
	
			//for credit score//
				$user_email = $user_detail->email;
				$club_id=$user_detail->r_club_id;
				$data['club']=$this->report_pdf_model->club_detail($club_id);
				$coach_id=$this->report_pdf_model->get_coach_id($user_id);
				$data['coach_detail'] = $this->report_pdf_model->user_detail($coach_id);
				$data['coach_image'] = $this->report_pdf_model->coach_image($coach_id);
				$goal= $this->report_pdf_model->client_goal($user_id);
		
			/*****for target text**/
		    $target_text = array('1'=>'Ik wil fit worden',
								'2'=>'Ik wil afvallen en/of mijn buikomvang verminderen',
								'3'=>'Ik wil meer energie',
								'4'=>'Ik wil vitaal worden'
								);
						$data['goal'] = $target_text[$goal];	
/*end*/		$data['movesmart'] = $this->move_level($user_id);				
		
		$data['iand_70'] =$iand_70 = round($target['iant_p']*70/100);
		$data['iand_80'] =$iand_80 = round($target['iant_p']*80/100);
		$data['iand_90'] =$iand_90 = round($target['iant_p']*90/100);
		$data['iand_97'] =$iand_97 = round($target['iant_p']*97/100);
		$data['cycle_heart_zone_70'] = $target['min_heart_70'];
		$data['cycle_heart_zone_90'] = $target['max_heart_90'];
		$data ['cycle_heart_zone_97'] = round($target['iant_hr']*97/100);
	/*	$data['cycle_heart_zone_70'] = round($target['iant_hr']*70/100);
		$data['cycle_heart_zone_80'] = round($target['iant_hr']*80/100);
		$data['cycle_heart_zone_90'] = round($target['iant_hr']*90/100);
		$data['cycle_heart_zone_97'] = round($target['iant_hr']*97/100);*/
		
	  echo json_encode($data);
	  
	 
 }
 public function check_result()
 {
	 
			$user_id = $this->input->post('user_id');
	
			$data['user_detail']=$user_detail = $this->report_pdf_model->user_detail($user_id);
			$data['general_ques'] = $this->report_pdf_model->personal_questions($user_id,999999,30,2,'profile');
			
			$data['preference'] = $this->report_pdf_model->preference_model($user_id,999999,30,'preference');
			$data['eat_day'] = $this->report_pdf_model->get_days($user_id,2,2);
			$data['mind_day'] = $this->report_pdf_model->get_days($user_id,3,2);
			$health_skill= $this->report_pdf_model->personal_questions($user_id,999999,30);
			/********for goal question**/
			$element_id = $this->report_pdf_model->get_felement_id(999999,30,'goal');
		
			 $goal = $this->report_pdf_model->mv_question($user_id,$element_id);
			 
			$val_goal=$goal->actmv_otherdata;
			
			$goal_val = explode(':',$val_goal);
			
			foreach($goal_val as $ex_val_goal)
			{
				if (is_numeric($ex_val_goal)) {
				$data['goal']=$ex_val_goal;
				}
			}
			/**end**/
			/****for medical question value**/
			$elementid = $this->report_pdf_model->get_felement_id(999999,30,'medical');
			$medical = $this->report_pdf_model->mv_question($user_id,$elementid);
			
			$val_medical=$medical->actmv_otherdata;
			$medical_val = explode('_:',$val_medical);
			foreach($medical_val as $key => $ex_val_medical)
			{
				if($key!=0)
				{				
				$option_id =$ex_val_medical;
				$info_medical[] =$this->report_pdf_model->medical_text($option_id);
				
				$med = $info_medical;
		
				}
				
			}
			
			$data['med'] =$med;
			
		
			/**end**/
			foreach($health_skill as $val)
			{
				$health = $health+$val;
			}
			$data['health_skill']=$health;
			
	$data['step_count'] = $this->report_pdf_model->count_step($user_id,2);
	/********for credit point**********/
	$credits = $this->report_pdf_model->get_save_credits($user_id);
	$data['mov_creditindex'] = round($credits->move_val);
	$data['eat_creditindex'] = round($credits->eat_val);
	$data['mind_creditindex'] = round($credits->mind_val);
	$data['total_creditindex'] = round($credits->total_val);
	$data['total_credits'] = round($credits->total_credits);
		/*****for target text**/
		   
		
					
	echo json_encode($data);
 }
 
 public function savecharts()
   {  


		    $user_id = $this->input->post('user_id');
			$string = str_replace(' ', '+', $this->input->post('bin_data'));
			$string = base64_decode($string);
			$record = $this->voucher_model->get_save_credits($user_id);
			if(!empty($record)){

			$record_id=$record->id; 
			$fileName =$record_id.'highchart.png';
			$save = "chart/".$fileName ."";
			$file = file_put_contents($save, $string);

			 if($file){
			$data['status']=1;
			}
			else{
				$data['status']=0;
			} 
				}
				else{
				}
			echo $data['status'];
			die;
   }
		
	
	public function makeimage(){
	$user_id = $this->input->post('user_id');
		
	$credits = $this->report_pdf_model->get_save_credits($user_id);
	$data['mov_creditindex'] = round($credits->move_val);
	$data['eat_creditindex'] = round($credits->eat_val);
	$data['mind_creditindex'] = round($credits->mind_val);
	$data['total_creditindex'] = round($credits->total_val);
	$data['total_credits'] = round($credits->total_credits);
	echo $data['chart_image'] = $this->load->view('chart',$data,TRUE);
	 

	echo json_encode(array('status'=>1,'message'=>'sucessfully made'));
		
	}
}
