<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	 function __construct()
    {
        parent::__construct();
        
        $this->load->model('report_model');
        $this->load->model('report_pdf_model');
       
    }
	public function index()
	{
		
		//$user_id = $this->input->post('user_id');
		
		//$data = $this->report_model->get_reporting_by_consult_type($user_id);
		
		//echo  json_encode($data);
		
	}
	
	/****** function to get movesmart level*****/
	public function movesmart()
	{
		$user_id = $this->input->post('user_id');
		
		$fitness_level= $this->report_model->get_fitness($user_id);
		if($fitness_level<=1)
		{
			$fitness_level=1;
		}
		else if($fitness_level>=7)
		{
			$fitness_level=7;
		}
		$flexbility_level = $this->report_model->get_flexibility($user_id);
		 if($flexbility_level<=1)
		{
			$flexbility_level=1;
		}
		else if($flexbility_level>=7)
		{
			$flexbility_level=7;
		}
		$strength_level = $this->report_model->get_strength($user_id);
		if($strength_level<=1)
		{
			$strength_level=1;
		}
		else if($strength_level>=7)
		{
			$strength_level=7;
		}
		if($fitness_level !=0 && $fitness_level !='')
		{
			
		$fitness = ($fitness_level*80)/100;
		}
		if($flexbility_level !=0 && $flexbility_level !='')
		{
		$flexbility = ($flexbility_level*10)/100;
		}
		if($strength_level !=0 && $strength_level !='' )
		{
		$strength = ($strength_level*10)/100;
		}
		$data['cardio_fitness'] = $fitness_level;
		$data['agility_flexbility'] = $flexbility_level;
		$data['strength'] = $strength_level;
		$data['movesmart_total_level']=round($fitness+$flexbility+$strength);
		$result['movesmart'] = $data;
		echo json_encode($result);
		die;
		
		
		
	}
	/****** function to get eat level*****/
	
	 public function eat()
	 {

		$user_id = $this->input->post('user_id');
		$user_info = $this->report_model->get_personal_info($user_id);
		$user = $this->report_model->get_user($user_id);
		$gender = $user->gender;
		$birthDate = $user_info->dob; //(y-m-d)
		  //$birthDate = "12/17/1983";mdy
		  //explode the date to get month, day and year
		  $birthDate = explode("-", $birthDate);
		  //get age from date or birthdate
		  $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[1], $birthDate[2], $birthDate[0]))) > date("md")
			? ((date("Y") - $birthDate[0]) - 1)
			: (date("Y") - $birthDate[0]));
		  
		$body_compo = $this->report_model->get_body_composition($user_id,$gender,$age);
		 if($body_compo<=1)
		{
			$body_compo=1;
		}  
		else if($body_compo>=7)
		{
			$body_compo=7;
		}
		$data['body_composition'] = intval($body_compo);
	//	$body_composition= $body_compo*40/100;
		//$data['body_composition'] =round($body_composition);
		$vis_fat = $this->report_model->get_visceral_fat($user_id);
		 if($vis_fat<=1)
		{
			$vis_fat=1;
		}
		else if($vis_fat>=7)
		{
			$vis_fat=7;
		}
		$data['viscreal_fat'] = $vis_fat;
		//$viscreal_fat=$vis_fat*40/100;
		//$data['viscreal_fat'] =round($viscreal_fat);
		$choles = $this->report_model->get_cholestrol($user_id);
		 if($choles<=1)
		{
			$choles=1;
		}
		else if($choles>=7)
		{
			$choles=7;
		}
		$data['Cholesterol'] = $choles;
		//$cholesterol=$choles*10/100;
		//$data['Cholesterol'] = round($cholesterol);
		$gluc = $this->report_model->get_glucose($user_id);
		if($gluc<=1)
		{
			$gluc=1;
		}
		else if($gluc>=7)
		{
			$gluc=7;
		}
		$data['Glucose'] = $gluc;
		//$glucose=$gluc*10/100;
		if($body_compo !=0 && $body_compo !='')
		{			
		$body_composition = ($body_compo*40)/100;
		}
		if($vis_fat !=0 && $vis_fat !='')
		{
		$viscreal_fat = ($vis_fat*40)/100;
		}
		if($choles !=0 && $choles !='' )
		{
		$cholesterol = ($choles*10)/100;
		}
		if($gluc !=0 && $gluc !='' )
		{
		$glucose = ($gluc*10)/100;
		}
		$eat=round($body_composition+$viscreal_fat+$cholesterol+$glucose);
		 if($eat<=1)
		{
			$eat=1;
		}
		else if($eat>7)
		{
			$eat=7;
		}

   $data['eat_total_level'] = $eat;
		//$data['Glucose'] = round($glucose);
	//	$data['eat_total_level'] = round(body_composition+$viscreal_fat+$cholesterol+$glucose);
		$result['Eat'] = $data;
		
		echo json_encode($result);
	 }
 
 /******************************
 function for get mind switch
 ******************************/
 public function mind_switch()  
 {
  $user_id = $this->input->post('user_id');
  $dat= $this->report_model->get_vita_sixteeen($user_id);

 $vita_16=$dat['vita_16'];

  if($vita_16<=1)
		{
			$vita_16=1;
		}
		else if($vita_16>7)
		{
			$vita_16=7;
		}
$data['vita_16']=round($vita_16);
 $except_vita_16 = $this->report_model->get_stress_sleep_mindfullness_level($user_id);
 $stress= $except_vita_16['stress'];
$data['stress_val'] = $except_vita_16['stress_val']; 
$data['sleep_val'] = $except_vita_16['sleep_val'];
$data['mindful_val'] = $except_vita_16['mindful_val'];
 if($stress<=1)
		{	
			$stress=1;
		}
		else if($stress>7)
		{
			$stress=7;
		}
		
		$data['stress'] =$stress;
		$sleep= $except_vita_16['sleep'];
	if($sleep<=1)
		{
			$sleep=1;
		}
		else if($sleep>7)
		{
			$sleep=7;
		}
$data['sleep'] =$sleep;
$mindfull= $except_vita_16['mindful'];
 if($mindfull<=1)
		{
			$mindfull=1;
		}
		else if($mindfull>7)
		{
			$mindfull=7;
		}
 $data['mindful'] =$mindfull;
if($vita_16 !=0 && $vita_16 !='')
		{			
		$vita_16 = ($vita_16*70)/100;
		}
		if($stress !=0 && $stress !='')
		{
		$stress = ($stress*10)/100;
		}
		if($sleep !=0 && $sleep !='' )
		{
		$sleep = ($sleep*10)/100;
		}
		if($mindfull !=0 && $mindfull !='' )
		{
		$mindfull = ($mindfull*10)/100;
		}
					 
 //$watiwant       = $this->report_model->get_vita_sixteeen($user_id,'others');
 $data['energy'] = $dat['energy']; 
 $data['motivation'] = $dat['motivation']; 
 $data['resilience'] = $dat['resilience']; 
 $data['personalgoal'] = $this->report_model->personal_goal($user_id);

 $mind =  round($vita_16+$stress+$sleep+$mindfull);
 if($mind<=1)
		{
			$mind=1;
		}
		else if($mind>7)
		{
			$mind=7;
		}
		
 $data['Mind_switch_total_level'] =$mind;
  $result['Mind_Switch'] = $data; 

  echo json_encode($result);  
 
  
 }
 public function level_description()
 {
	  $type_id = $this->input->post('type_id');
	  $item_id = $this->input->post('item_id');
	  $level = $this->input->post('level');
	 $description = $this->report_model->get_level_description($type_id,$item_id,$level);
	if(!empty($description))
	{	$data = array('status'=>'Success','status_code'=>1,'description'=>$description);
		
	}
	else
	{	$data = array('status_code'=>0,'status'=>'No Data available');
		
	}
	echo json_encode($data);
 }

  public function eat_level()
 {
  
    $user_id = $this->input->post('user_id');
	$user_info = $this->report_model->get_personal_info($user_id);
		$user = $this->report_model->get_user($user_id);
		$gender = $user->gender;
		$birthDate = $user_info->dob; //(y-m-d)
		  //$birthDate = "12/17/1983";mdy
		  //explode the date to get month, day and year
		  $birthDate = explode("-", $birthDate);
		  //get age from date or birthdate
		  $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[1], $birthDate[2], $birthDate[0]))) > date("md")
			? ((date("Y") - $birthDate[0]) - 1)
			: (date("Y") - $birthDate[0]));
	
	//$body_composition  = $this->report_model->get_body_composition($user_id);
	$b_comp = $this->report_pdf_model->get_body_composition($user_id,$gender,$age);
	$b_compsition_value=$b_comp['bmi'];
	$body_composition = $b_comp['body_composition'];
	$bmi_level = $b_comp['bmi_level'];
	$waist = $b_comp['waist'];
	$w_level = $b_comp['waist_level'];
	$fat_percentage = $b_comp['fat_percentage'];
	$fat_level = $b_comp['fat_level'];
	 if($body_composition<=1)
		{
			$body_composition=1;
		}
		else if($body_composition>=7)
		{
			$body_composition=7;
		}
	$data['body_composition'] = intval($body_composition);
	
     $body_desc =  $this->report_model->get_level_description('2','26',intval($body_composition));
    $visc_data =$this->report_pdf_model->get_visceral_fat($user_id);
    $visceral=$visc_data['visceral'];
   $vis_val=$visc_data['vis_value'];
  
     if($visceral<=1)
		{
			
			$visceral=1;
			
		}
		else if($visceral>7)
		{
			$visceral=7;
			
		}
		
	
    $data['viscreal_fat%'] =$visceral;
	$visceral_desc =  $this->report_model->get_level_description('2','67',$visceral);
    $chol_data= $this->report_pdf_model->get_cholestrol($user_id);

   $cholestrol=$chol_data['cholestrol'];
   $cholestrol_val=$chol_data['chol_value'];
 	if($cholestrol<=1)
		{
			$cholestrol=1;
		}
		else if($cholestrol>7)
		{
			$cholestrol=7;
		}
	
   $data['Cholesterol'] = $cholestrol;
    $cholestrol_desc = $this->report_model->get_level_description('2','8',$cholestrol);
   $glu_data= $this->report_pdf_model->get_glucose($user_id);
 $glucose=$glu_data['glucose'];
   $glucose_val=$glu_data['glu_value'];
   if($glucose<=1)
		{
			$glucose=1;
		}
		else if($glucose>7)
		{
			$glucose=7;
		}
		
   $data['Glucose'] =$glucose;
   $glucose_desc = $this->report_model->get_level_description('2','68',$glucose);
   
   if($body_composition !=0 && $body_composition !='')
		{			
		$body_compos = ($body_composition*40)/100;
		}
		if($visceral !=0 && $visceral !='')
		{
		$viscera = ($visceral*40)/100;
		}
		if($cholestrol !=0 && $cholestrol !='' )
		{
		$choles = ($cholestrol*10)/100;
		}
		if($glucose !=0 && $glucose !='' )
		{
		$gluco = ($glucose*10)/100;
		}
					 
		 $eat=round($body_compos+$viscera+$choles+$gluco);

   //$eat=round($data['body_composition']+$data['viscreal_fat%']+$data['Cholesterol']+$data['Glucose']);
   if($eat<=1)
		{
			$eat=1;
		}
		else if($eat>7)
		{
			$eat=7;
		}

   $data['eat_total_level'] = $eat;
    $data['eat_total_level_desc'] = $this->report_model->get_level_description('2','79',$eat);
    //$result['Eat'] = $data;
     $data['bmi'] = $b_compsition_value;
    $data['b_level'] = $bmi_level;
   
	   
  $find=array("{bmi_val}","{bmi}","{waist_val}","{waist}","{fat_val}","{fat_percentage}");
			$replace=array($b_compsition_value,$bmi_level,$waist,$w_level,$fat_percentage,$fat_level);
			$body_string=str_replace($find,$replace,$body_desc);
			$visceral_string=str_replace("{visceral}",$vis_val."%",$visceral_desc);
			$glucose_string=str_replace("<<glucose>>",$glucose_val,$glucose_desc);
			$data['body_string']=$body_string;
			$data['visceral_string']=$visceral_string;
			$data['glucose_string']=$glucose_string;
			$data['cholestrol_desc'] = $cholestrol_desc;
		
    echo json_encode($data);
 }	
 public function get_supportlink()
 {
 	$link = $this->report_model->get_link();
	
 	echo json_encode(array('link'=>$link,'success'=>1));

 }
/*****************for coach info***********/
public function coach_info(){
	
	$user_id = $this->input->post('user_id');
	$coach_user_id = $this->report_model->coach_id($user_id);
	$info = $this->report_model->getinfo($coach_user_id);
	if(!empty($info)){
	echo json_encode(array('data'=>$info,'status'=>1));
	}
	else{
		echo json_encode(array('data'=>'No data','status'=>0));
	}
	die;
	
}

  public function stepchecknew(){
	$user_id = $this->input->post('user_id');
	$step_id = $this->input->post('stp_id');
	$infos = $this->report_model->stepcheckgetinfo($user_id,$step_id);
	if($infos==1)
	{
		echo json_encode(array('data'=>'Data Update Success','status'=>1));
	}
	else{
		echo json_encode(array('data'=>'Data Update Not Successfully','status'=>0));
	}
}

/*****************Get the created date of Phase 10-04-17 ***********/
function getstareddate()
{
	$user_id = $this->input->post('user_id');
	$group_id = $this->input->post('group_id');
	$phase_id = $this->input->post('phase_id');
	$reftypeid = $this->input->post('reftypeid');
	$f_status = $this->input->post('f_status');
	
	$infos = $this->report_model->getdetails($user_id,$group_id,$phase_id,$reftypeid,$f_status);

	if(!empty($infos)){
		echo json_encode(array('data'=>$infos,'status'=>1));
	}
	else{
		echo json_encode(array('data'=>'No data','status'=>0));
	}
}

public function save_manual_data()
{
	//printTestLog($_POST);
	$club_id = $this->input->post('club_id');
	$user_id = $this->input->post('user_id');
	$coach_id = $this->input->post('coach_id');
	$on_start = $this->input->post('on_start');
	$beats = $this->input->post('beats');
	$runtime = $this->input->post('runtime');
	$test_level = ($this->input->post('test_level'))?$this->input->post('test_level'):'A';
	$weight = $this->input->post('weight');
	$start_beats = $this->input->post('start_beats');
	$on_iant = $this->input->post('on_iant');
	$dateString = date('Y-m-d');
	$date = date('Y-m-d H:m:s');
	$date_timestamp =  strtotime($dateString);
	$check_exist = $this->report_model->check_manual_data($user_id,$coach_id,$date_timestamp);
	if(empty($check_exist)){
		$data = array(
			'club_id'=>$club_id,
			'user_id'=>$user_id,
			'coach_id'=>$coach_id,
			'runtime'=>$runtime,
			'test_level' => $test_level,
			'start_beats'=>$start_beats,
			'on_iant'=>$on_iant,
			'weight'=>$weight,
			'date_timestamp'=>$date_timestamp,
			'created'=>$date,
			'modified'=>$date,
		);
		$result = $this->report_model->add_manual_data($data); 
	}
	else
	{
		$data = array(
			'club_id'=>$club_id,
			'coach_id'=>$coach_id,
			'runtime'=>$runtime,
			'start_beats'=>$start_beats,
			'test_level' => $test_level,
			'on_iant'=>$on_iant,
			'weight'=>$weight,
			'date_timestamp'=>$date_timestamp,
			'modified'=>$date
		);
		$id = $check_exist->id;
		$this->db->where('user_id',$user_id);
		$this->db->where('id',$id);
		$this->db->update('t_save_manual_data',$data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE)
		{
			$result = $id;
		}
	}
	if(!empty($result))
	{
		$res = $this->report_model->get_last_test($result);
		if(isset($res['status']) && $res['status'] == 101)
		{
			$this->db->where('id',$result);
			$this->db->delete('t_save_manual_data');
			echo json_encode(array('data'=>$res['message'],'status'=>0));
		}
		else
		{
			echo json_encode(array('data'=>$result,'status'=>1));
		}
	}
	else
	{
		echo json_encode(array('data'=>'','status'=>0));
	} 
}

	 public function get_weight()
	 {
		 $user_id = $this->input->post('user_id');
		 $result = $this->report_model->get_user_weight($user_id);
		 if($result!=false){
			 echo json_encode(array('data'=>$result,'status'=>1));
		 }
		 else{
			 echo json_encode(array('data'=>'','status'=>0));
		 }
	 }
function getextracredit()
{
	$user_id = $this->input->post('user_id');
	$group_id = $this->input->post('group_id');
	$phase_id = $this->input->post('phase_id');
	$f_points = $this->input->post('f_points');
	$f_status = $this->input->post('f_status');
	$reftypeid = $this->input->post('reftypeid');
	$f_creditdttm = date('Y-m-d H:i:s');
	$f_crdttm = date('Y-m-d H:i:s');
	
	$this->load->model('report_model');
	$fields = array('f_userid'=>$user_id,'f_groupid'=>$group_id,'f_phaseid'=>$phase_id,'f_points'=>$f_points,'f_status'=>$f_status,'f_reftypeid'=>$reftypeid,'f_creditdttm'=>$f_creditdttm,'f_crdttm'=>$f_crdttm);
	
	$infos = $this->report_model->insert('t_training_points_achieved',$fields);

	 if(!empty($infos)){
		echo json_encode(array('data'=>$infos,'status'=>1));
	}
	else{
		echo json_encode(array('data'=>'No data','status'=>0));
	} 
}	 

}
