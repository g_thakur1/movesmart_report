<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report2 extends CI_Controller {

	 function __construct()
    {
        parent::__construct();
        
        $this->load->model('report_model');
		 $this->load->model('report_pdf_model');
       
    }
	public function index()
	{
		
		//$user_id = $this->input->post('user_id');
		
		//$data = $this->report_model->get_reporting_by_consult_type($user_id);
		
		//echo  json_encode($data);
		
	}
	
	/****** function to get movesmart level*****/
	public function movesmart()
	{
		$user_id = $this->input->post('user_id');
		
		$fitness_level= $this->report_model->get_fitness($user_id);
		if($fitness_level<=1)
		{
			$fitness_level=1;
		}
		else if($fitness_level>=7)
		{
			$fitness_level=7;
		}
		$flexbility_level = $this->report_model->get_flexibility($user_id);
		 if($flexbility_level<=1)
		{
			$flexbility_level=1;
		}
		else if($flexbility_level>=7)
		{
			$flexbility_level=7;
		}
		$strength_level = $this->report_model->get_strength($user_id);
		if($strength_level<=1)
		{
			$strength_level=1;
		}
		else if($strength_level>=7)
		{
			$strength_level=7;
		}
		if($fitness_level !=0 && $fitness_level !='')
		{
			
		$fitness = ($fitness_level*80)/100;
		}
		if($flexbility_level !=0 && $flexbility_level !='')
		{
		$flexbility = ($flexbility_level*10)/100;
		}
		if($strength_level !=0 && $strength_level !='' )
		{
		$strength = ($strength_level*10)/100;
		}
		$data['cardio_fitness'] = $fitness_level;
		$data['agility_flexbility'] = $flexbility_level;
		$data['strength'] = $strength_level;
		$data['movesmart_total_level']=round($fitness+$flexbility+$strength);
		$result['movesmart'] = $data;
		echo json_encode($result);
		die;
		
		
		
	}
	/****** function to get eat level*****/
	
	 public function eat()
	 {
		$user_id = $this->input->post('user_id');
		$body_compo = $this->report_model->get_body_composition($user_id);
		 if($body_compo<=1)
		{
			$body_compo=1;
		}
		else if($body_compo>=7)
		{
			$body_compo=7;
		}
		$data['body_composition'] = round($body_compo);
		$body_composition= $body_compo*40/100;
		//$data['body_composition'] =round($body_composition);
		$vis_fat = $this->report_model->get_visceral_fat($user_id);
		 if($vis_fat<=1)
		{
			$vis_fat=1;
		}
		else if($vis_fat>=7)
		{
			$vis_fat=7;
		}
		$data['viscreal_fat'] = $vis_fat;
		$viscreal_fat=$vis_fat*40/100;
		//$data['viscreal_fat'] =round($viscreal_fat);
		$choles = $this->report_model->get_cholestrol($user_id);
		 if($choles<=1)
		{
			$choles=1;
		}
		else if($choles>=7)
		{
			$choles=7;
		}
		$data['Cholesterol'] = $choles;
		$cholesterol=$choles*10/100;
		//$data['Cholesterol'] = round($cholesterol);
		$gluc = $this->report_model->get_glucose($user_id);
		 if($gluc<=1)
		{
			$gluc=1;
		}
		else if($gluc>=7)
		{
			$gluc=7;
		}
		$data['Glucose'] = $gluc;
		$glucose=$gluc*10/100;
		//$data['Glucose'] = round($glucose);
		$data['eat_total_level'] = round($body_composition+$viscreal_fat+$cholesterol+$glucose);
		$result['Eat'] = $data;
		echo json_encode($result);
	 }
 
 /******************************
 function for get mind switch
 ******************************/
 public function mind_switch()  
 {

  $user_id = $this->input->post('user_id');
  $vita_16 = $this->report_model->get_vita_sixteeen($user_id);
  if($vita_16<=1)
		{
			$vita_16=1;
		}
		else if($vita_16>7)
		{
			$vita_16=7;
		}
$data['vita_16']=round($vita_16);
 $except_vita_16 = $this->report_model->get_stress_sleep_mindfullness_level($user_id);
 $stress= $except_vita_16['stress'];

 if($stress<=1)
		{	
			$stress=1;
		}
		else if($stress>7)
		{
			$stress=7;
		}
		
		$data['stress'] =$stress;
		$sleep= $except_vita_16['sleep'];
	if($sleep<=1)
		{
			$sleep=1;
		}
		else if($sleep>7)
		{
			$sleep=7;
		}
$data['sleep'] =$sleep;
$mindfull= $except_vita_16['mindful'];
 if($mindfull<=1)
		{
			$mindfull=1;
		}
		else if($mindfull>7)
		{
			$mindfull=7;
		}
 $data['mindful'] =$mindfull;
 $stress= round(stress*10/100);
 $sleep= round($sleep*10/100);
 $mindfull = round($mindfull*10/100);
 $vita_16= round($vita_16*70/100);
 $watiwant       = $this->report_model->get_vita_sixteeen($user_id,'others');
 $data['energy'] = $watiwant['energy']; 
 $data['motivation'] = $watiwant['motivation']; 
 $data['resilience'] = $watiwant['resilience']; 
 $data['personalgoal'] = $this->report_model->personal_goal($user_id);
  
 $data['Mind_switch_total_level'] = $vita_16+$stress+$sleep+$mindfull;
    
  $result['Mind_Switch'] = $data; 
  echo json_encode($result);  
 
  
 }
 public function level_description()
 {
	  $type_id = $this->input->post('type_id');
	  $item_id = $this->input->post('item_id');
	  $level = $this->input->post('level');
	 $description = $this->report_model->get_level_description($type_id,$item_id,$level);
	if(!empty($description))
	{	$data = array('status'=>'Success','status_code'=>1,'description'=>$description);
		
	}
	else
	{	$data = array('status_code'=>0,'status'=>'No Data available');
		
	}
	echo json_encode($data);
 }
 public function eat_level()
 {
  
    $user_id = $this->input->post('user_id');
	
	//$body_composition  = $this->report_model->get_body_composition($user_id);
	$b_comp = $this->report_pdf_model->get_body_composition($user_id);
	$b_compsition_value=$b_comp['bmi'];
	$body_composition = $b_comp['body_composition'];
	$bmi_level = $b_comp['bmi_level'];
	$waist = $b_comp['waist'];
	$w_level = $b_comp['waist_level'];
	$fat_percentage = $b_comp['fat_percentage'];

	 if($body_composition<=1)
		{
			$body_composition=1;
		}
		else if($body_composition>=7)
		{
			$body_composition=7;
		}
	$data['body_composition'] = round($body_composition);
	
     $body_desc =  $this->report_model->get_level_description('2','26',round($body_composition));
    $visc_data =$this->report_pdf_model->get_visceral_fat($user_id);
    $visceral=$visc_data['visceral'];
   $vis_val=$visc_data['vis_value'];
     if($visceral<=1)
		{
			
			$visceral=1;
			
		}
		else if($visceral>7)
		{
			$visceral=7;
			
		}
		
	
    $data['viscreal_fat%'] =$visceral;
	$visceral_desc =  $this->report_model->get_level_description('2','67',$visceral);
    $chol_data= $this->report_pdf_model->get_cholestrol($user_id);

   $cholestrol=$chol_data['cholestrol'];
   $cholestrol_val=$chol_data['chol_value'];
 	if($cholestrol<=1)
		{
			$cholestrol=1;
		}
		else if($cholestrol>7)
		{
			$cholestrol=7;
		}
	
   $data['Cholesterol'] = $cholestrol;
    $cholestrol_desc = $this->report_model->get_level_description('2','8',$cholestrol);
   $glu_data= $this->report_pdf_model->get_glucose($user_id);
 $glucose=$glu_data['glucose'];
   $glucose_val=$glu_data['glu_value'];
   if($glucose<=1)
		{
			$glucose=1;
		}
		else if($glucose>7)
		{
			$glucose=7;
		}
		
   $data['Glucose'] =$glucose;
   $glucose_desc = $this->report_model->get_level_description('2','68',$glucose);
   
   if($body_composition !=0 && $body_composition !='')
		{			
		$body_compos = ($body_composition*40)/100;
		}
		if($visceral !=0 && $visceral !='')
		{
		$viscera = ($visceral*40)/100;
		}
		if($cholestrol !=0 && $cholestrol !='' )
		{
		$choles = ($cholestrol*10)/100;
		}
		if($glucose !=0 && $glucose !='' )
		{
		$gluco = ($glucose*10)/100;
		}
					 
		 $eat=round($body_compos+$viscera+$choles+$gluco);

   //$eat=round($data['body_composition']+$data['viscreal_fat%']+$data['Cholesterol']+$data['Glucose']);
   if($eat<=1)
		{
			$eat=1;
		}
		else if($eat>7)
		{
			$eat=7;
		}

   $data['eat_total_level'] = $eat;
    $data['eat_total_level_desc'] = $this->report_model->get_level_description('2','79',$eat);
    //$result['Eat'] = $data;
     $data['bmi'] = $b_compsition_value;
    $data['b_level'] = $bmi_level;
    // $data['waist'] = $waist;
    // $data['waist_level']=$w_level;
    // $data['fat_percentage'] = $fat_percentage;
    // $data['Glucose_val'] = $glucose_val;
    // $data['choles_val'] = $cholestrol_val;
    // $data['viscreal_val'] =$vis_val;
	   
  $find=array("{bmi_val}","{bmi}","{waist_val}","{waist}","{fat_val}","{fat_percentage}");
			$replace=array($b_compsition_value,$bmi_level,$waist,$w_level,$fat_percentage,$fat_percentage);
			$body_string=str_replace($find,$replace,$body_desc);
			$visceral_string=str_replace("{visceral}",$fat_percentage."%",$visceral_desc);
			$glucose_string=str_replace("<<glucose>>",$glucose_val,$glucose_desc);
			$data['body_string']=$body_string;
			$data['visceral_string']=$visceral_string;
			$data['glucose_string']=$glucose_string;
			$data['cholestrol_desc'] = $cholestrol_desc;
		
    echo json_encode($data);
 }	
 
}
