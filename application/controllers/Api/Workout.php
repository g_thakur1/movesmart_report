<?php 
//error_reporting(-1);
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Workout extends CI_Controller {
	public function __construct()
		{
		    parent::__construct();
			$this->load->model('Api/Workout_model');
			$this->load->library('JWT');
		}
		
	public function week_workout()
		{
			
			$user_id = $this->input->post('user_id');
			$program_type = $this->input->post('program_type');
			$program_id = $this->input->post('program_id');
			$action = $this->input->post('action');
			$workout = $this->input->post('week_data');
			//$week_data = json_decode(json_decode($workout));
			$week_data = json_decode($workout);
			$is_reporting = $this->input->post('is_reporting');
			
			$result = $this->Workout_model->week_workout($user_id,$week_data,$program_id,$program_type,$action);  
			if($result){
				echo json_encode(array('response'=> "data saved", 'status'=>1));
			}
			else{
				echo json_encode(array('response'=>'not saved','status'=>0));
			}
		}

	 
	}
?> 