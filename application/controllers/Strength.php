<?php

//error_reporting(-1);
defined('BASEPATH') OR exit('No direct script access allowed');

class Strength extends CI_Controller {

	 function __construct()
    {
        parent::__construct();
        
        $this->load->model('strength_model');
		$this->load->model('report_model');
        $this->load->model('report_pdf_model');
		 $this->load->model('Api/Saveprogram_model');
      

    }
	public function index()
	{

		$situp_val=0;
		$pushup_val=0;
		$qua_val=0;	
		$situp_score=0;
		$pushup_score=0;
		$qua_score=0;

		$data= $this->input->post(NULL, TRUE);	
	    $userId = $this->input->post('userId');
		$situp_val=$this->input->post('situp');
		$pushup_val=$this->input->post('pushup');
		$qua_val=$this->input->post('quadriceval');
		$post_data['clubid'] = $this->input->post('clubid');
		if($post_data['clubid']==''){
			$post_data['clubid'] = 0;
		}
		$machine_id = $this->input->post('machine_id');
		if($machine_id==''){
			$machine_id = 0;
		}
	    $program  = $this->input->post('r_strength_program_id');
		if($program==''){
			$program = 0;
		}
		$program_array = explode(",",$program);
		$program_id = $program_array[0];
		$post_data['program_type'] = $program_type = $program_array[1];
		if($post_data['program_type']==''){
			$post_data['program_type'] = 0;
		}
		$user_detail = $this->report_pdf_model->user_detail($userId);
		
		$coach_id = $this->strength_model->get_coach_id($userId);
		$week = $this->userweek($userId);
			if($program_type==1){

			        			$post_data['r_strength_program_free_id'] = $program_id;
			        		}
			else if($program_type==2){

			        			$post_data['r_strength_program_mix_id'] = $program_id;
			        		}
			else if($program_type==3){

			        			
									$post_data['r_strength_program_circuit_id'] = $program_id;
								
							} 
							$machines_array = explode(",",$machine_id);
							//print_r($machines_array);
							
							foreach($machines_array as $machine){
							
								$post = array('r_machine_id'=>$machine,
											'r_strength_pgmid'=>$program_id,
											'r_user_id'=>$userId,
											'f_creatdby'=>$coach_id,
											'created_on'=>date('Y-m-d h:i:s'),
											);
								$machine_insert_id = $this->strength_model->insert_machine($post);
								
							}
							
			$gender = $user_detail->gender;
				//for credits per activity//
				$age_weight_on_test = $this->report_pdf_model->user_age($userId);
				$age=$age_weight_on_test->age_on_test;
			$user_info = $this->report_model->get_personal_info($userId);
		
			$birthDate = $user_info->dob; //(y-m-d)
		    $birthDate = explode("-", $birthDate);
			
		    $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[1], $birthDate[2], $birthDate[0]))) > date("md")
			? ((date("Y") - $birthDate[0]) - 1)
			: (date("Y") - $birthDate[0]));
			
				$testdate = date('Y-m-d h:i:s');
				$c_date = date('Y-m-d h:i:s', strtotime('-3 month', strtotime($testdate)));
	
				$situp_score = $this->strength_model->get_strength_score($gender,$age,$situp_val,1);
				
				$pushup_score = $this->strength_model->get_strength_score($gender,$age,$pushup_val,2);

				$qua_score = $this->strength_model->get_strength_score($gender,$age,$qua_val,3);
				//echo $situp_score;
	
				$score = $situp_score.$pushup_score.$qua_score;
				$correct_level =$this->strength_model->get_strength_level($score);
				//echo $correct_level;
				//echo $correct_level;
								$post_data['f_situpval'] = $situp_val;
								$post_data['f_pushupval'] =$pushup_val;
								$post_data['f_quadriceval'] =$qua_val;
								$post_data['created_date'] = $testdate;
								$post_data['r_user_id'] = $userId;
								$post_data['coach_id'] = $coach_id;
								$post_data['strength_auto_or_manual_calculation'] = $score;
								

						$is_record = $this->strength_model->check_result($userId);

						$ins_id = $this->strength_model->insert_data($post_data);
					
						$in_data = array('strength_level' => $correct_level,
								'date' => $testdate,
								'r_user_id'=>$userId
								);
						$insert_id = $this->strength_model->insert_data_strength($in_data);
						
						$msg = array('message'=>'Test is Succesfully inserted.' ,'status'=>'11');
				
					echo json_encode($msg);
	}
	public function getstre()
	{   
		$userId=$this->input->post('userId');
		$data['strength']= $this->strength_model->check_result($userId);
		
		echo json_encode($data);
	}
	public function getmachineList()
	{   
	
		$clubid=$this->input->post('Clubid');
		$strength_machine= $this->strength_model->getlist($clubid);
	
		
		if(!empty($strength_machine)){
			
		echo json_encode(array('response'=>$strength_machine,'status'=>1));
		}
		else{
			echo json_encode(array('response'=>'no record','status'=>0));
		}
	}
	
	public function getstrengthProg()
	{
		
	//	$clubid = $this->input->post('clubid');
		$type = $this->input->post('type');

			if($type==1){
				
					$strength_Prog = $this->strength_model->getProlist($clubid,$type);

			}
			else if($type==2){
				
					$strength_Prog = $this->strength_model->getProlist_where($clubid);

			}

		if(!empty($strength_Prog)){

		echo json_encode(array('response'=> $strength_Prog, 'status'=>1));	

		}else{

		echo json_encode(array('response'=>'no record','status'=>2));		
		}

	}
	
	/****************user week strength***********/
	
	public function userweek($userId){
		
		
		$record = $this->strength_model->check_week($userId);
		$data['r_user_id'] = $userId;
		 if(!empty($record)){
			
			$t=$record->week_end_date;
				 $curr_day = date("D",strtotime($t));
				if($curr_day=='Mon')
				{
					 $data['week_start_date']=$start_date = date('Y-m-d');
					 $data['week_end_date'] = date('Y-m-d',strtotime('+6 day', strtotime($start_date)));
				}
				else{
					
					
					 $data['week_start_date'] =$start_date= date('Y-m-d', strtotime('next monday', strtotime($t)));
					 $data['week_end_date'] = date('Y-m-d',strtotime('+6 day', strtotime($start_date)));
				}
				$week = $record->week;
				$data['week']=$week+1;
			//	$this->db->where('r_user_id',$userId);
				//$this->db->update('t_strength_user_test_week',$data);
				$this->db->insert('t_strength_user_test_week',$data);
			
		}
		else{
				$t=date('d-m-Y');
				 $curr_day = date("D",strtotime($t));
				if($curr_day=='Mon')
				 {
					 $data['week_start_date']=$start_date = date('Y-m-d');
					 $data['week_end_date'] = date('Y-m-d',strtotime('+6 day', strtotime($start_date)));
				} 
			 	else{
					
					$date = date('Y-m-d');
					$start_date= date('Y-m-d', strtotime('next monday', strtotime($date)));
					 $data['week_start_date'] = date('Y-m-d', strtotime('next monday', strtotime($date)));
					 $data['week_end_date'] = date('Y-m-d',strtotime('+6 day', strtotime($start_date)));
				}  
				$data['week']=1;
				$this->db->insert('t_strength_user_test_week',$data);
		}
		
		
	}
	
	
	
	
	
	/**end**************/
	
	
	
	
	
	
	
	public function updateStrengthUserTest()
	{
					$data = array();
					$data['r_strength_user_test_type'] = $type = $this->input->post('type');			
							
			 		$strength_user_test_id = $this->input->post('strength_user_test_id');
			        $r_user_id = $this->input->post('r_user_id');
			        $data['coach_id'] = $this->input->post('coach_id');
			        $data['clubid'] = $this->input->post('clubid');
			      
					$program_free_id  = $this->input->post('r_strength_program_free_id');
			        $r_strength_user_test_type = $this->input->post('r_strength_user_test_type');
											
			        		if($type==1){

			        			$data['r_strength_program_free_id'] = $program_free_id;
			        		}
			        		else if($type==2){

			        			$data['r_strength_program_mix_id'] = $program_free_id;
			        		}
			        		else if($type==3){

			        			
									$data['r_strength_program_circuit_id'] = $program_free_id;
								
							} 
				$result = $this->strength_model->update_Strength_Usertest($r_user_id,$strength_user_test_id,$data);
				if($result == true){
						echo json_encode(array('response'=>'update successfully'));	
				}else{
					
					echo json_encode(array('response'=>'no record'));	
					
				}
						
	 	}

	 public function get_userData(){
		
	 	$id = $this->input->post('user_id');
		$week = $this->input->post('week');
		$week_data = $this->strength_model->check_week($id,$week);
	
		$start_date = $week_data->week_start_date;
		$end_date = $week_data->week_end_date;
	 	$result = $this->strength_model->userId_details($id,$week,$start_date,$end_date);
		$user = $this->strength_model->userdet($id,$week);
		
	 		if($result == true){
						echo json_encode(array('response'=>$result,'userinfo'=>$user,'status'=>1));	
				}else{
					
					echo json_encode(array('response'=>'' ,'status'=>0));	
					
				}

	 }


	 public function Get_blockTable(){

	 		$id = $this->input->post('user_id');
	 		
	 		$result = $this->strength_model->strength_mch_data($id);
	 		//echo '<pre>';print_r($id);
	 		if($result == true){
						echo json_encode(array('response'=>$result ,'status'=>1));	
				}else{
					
					echo json_encode(array('response'=>'' ,'status'=>0));	
					
				}


	 }	
	 public function str_progId(){

	 		
	 		$result = $this->strength_model->strength_proId();
	 				
	 		if(!empty($result)){
					echo json_encode(array('response'=>$result ,'status'=>1));	
				}else{
					
					echo json_encode(array('response'=>'' ,'status'=>0));	
					
				}

	 }  
	 
	 
	 public function getuser()
	 {
		 $user_id = $this->input->post('user_id');
		 $result = $this->strength_model->get_user_detail($user_id);
	 				
	 		if(!empty($result)){
					echo json_encode(array('response'=>$result ,'status'=>1));	
				}else{
					
					echo json_encode(array('response'=>'' ,'status'=>0));	
					
				}
		 
	 }
	 
	 
	 
	
}

?>
	