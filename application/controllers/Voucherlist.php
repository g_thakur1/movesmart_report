<?php 
error_reporting(1);
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Voucherlist extends CI_Controller {
	 public function __construct()
		{
		    parent::__construct();
			$this->load->model('voucher_model');
			$this->load->model('Report_model');
		}

//***********************************************************************//
//*******************function used for voucher list**********************//
//***********************************************************************//
	      public function vlist()
			 { 
			 	$data = $this->voucher_model->voucherlist();
			 	if(!empty($data))
			 	{
			 	echo json_encode(array('status'=>1,'data'=>$data));
				}
				else
				{
				echo json_encode(array('status'=>0,'data'=>'No list found!'));	
				}
			 }
			 
//***********************************************************************//
//*******************function used for coach list**********************//
//***********************************************************************//
			public function coachList()
			 { 
				$user_id = $this->input->post('user_id');
				$club_id = $this->input->post('club_id');
				$user_typeId = $this->input->post('user_typeId');
				if($user_typeId == 9)
				{
				$data = $this->voucher_model->coachListAll($user_id,$club_id,$user_typeId);
				}
				else if($user_typeId == 1)
				{
				$data = $this->voucher_model->coachListOne($user_id,$club_id,$user_typeId);
				}
			 	if(!empty($data))
			 	{
			 	echo json_encode(array('status'=>1,'data'=>$data));
				}
				else
				{
				echo json_encode(array('status'=>0,'data'=>'No list found!'));	
				}
			 }

			 public function strength_progWeek(){

			    $id = $this->input->post('r_strength_pgmid');
			    $r_week = $this->input->post('r_week');
			   
			    $result = $this->voucher_model->program_week($id);
			    if(!empty($result)){
			          echo json_encode(array('response'=>$result ,'status'=>1));  
			        }else{
			          
			          echo json_encode(array('response'=>'' ,'status'=>0)); 
			          
			        }

			  }

			  public function save_strengthProg(){

			  	$user_id = $this->input->post('user_id');
			  	$trainingsweek = $this->input->post('trainingsweek');
			  	$series = $this->input->post('series');
			  	$herh = $this->input->post('herh');
			  	$tijd = $this->input->post('tijd');
			  	$kracht = $this->input->post('kracht');
			  	$rust = $this->input->post('rust');

			  				$data = array(
				
						'user_id' => $user_id, 	
						'trainingsweek' => $trainingsweek, 	
						'series' => $series, 	
						'herh' => $herh, 	
						'tijd' => $tijd, 	
						'kracht' => $kracht,
						'rust' => $rust 	
							);

			  	$result = $this->voucher_model->insert_data_strength($data);	
		
				$msg = array('message'=>'data Succesfully inserted.' ,'status'=>'1');
				
					echo json_encode($msg);

			  }
			   /******************************
			  Function to save device id of the coach
			  **********************************/
			  public function addupdateDeviceIdforcoach(){
				  
				$user_id = $this->input->post('user_id');
				$dev_id = $this->input->post('device_id');
				if(isset($dev_id))
				{
					$data['device_id'] = $this->input->post('device_id');
				}
				else{
				$data['device_id'] = 0;	
				}
			  	$data['device_type'] = $this->input->post('device_type');
				$personal['device_version'] = $this->input->post('device_version');
				$personal['app_version'] = $this->input->post('app_version');
				$this->db->where('user_id',$user_id);
				 $this->db->update('t_users',$data);
				
					 if ($this->db->trans_status() === TRUE)
					{
						$this->db->where('r_user_id',$user_id);
						$this->db->update('t_personalinfo',$personal);
						echo json_encode(array('status'=>1,'msg'=>'value updated'));
					}
					else{
						echo json_encode(array('status'=>0,'msg'=>'value not updated'));
					}
			  }
			  /******************************
			  Function to get deviceid
			  *******************************/
		public function getDeviceId(){
				 
				$coach_id = $this->input->post('coach_id');
				$client_id = $this->input->post('user_id');
				$data = $this->voucher_model->getdeviceid($coach_id);
				$client_info = $this->Report_model->get_user($client_id);
				//print_r(client_info);
				//die;
					 if ($data!=false)
					{
						$device_id = $data->device_id;
						$device_type = $data->device_type;
						$message = $client_info->first_name." ".$client_info->last_name." has signed up for a coach call.";
						if($device_type=='iOS')
						{
							$result = $this->send_fcm_notify($device_id, $message);
							//$this->Iphone_Notification($message,$device_id);
						}
						else if($device_type=='Android'){
							$result = $this->send_fcm_notify($device_id, $message);
							//echo $data1;
						}
						echo json_encode(array('status'=>1,'msg'=>'Success','data'=>$data,'is_send'=>$result));
					}
					else{
						echo json_encode(array('status'=>0,'msg'=>'no record'));
					}
			  }
			  
			  	/**************************************************
				 Function to Notify User for Iphone alerts 
				**************************************************/ 
				public function Iphone_Notification($message,$device_id)
				 {
				 	
							$payload['aps'] = array('alert' => $message);
							 $payload = json_encode($payload);
							 $apnsCert = './Certificate5.pem';
							 $streamContext = stream_context_create();
							 stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
							$apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $streamContext);
							 //$apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device_id)) . chr(0) . chr(strlen($payload)) . $payload;
							 $apnsMessage = chr(0) . pack('n', 32) . pack('H*', $device_id) . pack('n', strlen($payload)) . $payload;
							// print_r($apnsMessage);
							// die;
							fwrite($apns, $apnsMessage, strlen($apnsMessage));
							//socket_close($apns);
							 fclose($apns);
				   
				   
				 }
				 
				 
	
				 /************************************************
				 function is used for push notification andorid
				***********************************************/   
	 				public function send_fcm_notify($device_id, $message) 
				 {
					
				  //echo $device_id;
				 /*  $device_id = 'faoVI53OLPs:APA91bEMnDLdMWD-USYr7ouPO6QnKWA-ysTkYMJb8wXIzgW2_mozTZBXGewwD3eLVKyaTMuPAtpRDBs0Su8RYTHB2f'; 
				 */
				/* define("GOOGLE_API_KEY", "AAAAhbXWWNY:APA91bGePWCDg9w7mJOajh63ciOudNhozbiJJlpMDQ6Y116VkkpI1WJHM-EeELjqUhgnmoLCbIlpUS7nHfuyDVLbAX0RZTH3dkg55FDnSt7PnJowZHH5Fo0esy8OA7Lu_ZZYixBa_wqg");*/
				 
				 // define("GOOGLE_API_KEY", "AIzaSyBR2ItcLh5ypzQBV1GasemSrbn_aWfk580");
				 	define("GOOGLE_API_KEY", "AIzaSyC9B08h2ROJ8m4V-1NS26aLi_k5-UK3iak"); 
				  define("GOOGLE_GCM_URL", "https://fcm.googleapis.com/fcm/send");
			
					$fields = array('to' => $device_id,
								'priority'     => "high",
								'notification' => array( "title" => "Movesmart", "body" => $message),
								'data' => array("message" =>$message),
							);
				  
						$headers = array(
				   'Content-Type: application/json',
							'Authorization: key=' . GOOGLE_API_KEY 
						);
				  $ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, GOOGLE_GCM_URL);
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
				  
						$result = curl_exec($ch);
						if ($result === FALSE) {
							die('Problem occurred: ' . curl_error($ch));
						}
				  
						curl_close($ch);

				 return $result;

					}
/**************************************************
		Function to check loginreffield_id in database
	**************************************************/
	function check_loginreffield()
	{
		$loginreffield = $this->input->post('loginreffield');
		$data = $this->voucher_model->check_loginreffield($loginreffield);
		
		if($data != false)
		{
			$email = $data->email;
			$pass = $data->password;
			$msg = array('message'=>'This id already exists.' ,'email'=>$email,'password'=>$pass,'status'=>'0');
			echo json_encode($msg);
		}
		else
		{
			$msg = array('message'=>'This id not exists.' ,'status'=>'1');
			echo json_encode($msg);
		}

	}

function check_DOB()
	{
		$email = $this->input->post('email');
		$data = $this->voucher_model->check_dobuser($email);
		
		if($data != false)
		{
			$email = $data->email;
			$pass = $data->password;
			$msg = array('message'=>'This id already exists.' ,'dob'=>$data->dob,'email'=>$data->email,'password'=>$data->password,'status'=>'1');
			echo json_encode($msg);
		}
		else
		{
			$msg = array('message'=>'This id not exists.' ,'status'=>'0');
			echo json_encode($msg);
		}
		
	}		
				
 }
 ?>