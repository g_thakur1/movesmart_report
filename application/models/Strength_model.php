<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//error_reporting(-1);
class Strength_model extends CI_Model {
/***********for mind switch insertion api*******/
public function insert_data($post_data)
{
	
	 $this->db->insert('t_strength_user_test', $post_data);
   $insert_id = $this->db->insert_id();

   return  $insert_id;
	
}
public function insert_data_strength($in_data)
{
	 $this->db->insert('t_user_strength_level', $in_data);
   $insert_id = $this->db->insert_id();

   return  $insert_id;
	
}
public function check_result($userId)
{
	 $this->db->select('*');
	 $this->db->from('t_strength_user_test');
	 $this->db->where('r_user_id',$userId);
	 //$this->db->join('t_strengthpgm_map_machine','t_strength_user_test.r_user_id=t_strengthpgm_map_machine.r_user_id','inner');
	 $this->db->order_by('strength_user_test_id','DESC');
	$query = $this->db->get();
	$result = $query->row();
	if(!empty($result))
	{
		$program_type= $result->program_type;
		if($program_type==1){
			$program_id= $result->r_strength_program_free_id;
		}
			else if($program_type==2){
				$program_id= $result->r_strength_program_mix_id;
			}
			else if($program_type==3){
				$program_id= $result->r_strength_program_circuit_id;
			}
		$result->machine_map= $this->get_machine_id($userId,$program_id);
		$result->programs = $program_id.','.$program_type;
		return $result;
	}
	else
	{
		return false;
	}
	
}
public function get_machine_id($userId,$program_id)
	{
	 $this->db->select('r_machine_id');
	 $this->db->from('t_strengthpgm_map_machine');
	 $this->db->where('r_user_id',$userId);
	 $this->db->where('r_strength_pgmid',$program_id);
	$query = $this->db->get();
	$result = $query->result();
	if(!empty($result))
	{	
		return $result;
		
	}
	else
	{
		return false;
	}
}



public function get_strength_score($gender,$age,$val,$type)
{
	 $this->db->select('r_level');
	 $this->db->from('t_strength_level');
	 $this->db->where('r_gender',$gender);
	 $this->db->where('r_type',$type);
	 $this->db->where('age_min <=',$age);
	$this->db->where('age_max >=',$age);
	$this->db->where('f_min <=',$val);
	$this->db->where('f_max >=',$val);
	$query = $this->db->get();
	$result = $query->result();
	if(!empty($result))
	{
		
		return $result[0]->r_level;
		
		
	}
	else
	{
		return false;
	}
	
}
public function get_strength_level($score)
	{
		 $this->db->select('level');
		 $this->db->from('t_strength_correct_level');
		// $this->db->like('fscore',$score);
		$this->db->like('score', $score);
		$query = $this->db->get();
		$result = $query->result();

		if(!empty($result))
		{
			foreach($result as $score_level)
			{
			return $score_level->level;
			}
		}
		else
		{
			return false;
		}
		
	}
public function check_strength_level_record($user_id,$date)
	{
			 $this->db->select('*');
			 $this->db->from('t_user_strength_level');
			 $this->db->where('r_user_id',$user_id);
			 $this->db->order_by('t_user_strength_level_id','desc');
			$query = $this->db->get();
			$result = $query->result();
			if(!empty($result))
			{
				$data['dt'] = $result[0]->date;
				$data['id'] = $result[0]->t_user_strength_level_id;
				
				return $data;
			}
			else
			{
				return false;
			}
		
	}
public function get_coach_id($userId)
{
	$this->db->select('r_coach_id');
	$this->db->from('t_coach_member');
	$this->db->where('r_user_id',$userId);
	$query = $this->db->get();
	$result = $query->row();

	if(!empty($result)){
		return $result->r_coach_id;
	}
	else{
		return false;
	}
	
	
	
}
public function getlist($clubid)
{
	$this->db->select('*');
	$this->db->from('t_strength_machine');
	$this->db->where('clubid',$clubid);
	$query = $this->db->get();
	$result = $query->result();

	if(!empty($result)){
		return $result;
	}
	else{
		return false;
	}
	
	
	
}

		public function getProlist($clubid,$type)
		{
			$this->db->select('*');
			$this->db->from('t_strength_program');
			//$this->db->where('clubid',$clubid);
			$this->db->where_in('type',$type);
			$query = $this->db->get();
			$result = $query->result();

			if(!empty($result)){
				return $result;
			}
			else{
				return false;
			}
			
		}
		public function getProlist_where($clubid)
		{

			$type = array(2,3);
		$this->db->select('*');
		$this->db->from('t_strength_program');
		//$this->db->where('clubid',$clubid);
		$this->db->where_in('type',$type);
		$query = $this->db->get();
		$result = $query->result();

			if(!empty($result)){
				return $result;
			}
			else{
				return false;
			}
			
		}

	public function insert_machine($post){
		$this->db->insert('t_strengthpgm_map_machine', $post);
		$insert_id = $this->db->insert_id();

		return  $insert_id;
	}
		public function update_Strength_Usertest($user_id,$strength_user_test_id,$data)
		{

			$this->db->where('strength_user_test_id',$strength_user_test_id);
			$this->db->where('r_user_id',$user_id);
			$this->db->update('t_strength_user_test',$data);
			 
			return true;
		}

		public function userId_details($id,$week,$start_date,$end_date){

			
			$this->db->select('*');
			$this->db->from('t_save_strength_program');
			//$this->db->query("selecty * from t_strength_machine where strength_machine_id IN(select * from ));
			$this->db->join('t_strength_machine', 't_save_strength_program.machine_id=t_strength_machine.strength_machine_id', 'left');
			//$this->db->join('t_bolck_weight', 't_save_strength_program.machine_id = t_bolck_weight.strength_machine_id', 'left');
			 $this->db->where('t_save_strength_program.user_id',$id);
			 $this->db->where('t_save_strength_program.week',$week);
			 $this->db->where('t_save_strength_program.training_date >= ',$start_date);
			 $this->db->where('t_save_strength_program.training_date <= ',$end_date);
			 $query = $this->db->get(); 
			
			$result = $query->result();
			  
			if(!empty($result)){
				foreach($result as $result_machine){
					$machine = $result_machine->machine_id;
					$pin = $result_machine->pin;
					
					$weight = $this->GetWeightPin($machine,'weight',$pin);					
					 $data[$machine][] = array('machine_name' => $result_machine->machine_name,
												'weight' => $weight,
												'pin' => $result_machine->pin,
												'training_date' => $result_machine->training_date,
												'machine_id' => $result_machine->machine_id);
					 
					 
					
				}
				
				 foreach($data as $key=>$redata){
					$new_array[] = array('machine_id'=>$key,
										'machine_name'=>$redata[0]['machine_name'],
										'training_date'=>$redata[0]['training_date'],
										'training_data'=>$redata
										);
					
				} 
				
				return $new_array;
			}
			else{
				return false;
			}

		}


		public function userdet($id,$week){
			
			$this->db->select('*');
					$this->db->from('t_users'); 
					$this->db->where('user_id',$id);
					//$this->db->join('t_strength_user_test_week', 't_users.user_id = t_strength_user_test_week.r_user_id', 'left'); 
					$query = $this->db->get(); 
					
					$result = $query->row();
					$week_dates = $this->get_user_week_and_id($id,$week);
					$result->week_start_date = $week_dates->week_start_date;
					$result->week_end_date = $week_dates->week_end_date;
					$result->week = $week_dates->week;
					
					if(!empty($result)){
						
						return $result;
					}
					else{
						return false;
					}
			
			
			
		}
		
		
		public function strength_mch_data($id)
		{
			$this->db->select('*');
			$this->db->from('t_save_strength_program');
			$this->db->join('t_bolck_weight', 't_save_strength_program.machine_id = t_bolck_weight.strength_machine_id', 'left'); 
			$this->db->where('t_save_strength_program.user_id',$id);
			$query = $this->db->get(); 
			
			$result = $query->result();
			
			if(!empty($result)){
				return $result;
			}
			else{
				return false;
			}
			
			
		}
		public function strength_proId(){


			$this->db->select('*');
			$this->db->from('t_strength_program');
			$query = $this->db->get();
			$result = $query->result();
			//$data = array();
			foreach($result as $var){
				
				
			$prog_id = $var->strength_program_id;
			$week = $this->weekdata($prog_id);

			$var->week=$week;
			$data[] = $var;


		}
		
		return $data;
			
		}

		public function weekdata($prog_id)
		{

			$this->db->select('*');
		
			$this->db->from('t_strength_program_week');
			$this->db->where(r_strength_pgmid, $prog_id);
			$query = $this->db->get();
			$result = $query->result();
				
			if(!empty($result)){
				//print_r($result);
				return $result;
			}
			else{
				return false;
			}
		}
		
		
		public function check_week($user_id,$week=''){
			$this->db->select('*');
			$this->db->from('t_strength_user_test_week');
			$this->db->where('r_user_id', $user_id);	
			if($week!=''){
				$this->db->where('week',$week);
				$this->db->order_by('strength_user_test_week_id','desc');
				$this->db->limit(1);
			}
			$query = $this->db->get();
			$result = $query->row();
				
			if($query->num_rows()>0){
				//print_r($result);
				return $result;
			}
			else{
				return false;
			}
			
		}
		public function get_user_detail($user_id){
			$this->db->select('*');
			$this->db->from('t_users');
			$this->db->where('user_id', $user_id);
			$query = $this->db->get();
			$result = $query->row();
				
			if($query->num_rows()>0){
				//print_r($result);
				$user_week = $this->getusersavedweeklatest($user_id);
				$week = $user_week->week;
				$result->week_no = $week;
				return $result;
			}
			else{
				return false;
			}
			
		}
		
		public function getusersavedweeklatest($user_id)
		{
			$this->db->select('*');
			$this->db->from('t_strength_user_test_week');
			$this->db->where('r_user_id',$user_id);
			$this->db->order_by('strength_user_test_week_id', 'desc');
			$this->db->limit(1);
			$query = $this->db->get(); 
			
			$result = $query->row();
			
			if(!empty($result)){
				return $result;
			}
			else{
				return false;
			}
			
			
		}
		
		public function get_user_week_and_id($user_id,$week){
			$this->db->select('*');
			$this->db->from('t_strength_user_test_week');
			$this->db->where('r_user_id', $user_id);
			$this->db->where('week', $week);
			$query = $this->db->get();
			$result = $query->row();
				
			if($query->num_rows()>0){
				//print_r($result);
				return $result;
			}
			else{
				return false;
			}
			
		}
		
		 public function GetWeightPin($machine,$key,$pin){
			$this->db->select($key);
			$this->db->from('t_bolck_weight');
			$this->db->where('strength_machine_id',$machine);
			$this->db->where('position',$pin);
			$query = $this->db->get(); 
			$result = $query->row();
			
			if(!empty($result)){
			
				return $result->$key;
			}
			else{
				return false;
			}
		} 
		

   }

?>