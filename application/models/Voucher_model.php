<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Voucher_model extends CI_Model {
 
 
   
     		 public function insertVoucher ($data)
			 	{
						$this->db->insert('t_voucher', $data);
					    $last_id=$this->db->insert_id();
						return $last_id;   				
				}
				public function voucherGetdata($user_id)
				{
					$this->db->select('*');			
					$this->db->from('t_users');
					$this->db->where('user_id', $user_id);
					$q=$this->db->get();			
					$data=$q->row();
					 return $data;
				}
				/**************to check the record if credit exist*************/
				public function get_save_credits($user_id)
				{
					
					$this->db->select('*');			
					$this->db->from('t_save_credits');
					$this->db->where('user_id', $user_id);
					$this->db->order_by('id','DESC');
					$query=$this->db->get();			
					$data=$query->row();
				
					if(!empty($data)){
					 return $data;
					}
					else
					{
						return false;
					}
				}
				/****************to save credits************/
				 public function save_credit($data)
			 	{
						$this->db->insert('t_save_credits', $data);
					    $last_id=$this->db->insert_id();
						return $last_id;   				
				}
				/****************to update credits************/
				public function update_credit($data,$id)
			 	{
					$this->db->where('id',$id);
					$this->db->update('t_save_credits', $data);
					
				}
				/****************to voucher list************/
				public function voucherlist()
			 	{
					$this->db->select('*');			
					$this->db->from('t_voucherlist');
					$query=$this->db->get();			
					if($query->num_rows() > 0)
					{
						$data = $query->result();
						return $data;
					}
					else
					{
						return false;
					}
					
				}
				/****************to ALL Coach list************/
				public function coachListAll($user_id,$club_id,$user_typeId)
			 	{
					$in = array(1,9);
					$this->db->select('*');			
					$this->db->from('t_users');
					$this->db->where_in('r_usertype_id',$in);
					$this->db->where('r_club_id',$club_id);
					$query=$this->db->get();					
					if($query->num_rows() > 0)
					{
						$data = $query->result();
						return $data;
					}
					else
					{
						return false;
					}
					
				}
				/****************to Single  coach list************/
				public function coachListOne($user_id,$club_id,$user_typeId)
			 	{
					$this->db->select('*');			
					$this->db->from('t_users');
					//$this->db->where('r_club_id',$club_id);
					$this->db->where('user_id',$user_id);
					//$this->db->or_where('r_usertype_id',$user_typeId);
					$query=$this->db->get();			
					if($query->num_rows() > 0)
					{
						$data = $query->result();
						return $data;
					}
					else
					{
						return false;
					}
					
				}
				
				Public function get_points($user_id,$f_level,$activityid){
					$this->db->select('*');			
					$this->db->from('t_heartrate_zone_activity');
					$this->db->where('fitness_level',$f_level);
					$this->db->where('r_user_id',$user_id);
					$this->db->where('activity_id',$activityid);
					$this->db->order_by('r_user_test_id','desc');
					$query=$this->db->get();			
					if($query->num_rows() > 0)
					{
						$data = $query->row();
						$points_rate = (($data->points_rate)!='')?$data->points_rate:0;
						return $points_rate;
					}
					else
					{
						return 0;
					}
					
				}
				
				public function getUserCredits($user_id,$group_id,$phase_id,$voucherpoints)
				{
					$this->db->select('*');			
					$this->db->from('t_training_points_achieved');
					$this->db->where('f_userid',$user_id);
					$this->db->where('f_groupid',$group_id);
					$this->db->where('f_phaseid',$phase_id);
					$this->db->where('f_status',1);
					$query=$this->db->get();
					$point = 0;
					if($query->num_rows() > 0)
					{
						$data = $query->result();
						foreach($data as $dat)
						{
							if($voucherpoints > 0)
							{
								$existingPoints = $dat->f_points;
								if($existingPoints < $voucherpoints)
								{
									$status = 2;
								}
								else
								{
									$status = 1;
								}
								//echo $existingPoints." ".$newpoints."<br/>";
								$voucherpoints = $voucherpoints-$existingPoints;
								
								$datR = array('f_status'=>2);
								$this->db->where('f_pointachievedid',$dat->f_pointachievedid);
								$this->db->update('t_training_points_achieved',$datR);
							}
						}
						return 1;
					}	
					else
					{
						return 0;
					}
					
				}

				 public function program_week($id){

				      $this->db->select('*');
				    
				      $this->db->from('t_strength_program_week');
				      $this->db->where(r_strength_pgmid, $id);
				      $query = $this->db->get();
				      $result = $query->result();
				        
				      if(!empty($result)){
				        //print_r($result);
				        return $result;
				      }
				      else{
				        return false;
				      }

       
   				}
				
				public function insert_data_strength($data)
					{
					  $insert_data = $this->db->insert('t_strength_program_change', $data);
					   
					   return  $insert_data;
						
					}
					/***************get device id*********/
					public function getdeviceid($coach_id){
						 $this->db->select('*');
				      $this->db->from('t_users');
				      $this->db->where('user_id',$coach_id);
				      $query = $this->db->get();
				      $result = $query->row();
				        
				      if(!empty($result)){
				        //print_r($result);
				        return $result;
				      }
				      else{
				        return false;
				      }
					}
					/**************************************************
		Function to check loginreffield_id in database
	**************************************************/
	function check_loginreffield($loginreffield)
	{
		$this->db->select('*');			
		$this->db->from('t_users');
		$this->db->where('loginreffield_id',$loginreffield);
		$query=$this->db->get();			
		if($query->num_rows() > 0)
		{
			$data = $query->row();
			//print_r($data);die;
			return $data;
		}
		else
		{
			return false;
		}
		
	}
		
	function check_dobuser($email)	
		{
			
				$this->db->select('*');			
				$this->db->from('t_users');
				$this->db->where('email',$email);
				$query=$this->db->get();			
				if($query->num_rows() > 0)
				{
					$data = $query->row();

					$user_id =$data->user_id;
						$this->db->select('dob');			
						$this->db->from('t_personalinfo');
						$this->db->where('r_user_id',$user_id);
						$query2=$this->db->get();
						$result = $query2->row();		
					//print_r($data);die;
					$email = $data->email;
					$pass = $data->password;
					$result->email = $email;
					$result->password = $pass;
					return $result;
				}
				else
				{
					return false;
				}
				
			}	
				
					
			}
		?>