<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report_pdf_model extends CI_Model {
/*********************************
fetch user detail
***********************************/
public function user_detail($user_id)
	{
		$this->db->select('*');
		$this->db->from('t_users');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		$result = $query->row();
		if(!empty($result))
		{
			
		return $result;
		}
		else{
			return false;
		}
	}
	public function club_detail($club_id)
	{
		$this->db->select('*');
		$this->db->from('t_clubs');
		$this->db->join('t_group', 't_clubs.group_id=t_group.group_id', 'left');
		$this->db->where('club_id',$club_id);
		$query = $this->db->get();
		$result = $query->row();
		if(!empty($result))
		{
			
		return $result;
		}
		else{
			return false;
		}
	}
	public function get_coach_id($user_id)
	{
		$this->db->select('*');
		$this->db->from('t_bodycomposition_test');
		$this->db->where('r_user_id',$user_id);
		$this->db->order_by('bodycomposition_test_id','desc');
		$query = $this->db->get();
		$result = $query->row();
		if(!empty($result))
		{
			
		return $result->created_by;
		}
		else{
			return false;
		}
	}
	public function coach_image($coach_id)
	{
		$this->db->select('*');
		$this->db->from('t_personalinfo');
		$this->db->where('r_user_id',$coach_id);
		$query = $this->db->get();
		$result = $query->row();
		if(!empty($result))
		{
			
		return $result->userimage;
		}
		else{
			return false;
		}
	}
public function user_test_type($user_id)
	{
		$this->db->select('*');
		$this->db->from(' t_test_overall_result_level');
		$this->db->where('r_user_id',$user_id);
		$query = $this->db->get();
		$result = $query->result();
		
		$count = count($result);
		
		return $count;  
	}
	public function user_age($user_id)
	{
		$this->db->select('*');
		$this->db->from('t_user_test_parameter');
		$this->db->where('r_user_id',$user_id);
		$this->db->order_by('user_test_parameter_id','DESC');
		$query = $this->db->get();
		$result = $query->row();

		if(!empty($result))
		{
			
		return $result;
		}
		else{
			return false;
		}  
	}
	public function get_point_speed($age_on_test,$fitness,$gender)
	{
		
		$this->db->select('*');
		$this->db->from('t_fitlevel');
		 $this->db->where('f_agemin <=',$age_on_test);
		$this->db->where('f_agemax >=',$age_on_test);
		$this->db->where('f_fitlevel',$fitness);
		$this->db->where('f_gender',$gender);
		$query = $this->db->get();
		$result = $query->row();
		
		if(!empty($result))
		{
			
		return $result->f_point_speeda;

		}
		else{
			return false;
		}
	}



	public function get_activity($fitness,$factivity,$point_speed)
	{
	
		$fpcsp = 'f_pcsp'.$fitness;
		$fdeflcoef = 'f_deflcoef'.$fitness;
		$this->db->select($fpcsp.','.$fdeflcoef.',f_minhrcond,f_maxhrcond');
		$this->db->from('t_activitypoints');
		$this->db->where('f_min_level <=',$fitness);
		$this->db->where('f_max_level >=',$fitness);
		 $this->db->like('f_activity',$factivity);
		 $this->db->order_by('f_activitypoint_id','DESC');
		$query = $this->db->get();
		$result = $query->row();
		
		if(!empty($result))
		{
		$fpcs=$result->$fpcsp;
		$fdef=$result->$fdeflcoef;
		$f_minhrcond=$result->f_minhrcond;
		$f_maxhrcond=$result->f_maxhrcond;
		
	//	$f_minhrcond=round(($iant_hr*$minhr)/100);
		//$f_maxhrcond=round(($iant_hr*$maxhr)/100);
		   
		$activity_point =round((($point_speed*$fpcs)/100),1); 
		
		$data=array('fpcsp'=>$fpcs,
					'f_deflcoef'=>$fdef,
					'f_minhrcond'=>$f_minhrcond,
					'f_maxhrcond'=>$f_maxhrcond,
					'activity_point'=>$activity_point
					);
					
		return $data;

		}
		else{
			return false;
		}
	}
	public function get_heart_range($user_id,$factivity)
	{
		$this->db->select('hr_zone_a');
		$this->db->from(' t_heartrate_zone_activity');
		 $this->db->where('activity_name',$factivity);
		  $this->db->where('r_user_id',$user_id);
		 $this->db->order_by('heartrate_zone_activity_id','DESC');
		$query = $this->db->get();
		$result = $query->row();
			
		if(!empty($result))
		{
		$data = array('hr_zone_a'=>$result->hr_zone_a,
					'points_rate'=>$result->points_rate
					);
		return $data;

		}
		else{
			$data = array('hr_zone_a'=>0,
					'points_rate'=>0
					);
					return $data;
		}
	}
	public function latest_test_date($user_id)
	{
		$this->db->select('*');
		$this->db->from('t_testing_measuring_transaction');
		$this->db->where('r_user_id',$user_id);
		$this->db->limit(1);
		$this->db->order_by('t_testing_measuring_transaction_id','DESC');
		$query = $this->db->get();  
		$result = $query->row();
		
		return $result->test_date;
	}
	public function get_fitness_old($user_id)
	{
		$this->db->select('fitness_level');
		$this->db->from('t_user_test_parameter');
		$this->db->where(array('r_user_id'=>$user_id,'status'=>0));
		$this->db->order_by('user_test_parameter_id','DESC');  
		$result = $this->db->get();   
		$result = $result->result();
		
		if(!empty($result) && isset($result[1]->fitness_level))
		{
			
		return $result[1]->fitness_level;  
		}
		else
		{
			return 0;
		}
		
	}
	
	public function get_flexibility_old($user_id)
	{
		$this->db->select('flex_level');  
		$this->db->from('t_flexibility_test');
		$this->db->where(array('r_user_id'=>$user_id));
		$this->db->order_by('flexibility_test_id','DESC');  
		$query = $this->db->get();   
		$result = $query->result();
		
		if(!empty($result) && isset($result[1]->flex_level))
		{
			if($result[1]->flex_level != '')
			{
				
				return $result[1]->flex_level;
			}
			else
			{
				return 0;
			}
			
		}
		else
		{
			return 0;
		}
		  
		
	}
	
	public function get_strength_old($user_id)
	{
		$this->db->select('strength_level');  
		$this->db->from('t_user_strength_level');
		$this->db->where(array('r_user_id'=>$user_id));
		$this->db->order_by('t_user_strength_level_id','DESC');  
		$query = $this->db->get();   
		$result = $query->result();  
		
		if(!empty($result)&& isset($result[1]->strength_level))
		{
			if($result[1]->strength_level != '')
			{
				
				return $result[1]->strength_level;
			}
			else
			{
				
				return 0;
			}
			
		}
		else
		{
			
			return 0;
		}
		  
		
	}

	public function get_body_composition($user_id,$gender,$age)
{

 $bmi=0;
 $waist=0;
 $fat_percentage=0;
 $data=0;
 $this->db->select('*');
 $this->db->from('t_testing_measuring_transaction');
 $this->db->join('t_test_items','t_test_items.test_item_id=t_testing_measuring_transaction.r_test_item_id','left');
 $this->db->where('r_user_id',$user_id);
 
 $query=$this->db->get();
 $result=$query->result();
 if($result>0){
  //print_r($result);
  //die;
  foreach($result as $results)
  {

   $test_item_id=$results->r_test_item_id;
   $test_value=$results->value;
  
   $test_item=$results->test_item;
   $info = $this->get_user($user_id);
   $gender = $info->gender;
   if($test_item!='' && $test_item=='BODY_MASS_INDEX')
   {
   
    $bmi_data=$this->get_bmi_level($test_value);
    $bmi = $bmi_data['level'];
    $bmi_val = $bmi_data['value'];
   }   
   else if($test_item!='' && $test_item=='FAT_PERCENTAGE')
   {
    
    $fat_percentage=$test_value;
	 $fat_level=$this->get_fat_level($test_value,$age,$gender);
   }
   
   else if($test_item!='' && $test_item=='BELLY_GRITH')
   {

    $waist_data = $this->get_waist_level($test_value,$gender);

     $waist = $waist_data['level'];
    $waist_val = $waist_data['value'];
   }
   else
   {
    continue;
   }
   
     
  }
  

  $body_composition = ($bmi + ($waist*2) + ($fat_level*2))/5;
     $data= array('body_composition'=>$body_composition,
     				'bmi_level'=>$bmi,
    				'bmi'=>$bmi_val,
    				'waist_level'=>$waist,
  					'waist'=>$waist_val,
					'fat_level'=>$fat_level,
 				 'fat_percentage'=>$fat_percentage
 				 );
  
  return $data;
 }
 else
 {
 return false;
 } 
  
 
}

/**************for getting bmi level*************/
 public function  get_bmi_level($test_value)
 {
 
  $this->db->select('BMI_level');
  $this->db->from('t_bmi_level');
  $this->db->where('bmi_min <=',$test_value);
  $this->db->where('bmi_max >=',$test_value);
  //$this->db->where("$test_value BETWEEN bmi_min AND bmi_max");
  $query=$this->db->get();
  $result=$query->result();  
  if(!empty($result)){
   foreach($result as $bmi)
   {
    $bmis=array('value'=>$test_value,
    			'level'=>$bmi->BMI_level
    			);
    return $bmis;
   }
  }
  else
  {
   return false;
  }
 }
 /**************for fat level************/
 public function  get_fat_level($test_value,$age,$gender)
 {
 
  $this->db->select('fatlevel');
  $this->db->from('t_fat_level');
  $this->db->where('age_min <=',$age);
  $this->db->where('age_max >=',$age);
  $this->db->where('fat_min <=',$test_value);
  $this->db->where('fat_max >=',$test_value);
  $this->db->where('gender',$gender);
  $query=$this->db->get();
  $result=$query->result();  
  if(!empty($result)){
   foreach($result as $fat)
   {
    
    return $fat->fatlevel;
   }
  }
  else
  {
   return false;
  }
 }
 /************for getting waist level********/
 public function  get_waist_level($test_value,$gender)
 {
  
  $this->db->select('level');
  $this->db->from('t_waist_level');
  $this->db->where('waist_min <=',$test_value);
  $this->db->where('waist_max >=',$test_value);
  //$this->db->where("$test_value BETWEEN waist_min AND waist_max");
  $this->db->where('gender',$gender);
  $query=$this->db->get();
  $result=$query->result();
  if(!empty($result)){
   foreach($result as $waist)
   {
    $waists=array('value'=>$test_value,
    			'level'=>$waist->level
    			);
    return $waists;
   }
  }
  else
  {
   return false;
  }
 }
 
 /*********get user detail***********/
 public function  get_user($user_id)
 {
  
  $this->db->select('*');
  $this->db->from('t_users');
  $this->db->where('user_id',$user_id);
  $query=$this->db->get();
  $result=$query->row();
  if(!empty($result)){
   return $result;
  }
  else
  {
   return false;
  }
 }
 
 /***************for getting cholestrol***************/
 public function get_cholestrol($user_id)
{

 $choles=0;
 $data=0;
 $this->db->select('*');
 $this->db->from('t_testing_measuring_transaction');
 $this->db->join('t_test_items','t_test_items.test_item_id=t_testing_measuring_transaction.r_test_item_id','left');
 $this->db->where('r_user_id',$user_id);
 $query=$this->db->get();
 $result=$query->result();
 if($result>0){
  
  foreach($result as $results)
  {

   $test_item_id=$results->r_test_item_id;
   $test_value=$results->value;
   $test_item=$results->test_item;
  
   if($test_item!='' && $test_item=='CHOLESTROL')
   {

    $choles=$this->get_chol_level($test_value);
   }
   else
   {
    continue;
   }
      
  }

 return $choles;
 }
 else
 {
 return false;
 } 
  
 
}

/************for getting cholestrol level********/
 public function  get_chol_level($test_value)
 {
  
  $this->db->select('level');
  $this->db->from('t_cholestrol_level');
  $this->db->where('c_min <=',$test_value);
  $this->db->where('c_max >',$test_value);
  $query=$this->db->get();
  $result=$query->result();
  if(!empty($result)){
   foreach($result as $chol)
   {
   $chols=array('chol_value'=>$test_value,
    			'cholestrol'=>$chol->level
    			);
    return $chols;
   }
  }
  else
  {
   return false;
  }
 }
 
 /***************for getting Visceral_fat %***************/
 public function get_visceral_fat($user_id)
{
 
 $visceral=0;
 $data=0;
 $this->db->select('*');
 $this->db->from('t_testing_measuring_transaction');
 $this->db->join('t_test_items','t_test_items.test_item_id=t_testing_measuring_transaction.r_test_item_id','left');
 $this->db->where('r_user_id',$user_id);
 $query=$this->db->get();
 $result=$query->result();
 if($result>0){
  
  foreach($result as $results)
  {
   $test_item_id=$results->r_test_item_id;
   $test_value=$results->value;
   $test_item=$results->test_item;
  
   if($test_item!='' && $test_item=='VISCERAL_FAT_PERCENTAGE')
   {
    
    $visceral=$this->get_vis_level($test_value);
   }
   else
   {
    continue;
   }
     
  }
 return $visceral;
 }
 else
 {
 return false;
 } 
  
 
}
/************for getting Visceral fat level********/
 public function  get_vis_level($test_value)
 {
  
  $this->db->select('level');
  $this->db->from('t_visceral_fat_level');
  $this->db->where('fat_min <=',$test_value);
  $this->db->where('fat_max >',$test_value);
  $query=$this->db->get();
  $result=$query->result();
  if(!empty($result)){
   foreach($result as $vis)
   {
     $visl=array('vis_value'=>$test_value,
    			'visceral'=>$vis->level
    			);
    return $visl;
   }
  }
  else
  {
   return false;
  }
 }
 /***************for getting Glucose***************/
 public function get_glucose($user_id)
{
 
 $glucose=0;
 $data=0;
 $this->db->select('*');
 $this->db->from('t_testing_measuring_transaction');
 $this->db->join('t_test_items','t_test_items.test_item_id=t_testing_measuring_transaction.r_test_item_id','left');
 $this->db->where('r_user_id',$user_id);
 $query=$this->db->get();
 $result=$query->result();
 if($result>0){
  
  foreach($result as $results)
  {
   $test_item_id=$results->r_test_item_id;
   $test_value=$results->value;
   $test_item=$results->test_item;
  
   if($test_item!='' && $test_item=='GLUCOSE_SOBER')
   {
    
    $glucose=$this->get_glucose_level($test_value,1);

   }
   if($test_item!='' && $test_item=='GLUCOSE_NOT_SOBER')
   {
    
    $glucose=$this->get_glucose_level($test_value,0);
   }
   else
   {
    continue;
   }
     
  }

  return $glucose;
 }
 else
 {
 return false;
 } 
  
 
}

/************for getting glucose level********/
 public function  get_glucose_level($test_value,$sober)
 {
 
  $this->db->select('level');
  $this->db->from('t_glucose_level');
  $this->db->where('glucose_min <=',$test_value);
  $this->db->where('glucose_max >',$test_value);
  $this->db->where('sober',$sober);
  $query=$this->db->get();
  $result=$query->result();
  if(!empty($result)){
   foreach($result as $gluc)
   {
    $glucose=array('glu_value'=>$test_value,
    			'glucose'=>$gluc->level
    			);
    return $glucose;
   }
  }
  else
  {
   return false;
  }
 }
	
	public function EatMind_old_level($user_id)
	{
		$this->db->select('*');  
		$this->db->from('t_test_overall_result_level');
		$this->db->where(array('r_user_id'=>$user_id));
		$this->db->order_by('t_test_overall_result_level_id','DESC');  
		$query = $this->db->get();   
		$result = $query->result();  
		
		if(!empty($result)&& isset($result[1]))
		{
			if($result[1] != '')
			{
								
				return $result[1];
			}
			else
			{
				
				return 0;
			}
			
		}
		else
		{
			
			return 0;
		}
		  
		
	}
	
	function client_goal($user_id)
	{
		$this->db->select('target');
		$this->db->from('t_user_test_parameter');
		$this->db->where('r_user_id',$user_id);
		$this->db->limit(1);
		$this->db->order_by('user_test_parameter_id','DESC');  
		$result = $this->db->get();   
		$result = $result->row();
		  
		if(!empty($result))
		{
			
		return $result->target;  
		}
		else
		{
			return 0;
		}
	}   
	
	function target_credits($user_id)
	{
		$result = array();
		$this->db->select('iant_hr,max_max_load,max_load,max_heart_rate,min_hr_70,max_hr_90');
		$this->db->from('t_user_test_parameter');
		$this->db->where(array('r_user_id'=>$user_id));
		$this->db->order_by('user_test_parameter_id','DESC');  
		$result = $this->db->get();   
		$result = $result->result();
		  
		if(!empty($result) )
		{
		
		$response['iant_hr']= intval($result[0]->iant_hr);
		$response['iant_p']= $result[0]->max_max_load;	
		$response['max_load']= $result[0]->max_load;
		$response['max_heart_rate']= $result[0]->max_heart_rate;
		$response['min_heart_70']= $result[0]->min_hr_70;
		$response['max_heart_90']= $result[0]->max_hr_90;		
		}
		else
		{
			$response['iant_hr']= 0;
			$response['iant_p']= 0;
			$response['max_load']= 0;
			$response['max_heart_rate']= 0;
			$response['min_heart_70']= 0;
			$response['max_heart_90']= 0;
		}
		return $response;   
		
	}
	
	public function activity_score($user_id,$group_id,$type)
	{
		   $credit_score = array();
		$this->db->select('topic_id');
		$this->db->from('t_form_topics');
		$this->db->where('group_id',$group_id);
		$query = $this->db->get();
		$result = $query->result();
		foreach($result as $topics)
		{
			$t_id = $topics->topic_id;
			$form_id =$this->get_form_id($t_id);
			//print_r($form_id);
			if($form_id!=''){
			foreach($form_id as $value)
			{
				//print_r($value);
				 $element_id = $value['element_id'];
					$credit_new_score[]=$this->get_score_values($element_id,$user_id);
					
				}
			}  
		}
			
		     
		//print_r($credit_new_score);
		
		if($type=='first')
		{
			return $credit_new_score;
		}
		else if($type=='follow')
		{
					
		foreach($credit_new_score as $new)
					{
					
						$new_date = $new->date;
						$new_score = $new->value;
						$new_element_id = $new->r_form_element_id;
						$credit_old_score=$this->get_old_score_values($new_element_id,$user_id,$new_date);
											
						$old_score = $credit_old_score->value;
					 $credit_score[] = array('old_score' => $old_score,
											'new_score' => $new_score
											);
					
					}
					//print_r($credit_score);
					return $credit_score;
		}
	
		
	}
	
	public function get_form_id($t_id)
	{
		$this->db->select('form_id');
		$this->db->from('t_form');
		$this->db->where('r_topic_id',$t_id);
		$query = $this->db->get();
		$result = $query->result();
		if(!empty($result))
		{
				foreach($result as $form)
				{
				$f_id = $form->form_id;	
				
			//return $form->form_id;
			$element_id = $this->get_formelement_id($f_id);
			if(!empty($element_id))
			{
			$data = array('element_id'=>$element_id
						);
							   
				return $data;
			}
				}
		}
		else{
			return false;
		}
	}
	//element id//
	public function get_formelement_id($form_id)
	{
		$this->db->select('id');
		$this->db->from('t_form_element');
		$this->db->where('form_id',$form_id);
		$this->db->where('is_delete',0);
		$query = $this->db->get();
		$result = $query->result();
		if(!empty($result))
		{
			//return $result;
			foreach($result as $value)
			{
				
				return $value->id;
			}
					
		
			
		}
		else{
			return false;
		}
	}
	public function get_score_values($element_id,$user_id)
	{
		//echo $element_id;
		
		$this->db->select('*');
		$this->db->from('t_mv_data');
		$this->db->where('r_form_element_id',$element_id);
		$this->db->where('user_id',$user_id);
		$this->db->order_by('date','DESC');
		$query = $this->db->get();
		$result = $query->result();
		//print_r($result);  
		if(!empty($result))
		{
			foreach($result as $data){
			return $data;
			}
		}   
		else{
			return false;
		}           
	}
	public function get_old_score_values($element_id,$user_id,$latest_date)
	{   
		$date = strtotime($latest_date);
		 $date = strtotime("-7 day", $date);
		$past_date = date('Y-m-d h:i:s', $date);
		$this->db->select('*');         
		$this->db->from('t_mv_data');
		$this->db->where('r_form_element_id',$element_id);
		$this->db->where('user_id',$user_id);
		$this->db->where('date <=',$past_date);
		
		$query = $this->db->get();
		$result = $query->row();
		
		if(!empty($result))
		{		
			return $result;
			
		}   
		else{
			return false;
		}             
		 
	}
	public function per_week($age_on_test,$fitness,$gender,$week)
	{
		  
		$this->db->select('f_points');
  $this->db->from('t_fitpoints');    
  $this->db->where('f_age_min <=',$age_on_test);
  $this->db->where('f_age_max >=',$age_on_test);
   $this->db->where('f_gender',$gender);
   $this->db->where('f_fitlevel',$fitness);
   $this->db->where('f_weeknr',$week);
  $query=$this->db->get();
  $result=$query->row();
 
  if(!empty($result)) {
   return $result;
  
  }
  else
  {
   return false;
  }
	}
	
	
	/**************************************************
	for last two question of general personality profile
	***************************************************/
	
	public function personal_questions($user_id,$group_id,$topic,$limit='',$type='')
	{
		
		   $credit_score = array();
		$this->db->select('*');
		$this->db->from('t_form');
		$this->db->where('r_quesgroup_id',$group_id);
		$this->db->where('r_quesphase_id',1);
		$this->db->where('r_topic_id',$topic);
		$this->db->where('is_delete',0);
		$query = $this->db->get();
		$result = $query->result();
	
		foreach($result as $form)
		{
			$f_id = $form->form_id;
			$element_id = $this->get_form_element_id($f_id,$limit,$type);
			
			foreach($element_id as $key => $element){
					 $e_id = $element->id;
					
							$this->db->select('value');         
						$this->db->from('t_mv_data');
						$this->db->where('r_form_element_id',$e_id);
						$this->db->where('user_id',$user_id);
						$query = $this->db->get();
						$result1 = $query->row();
						$arr['value'.$key] =$result1->value;
					
					}		
		}
		
	
		return $arr;	
		
	}
	
	public function get_form_element_id($form_id,$limit='',$type='')
	{
		$this->db->select('*');
		$this->db->from('t_form_element');
		$this->db->where('form_id',$form_id);
		$this->db->where('is_delete',0);
		$this->db->order_by('id','DESC'); 
		if($limit!=''){
		$this->db->limit($limit);
		}
		if($type!=''){
			$this->db->like('ques_type',$type);
		}
		$query = $this->db->get();
		$result = $query->result();
		if(!empty($result))
		{	
				/* $element = array('e_id1' => $result[0]->id,
							'e_id2'=>$result[1]->id
							); */
						return $result;
		}
		else{
			return false;
		}
	
	
	
}
public function preference_model($user_id,$group_id,$topic,$type)
	{
		
		   $credit_score = array();
		$this->db->select('*');
		$this->db->from('t_form');
		$this->db->where('r_quesgroup_id',$group_id);
		$this->db->where('is_delete',0);
		$this->db->where('r_quesphase_id',1);
		$this->db->where('r_topic_id',$topic);
		$query = $this->db->get();
		$result = $query->result();
		
		foreach($result as $form)
		{
			$f_id = $form->form_id;
			
			$element_id = $this->get_form_pelement_id($f_id,$type);
			
			foreach($element_id as $key => $element){
					 $e_id = $element->id;
					
							$this->db->select('value');         
						$this->db->from('t_mv_data');
						$this->db->where('r_form_element_id',$e_id);
						$this->db->where('user_id',$user_id);
						$query = $this->db->get();
						$result1 = $query->row();
						$arr['preference'.$key] =$result1->value;
					
					}		
		}
		
	
		return $arr;	
		
	}
	
	public function get_form_pelement_id($form_id,$type)
	{
		$this->db->select('*');
		$this->db->from('t_form_element');
		$this->db->where('form_id',$form_id);
		$this->db->where('is_delete',0);
		$this->db->like('ques_type',$type);
		if($type!='goal' && $type!='medical'){
		$this->db->order_by('id','ASC'); 
		}
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result();
		if(!empty($result))
		{	
				/* $element = array('e_id1' => $result[0]->id,
							'e_id2'=>$result[1]->id
							); */
						return $result;
		}
		else{
			return false;
		}
		
}
public function get_days($user_id,$group_id,$phase_id)
{
		$this->db->select('*');
		$this->db->from('t_ques_phase_user');
		$this->db->where('r_phase_id',$phase_id);
		$this->db->where('r_group_id',$group_id);
		$this->db->where('r_user_id',$user_id);
		$query = $this->db->get();
		$result = $query->result();
		
		if(!empty($result))
		{	
			foreach($result as $date)
			{	
				$create_date = $date->created_date; 
				$now = time(); // or your date as well
				$your_date = strtotime($create_date);
				$datediff = $now - $your_date;

				$days = floor($datediff / (60 * 60 * 24));
				
			}
			if($days==-1){
				$days=1;
			}
			
			
			return $days;		
			
		}
		else{
			return 0;
		}
}
public function count_step($user_id,$form_element_id){
	$this->db->select('*');
			$this->db->from('t_mv_data');
			$this->db->join('t_mvactual_answers','t_mvactual_answers.actmv_id=t_mv_data.id','inner');
			$this->db->where('t_mv_data.user_id',$user_id);
			$this->db->where('t_mv_data.r_form_element_id',$form_element_id);
			$this->db->order_by('mvactanswer_id','DESC');
			$query = $this->db->get();
			$result = $query->row();
			
			if(!empty($result)){
				return $result->actmv_otherdata;
			}
			else{
				return 0;
			}
	
}
/*for score chart credits***********/
public function get_credit($user_id,$group_id)
{
	
		  $credit_score = array();
		$this->db->select('topic_id');
		$this->db->from('t_form_topics');
		$this->db->where('group_id',$group_id);
		$query = $this->db->get();
		$result = $query->result();
		
		if(!empty($result)){
		foreach($result as $topics)
		{
			$t_id = $topics->topic_id;
			$form_id =$this->get_form_id($t_id);
			//print_r($form_id);
			if($form_id!=''){
			foreach($form_id as $value)
			{
				//print_r($value);
				 $element_id = $value['element_id'];
					$credit_new_score[]=$this->get_score_values($element_id,$user_id);
					
				}
			}  
		}
	
	return $credit_new_score;
		}
		else{
			return false;
		}
}
	public function get_credit_index($group_id)
	{
		
			$this->db->select('*');
			$this->db->from('t_ques_phase_weightage');
			$this->db->where('r_group_id',$group_id);
			$this->db->where('r_phase_id',1);
			$this->db->order_by('id','DESC');
			$query = $this->db->get();
			$result = $query->row();
			if(!empty($result)){
				return $result;
			}
			else{
				return 0;
			}
			
	}
	//for form element id//
	public function get_felement_id($group_id,$topic,$type)
	{
		
		$this->db->select('*');
		$this->db->from('t_form');
		$this->db->where('r_quesgroup_id',$group_id);
		$this->db->where('is_delete',0);
		$this->db->where('r_quesphase_id',1);
		$this->db->where('r_topic_id',$topic);
		$query = $this->db->get();
		$result = $query->result();
		if(!empty($result)){
			foreach($result as $form)
			{
				$f_id = $form->form_id;
				$element_id = $this->get_form_pelement_id($f_id,$type);
				foreach($element_id as $e_id){
					$elementid = $e_id->id;
				}
			}
		
		return $elementid;
		}
		else{
			return false;
		}
		
	}
	public function mv_question($user_id,$form_element_id){
	
			$this->db->select('t_mv_data.*,t_mvactual_answers.*');
			$this->db->from('t_mv_data');
			$this->db->join('t_mvactual_answers','t_mvactual_answers.actmv_id=t_mv_data.id','inner');
			$this->db->where('t_mv_data.user_id',$user_id);
			$this->db->where('t_mv_data.r_form_element_id',$form_element_id);
			
			$query = $this->db->get();
			$result = $query->row();
			
			if(!empty($result)){
				return $result;
			}
			else{
				return 0;
			}
	
	
	}
	public function medical_text($option_id)
	{
			$this->db->select('options');
			$this->db->from('t_formelem_option_language');
			$this->db->where('optionid',$option_id);
			$this->db->where('language_id',3);
			$query = $this->db->get();
			$result = $query->row();
			if(!empty($result)){
				return $result->options;
			}
			else{
				return 0;
			}
	}
	public function get_save_credits($user_id)
				{
					$this->db->select('*');			
					$this->db->from('t_save_credits');
					$this->db->where('user_id', $user_id);
					$this->db->order_by('id','desc');
					$query=$this->db->get();			
					$data=$query->row();
				
					if(!empty($data)){
					 return $data;
					}
					else
					{
						return 0;
					}
				}
				public function get_save_old_credits($user_id)
				{
					$this->db->select('*');			
					$this->db->from('t_save_credits');
					$this->db->where('user_id', $user_id);
					$this->db->order_by('id','desc');
					$query=$this->db->get();			
					$data=$query->result();
				
					if(!empty($data[1])){
					 return $data[1];
					}
					else
					{
						return 0;
					}
				}
	
}