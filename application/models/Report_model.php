<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report_model extends CI_Model {
/*********************************
fetch data according to consult type
***********************************/

	/*public $warmuptime = 150;
    public $cooldowntime = 150;
	public $coolDownTime = '';*/
	public $warmuptime = 150;
    public $cooldowntime = 150;
    public $testtimewithwarmup = 0;
	public $coolDownTime = 0;
	public $testTime = 0;

public function get_fitness($user_id)
	{
		$this->db->select('fitness_level');
		$this->db->from('t_user_test_parameter');
		$this->db->where(array('r_user_id'=>$user_id,'status'=>0));
		$this->db->order_by('user_test_parameter_id','DESC');  
		$result = $this->db->get();   
		$result = $result->result();
		
		if(!empty($result))
		{
		return $result[0]->fitness_level;  
		}
		else
		{
			return 0;
		}
		
	}
	public function get_flexibility($user_id)
	{
		$this->db->select('flex_level');  
		$this->db->from('t_flexibility_test');
		$this->db->where(array('r_user_id'=>$user_id));
		$this->db->order_by('flexibility_test_id','DESC');  
		$query = $this->db->get();   
		$result = $query->result();
		
		if(!empty($result))
		{
			if($result[0]->flex_level != '')
			{
				return $result[0]->flex_level;
			}
			else
			{
				return 0;
			}
			
		}
		else
		{
			return 0;
		}
		  
		
	}
	public function get_strength($user_id)
	{
		$this->db->select('strength_level');  
		$this->db->from('t_user_strength_level');
		$this->db->where(array('r_user_id'=>$user_id));
		$this->db->order_by('t_user_strength_level_id','DESC');  
		$query = $this->db->get();   
		$result = $query->result();
		
		if(!empty($result))
		{
			if($result[0]->strength_level != '')
			{
				return $result[0]->strength_level;
			}
			else
			{
				return 0;
			}
			
		}
		else
		{
			return 0;
		}
		  
		
	}
	
	/*********************************
fetch data for body_composition
***********************************/
public function get_body_composition($user_id,$gender,$age)
{
 $bmi=0;
 $waist=0;
 $fat_percentage=0;
 $data=0;
 $this->db->select('*');
 $this->db->from('t_testing_measuring_transaction');
 $this->db->join('t_test_items','t_test_items.test_item_id=t_testing_measuring_transaction.r_test_item_id','left');
 $this->db->where('r_user_id',$user_id);
 
 $query=$this->db->get();
 $result=$query->result();
 if($result>0){
  //print_r($result);
  //die;
  foreach($result as $results)
  {
   $test_item_id=$results->r_test_item_id;
   $test_value=$results->value;
  
   $test_item=$results->test_item;
   $info = $this->get_user($user_id);
   $gender = $info->gender;
   if($test_item!='' && $test_item=='BODY_MASS_INDEX')
   {
    
    $bmi=$this->get_bmi_level($test_value);
    
   }   
   else if($test_item!='' && $test_item=='FAT_PERCENTAGE')
   {
    $fat_level=$this->get_fat_level($test_value,$age,$gender);
	$fat_percentage = $test_value;
    
   }
   
   else if($test_item!='' && $test_item=='BELLY_GRITH')
   {
    $waist=$this->get_waist_level($test_value,$gender);
   }
   else
   {
    continue;
   }
   
     
  }
  
  
  $body_composition = ($bmi + ($waist*2) + ($fat_level*2))/5;
     $data=$body_composition;
   
  return $data;
 }
 else
 {
 return false;
 } 
  
 
}

/**************for getting bmi level*************/
 public function  get_bmi_level($test_value)
 {
 
  $this->db->select('BMI_level');
  $this->db->from('t_bmi_level');
  $this->db->where('bmi_min <=',$test_value);
  $this->db->where('bmi_max >',$test_value);
  //$this->db->where("$test_value BETWEEN bmi_min AND bmi_max");
  $query=$this->db->get();
  $result=$query->result();  
  if(!empty($result)){
   foreach($result as $bmi)
   {
    
    return $bmi->BMI_level;
   }
  }
  else
  {
   return false;
  }
 }
 /**************for fat level************/
 public function  get_fat_level($test_value,$age,$gender)
 {
 
  $this->db->select('fatlevel');
  $this->db->from('t_fat_level');
  $this->db->where('age_min <=',$age);
  $this->db->where('age_max >',$age);
  $this->db->where('fat_min <=',$test_value);
  $this->db->where('fat_max >',$test_value);
  $this->db->where('gender',$gender);
  //$this->db->where("$test_value BETWEEN bmi_min AND bmi_max");
  $query=$this->db->get();
  $result=$query->result();  
  if(!empty($result)){
   foreach($result as $fat)
   {
    
    return $fat->fatlevel;
   }
  }
  else
  {
   return false;
  }
 }
 /************for getting waist level********/
 public function  get_waist_level($test_value,$gender)
 {
  
  $this->db->select('level');
  $this->db->from('t_waist_level');
  $this->db->where('waist_min <=',$test_value);
  $this->db->where('waist_max >',$test_value);
  //$this->db->where("$test_value BETWEEN waist_min AND waist_max");
  $this->db->where('gender',$gender);
  $query=$this->db->get();
  $result=$query->result();
  if(!empty($result)){
   foreach($result as $waist)
   {
    
    return $waist->level;
   }
  }
  else
  {
   return false;
  }
 }
 
 /*********get user detail***********/
 public function  get_user($user_id)
 {
  
  $this->db->select('*');
  $this->db->from('t_users');
  $this->db->where('user_id',$user_id);
  $query=$this->db->get();
  $result=$query->row();
  if(!empty($result)){
   return $result;
  }
  else
  {
   return false;
  }
 }
 
 /***************for getting cholestrol***************/
 public function get_cholestrol($user_id)
{
 $cholestrol=0;
 $data=0;
 $this->db->select('*');
 $this->db->from('t_testing_measuring_transaction');
 $this->db->join('t_test_items','t_test_items.test_item_id=t_testing_measuring_transaction.r_test_item_id','left');
 $this->db->where('r_user_id',$user_id);
 $query=$this->db->get();
 $result=$query->result();
 if($result>0){
  
  foreach($result as $results)
  {
   $test_item_id=$results->r_test_item_id;
   $test_value=$results->value;
   $test_item=$results->test_item;
  
   if($test_item!='' && $test_item=='CHOLESTROL')
   {
    
    $cholestrol=$this->get_chol_level($test_value);
   }
   else
   {
    continue;
   }
   
  
   
  }
 return $cholestrol;
 }
 else
 {
 return false;
 } 
  
 
}

/************for getting cholestrol level********/
 public function  get_chol_level($test_value)
 {
  
  $this->db->select('level');
  $this->db->from('t_cholestrol_level');
  $this->db->where('c_min <=',$test_value);
  $this->db->where('c_max >',$test_value);
  $query=$this->db->get();
  $result=$query->result();
  if(!empty($result)){
   foreach($result as $chol)
   {
   
    return $chol->level;
   }
  }
  else
  {
   return false;
  }
 }
 
 /***************for getting Visceral_fat %***************/
 public function get_visceral_fat($user_id)
{
 
 $visceral=0;
 $data=0;
 $this->db->select('*');
 $this->db->from('t_testing_measuring_transaction');
 $this->db->join('t_test_items','t_test_items.test_item_id=t_testing_measuring_transaction.r_test_item_id','left');
 $this->db->where('r_user_id',$user_id);
 $query=$this->db->get();
 $result=$query->result();
 if($result>0){
  
  foreach($result as $results)
  {
   $test_item_id=$results->r_test_item_id;
   $test_value=$results->value;
   $test_item=$results->test_item;
  
   if($test_item!='' && $test_item=='VISCERAL_FAT_PERCENTAGE')
   {
    
    $visceral=$this->get_vis_level($test_value);
   }
   else
   {
    continue;
   }
     
  }
 return $visceral;
 }
 else
 {
 return false;
 } 
  
 
}
/************for getting Visceral fat level********/
 public function  get_vis_level($test_value)
 {
  
  $this->db->select('level');
  $this->db->from('t_visceral_fat_level');
  $this->db->where('fat_min <=',$test_value);
  $this->db->where('fat_max >',$test_value);
  $query=$this->db->get();
  $result=$query->result();
  if(!empty($result)){
   foreach($result as $vis)
   {
    
    return $vis->level;
   }
  }
  else
  {
   return false;
  }
 }
 /***************for getting Glucose***************/
 public function get_glucose($user_id)
{
 
 $glucose=0;
 $data=0;
 $this->db->select('*');
 $this->db->from('t_testing_measuring_transaction');
 $this->db->join('t_test_items','t_test_items.test_item_id=t_testing_measuring_transaction.r_test_item_id','left');
 $this->db->where('r_user_id',$user_id);
 $query=$this->db->get();
 $result=$query->result();
 if($result>0){
  
  foreach($result as $results)
  {
   $test_item_id=$results->r_test_item_id;
   $test_value=$results->value;
   $test_item=$results->test_item;
  
   if($test_item!='' && $test_item=='GLUCOSE_SOBER')
   {
    
    $glucose=$this->get_glucose_level($test_value,1);
   }
   if($test_item!='' && $test_item=='GLUCOSE_NOT_SOBER')
   {
    
    $glucose=$this->get_glucose_level($test_value,0);
   }
   else
   {
    continue;
   }
     
  }
  return $glucose;
 }
 else
 {
 return false;
 } 
  
 
}

/************for getting glucose level********/
 public function  get_glucose_level($test_value,$sober)
 {
 
  $this->db->select('level');
  $this->db->from('t_glucose_level');
  $this->db->where('glucose_min <=',$test_value);
  $this->db->where('glucose_max >',$test_value);
  $this->db->where('sober',$sober);
  $query=$this->db->get();
  $result=$query->result();
  if(!empty($result)){
   foreach($result as $gluc)
   {
    
    return $gluc->level;
   }
  }
  else
  {
   return false;
  }
 }
 
 
 /*****model function for getting vita_16 value************/
 public function get_vita_sixteeen($user_id,$iwant=null)
 { 
	 $energy=0;
 $mot=0;
 $regilience=0;
 $data=0;
  
 $this->db->select('*');
 $this->db->from('t_testing_measuring_transaction');
 $this->db->join('t_test_items','t_test_items.test_item_id=t_testing_measuring_transaction.r_test_item_id','left');
 $this->db->where('r_user_id',$user_id);
 $this->db->order_by('t_testing_measuring_transaction_id','ASC');

 $query=$this->db->get();
 $result=$query->result();

 if($result>0){
 
  foreach($result as $results)
  {
    

   $test_item_id=$results->r_test_item_id;
   $test_value=$results->value;
 
   $test_item=$results->test_item;
   $info = $this->get_user($user_id);
   $gender = $info->gender;
   if($test_item!='' && $test_item=='ENERGY')
   {
    $energy_val= $test_value;


    $energy=$this->get_vita_level($test_value,'ENERGY');
    
   }   
   else if($test_item!='' && $test_item=='MOTIVATION')
   {
    $mot_val= $test_value;
     $mot=$this->get_vita_level($test_value,'MOTIVATION');
	 
   }
   
   else if($test_item!='' && $test_item=='RESILIENCE')
   { 
    $regilience_val= $test_value;
    $regilience=$this->get_vita_level($test_value,'RESILIENCE');
   }
   else
   {
    continue;
   }
     
  }


   $vita= ($energy+$mot+$regilience)/3;
  $data = array('vita_16'=>$vita,
              'energy'=>$energy_val,
              'motivation'=>$mot_val,
              'resilience'=>$regilience_val
              );

    
	  //  return array('energy'=>$energy_val,'motivation'=>$mot_val,'resilience'=>$regilience_val);


  return $data;
 }
 else
 {
 return false;
 } 
  
 
 }
 /************for getting e level********/
 public function  get_vita_level($test_value,$type)
 {
 if($type =='ENERGY')
 {
  $this->db->select('level');
  $this->db->from('t_vita_energy_level');
  $this->db->where('energy_min <',$test_value);
  $this->db->where('energy_max >=',$test_value);
  
  $query=$this->db->get();
  $result=$query->result();
  
  if(!empty($result)){
   foreach($result as $energy)
   {
    
    return $energy->level;
   }
  }
  else
  {
   return false;  
  }
 }
 else if($type =='MOTIVATION'){
	 $this->db->select('motivation_level');
  $this->db->from('t_vita_motivation_level');
  $this->db->where('mot_min <=',$test_value);
  $this->db->where('mot_max >=',$test_value);
  
  $query=$this->db->get();
  $result=$query->result();
  
  if(!empty($result)){
   foreach($result as $mot)
   {
    
    return $mot->motivation_level; 
   }
  }
  else
  {
   return false;  
  }
	 
 }
 else if($type =='RESILIENCE')
 {
	 $this->db->select('level');
  $this->db->from('t_vita_resilience_level');
  $this->db->where('res_min <=',$test_value);
  $this->db->where('res_max >=',$test_value);
  
  $query=$this->db->get();
  $result=$query->result();
  
  if(!empty($result)){
   foreach($result as $res)
   {
    
    return $res->level;
   }
  }
  else
  {
   return false;  
  }
 }
 }
 
 
 /*****model function for getting stress Sleep mindfullnes value************/
 public function get_stress_sleep_mindfullness_level($user_id)
 {
	$data['stress'] = 0;
	$data['sleep'] =0;
 $data['mindful'] =0;
 $this->db->select('*');
 $this->db->from('t_testing_measuring_transaction');
 $this->db->join('t_test_items','t_test_items.test_item_id=t_testing_measuring_transaction.r_test_item_id','left');
 $this->db->where('r_user_id',$user_id);
 $this->db->where('t_testing_measuring_transaction.r_test_id',9);
 //$this->db->order_by('t_testing_measuring_transaction_id','DESC');
 $query=$this->db->get();
 $result=$query->result();
 if($result>0){
   
  foreach($result as $results)
  {
   $test_item_id=$results->r_test_item_id;
   $test_value=$results->value;
 
   $test_item=$results->test_item;
   $info = $this->get_user($user_id);
   $gender = $info->gender;
   if($test_item!='' && $test_item=='STRESS')
   {
    $data['stress_val']=$test_value;
    $data['stress']=$this->get_stress_sleep_mindfull_level($test_value,'STRESS');
    
   } 
  
   else if($test_item!='' && $test_item=='SLEEP')
   {
     $data['sleep_val']=$test_value;
    $data['sleep']=$this->get_stress_sleep_mindfull_level($test_value,'SLEEP');
    
   }
   else if($test_item!='' && $test_item=='MINDFULNESS')
   {
     $data['mindful_val']=$test_value;
    $data['mindful']=$this->get_stress_sleep_mindfull_level($test_value,'MINDFULNESS');
    
   }
   {
    continue;
   }
   
     
  }
 
  
  return $data;
 }
 else
 {
 return false;
 } 
  
 
 }
/*********************************
Personal Goal evaluations
**********************************/ 
 public function personal_goal($user_id)
 {
	 
	  $userperonalgoal =$this->db->select('target')->from('t_user_test_parameter')->where(array('r_user_id'=>$user_id))->order_by("user_test_parameter_id", "desc")->get()->row();
	
	  if(count($userperonalgoal)>0)
	  {
		  return $userperonalgoal->target;
		  
	  }
	  else
	  {
		  return 0;  
		  
	  }
 
 }
 
 /************for getting e level********/
 public function  get_stress_sleep_mindfull_level($test_value,$type)
 {
 if($type =='MINDFULNESS')
 {
  $this->db->select('level');
  $this->db->from('t_mind_fullness_level');
  $this->db->where('mind_min <=',$test_value);
  $this->db->where('mind_max >=',$test_value);
  
  $query=$this->db->get();
  $result=$query->result();
  
  if(!empty($result)){
   foreach($result as $mind)
   {
    
    return $mind->level;
   }
  }
  else
  {
   return false;  
  }
 }
 else if($type =='SLEEP'){
	 $this->db->select('level');
  $this->db->from('t_sleep_level');
  $this->db->where('SLEEP_min <=',$test_value);
  $this->db->where('SLEEP_max >=',$test_value);
  
  $query=$this->db->get();
  $result=$query->result();
  
  if(!empty($result)){
   foreach($result as $SLEEP)
   {
    
    return $SLEEP->level; 
   }
  }
  else
  {
   return false;  
  }
	 
 }
 else if($type =='STRESS')
 {
	 $this->db->select('level');
  $this->db->from('t_stress_level');
  $this->db->where('stress_min <=',$test_value);
  $this->db->where('stress_max >=',$test_value);
  
  $query=$this->db->get();
  $result=$query->result();
  
  if(!empty($result)){
   foreach($result as $stress)  
   {
    
    return $stress->level;
   }
  }
  else
  {
   return false;  
  }
 }
 }
 Public function get_level_description($type_id,$item_id,$level)
 {
   $this->db->select('*');
   $this->db->from('t_level_description');
   $this->db->where('type_id',$type_id);
   $this->db->where('item_id',$item_id);
   $this->db->where('level',$level);
   $query = $this->db->get();
   $result=$query->row();
   if(!empty($result))
   {
     
    return $result->level_description;
   }
   else{
     return false;
    }
   
 }
//for admin support link//
public function get_link()
{
  $this->db->select('link');
  $this->db->from('t_support_link');
$query = $this->db->get();
   $result=$query->row();
   if(!empty($result))
   {
     
    return $result->link;
   }
   else{
     return false;
    }
} 

public function get_personal_info($user_id)
{
  $this->db->select('*');
  $this->db->from('t_personalinfo');
  $this->db->where('r_user_id',$user_id);
	$query = $this->db->get();
   $result=$query->row();
   if(!empty($result))
   {
     
    return $result;
   }
   else{
     return false;
    }
} 
/**********for coach id***********/
public function coach_id($user_id){
	
	$this->db->select('*');
	$this->db->from('t_coach_member');
	$this->db->where('r_user_id',$user_id);
	$query = $this->db->get();
   $result=$query->row();
   
     if(!empty($result))
	   {
		return $result->r_coach_id;
	   }
   else
	{
     return false;
    }
	
}
public function getinfo($coach_user_id){
	 $this->db->select('*');
  $this->db->from('t_users');
  $this->db->join('t_personalinfo','t_users.user_id=t_personalinfo.r_user_id','inner');
  $this->db->where('user_id',$coach_user_id);
	$query = $this->db->get();
   $result=$query->row();
   if(!empty($result))
   {
     
    return $result;
   }
   else{
     return false;
    }
	
}
public function stepcheckgetinfo($user_id,$step_id){
	$data['step_entry_type'] = $step_id;
	$this->db->where('r_user_id', $user_id);
    $this->db->update(t_personalinfo, $data);
	$afftectedRows = $this->db->affected_rows();
	return $afftectedRows;
}
/*****************Get the created date of Phase 10-04-17 ***********/
function getdetails($user_id,$group_id,$phase_id,$reftypeid,$f_status){
	$query = $this->db->query('SELECT t_point.f_userid, t_point.f_phaseid, t_point.f_groupid, t_point.f_creditdttm, SUM( t_point.f_points ) AS credit, DATEDIFF( t_point.f_creditdttm, t_ques_phase_user.created_date )+1 AS DiffDate FROM t_training_points_achieved AS t_point 
LEFT JOIN t_ques_phase_user ON t_point.f_userid = t_ques_phase_user.r_user_id AND t_point.f_phaseid = t_ques_phase_user.r_phase_id AND t_point.f_groupid = t_ques_phase_user.r_group_id WHERE f_userid ='.$user_id. ' AND f_phaseid ='.$phase_id. ' AND f_groupid ='.$group_id. ' AND f_reftypeid ='.$reftypeid. ' AND f_status='.$f_status .' GROUP BY t_point.f_groupid, t_point.f_phaseid, t_point.f_creditdttm');
	 if ($query->num_rows()>0)
    {
        $row = $query->result();
		return $row;
    }
	else{
		return false;
	}
}
public function add_manual_data($data){

      $this->db->insert('t_save_manual_data',$data);
      $result = $this->db->insert_id();

         return  $result;

}

public function check_manual_data($user_id,$coach_id,$date_timestamp){
	$this->db->select('*');
	$this->db->from('t_save_manual_data');
	$this->db->where('user_id',$user_id);
	$this->db->where('coach_id',$coach_id);
	$this->db->where('date_timestamp',$date_timestamp);
	$this->db->order_by('id','desc');
	$query = $this->db->get();
	$result=$query->row();
	if(!empty($result))
	{
		return $result;
	}
	else
	{
		return false;
    }
}
public function get_last_test($id)
{
	$res = array('status'=>101,'message'=>'Test is not present');
	$this->db->select('*');
	$this->db->from('t_save_manual_data');
	$this->db->where('id',$id);
	$query = $this->db->get();
	$result=$query->row();
	if(!empty($result))
	{
		$res =$this->addTestManually($result);
	}
	return $res;
	die ;
}


public function add_data($table,$data){
	
	  $this->db->insert($table,$data);
      $result = $this->db->insert_id();
	  return  $result;

}

public function addTestManually($data)
{

	$resCallBack = array('status'=>101,'message'=>'Please increase test time or change the Test Level');
	$testweight = $data->weight;
	$heartRateOnStart = $data->start_beats;
	$heartRateOnEnd = $data->on_iant;
	$testMinute = $data->runtime;
	$userid = $data->user_id;
	$coachid = $data->coach_id;
	$club_id = $data->club_id;
	$testdate = date('Y-m-d 00:00:00',$data->date_timestamp);
	$tstLvl = (isset($data->test_level) && $data->test_level != '')?$data->test_level:'A';
	$testLevel = strtolower($tstLvl);
	$speedLevel = array('a'=>3,'b'=>4,'c'=>5,'d'=>6,'e'=>7,'f'=>8);
	
	$startspeed = $speedLevel[$testLevel];
	
	$testSeconds = $testMinute*60;
	$numberOfHeartRate = $heartRateOnEnd-$heartRateOnStart;
	$heartRateStartWithoutTest = round($heartRateOnStart/100)*10;
	$heartRateEndWithoutTest = round(($heartRateOnStart/100)*80);
	
	$values['user_id'] =  $userid;
	$values['club_id'] =  $club_id;
	$values['coach_id'] =  $coachid;
	$values['weight'] =  $testweight;
	$values['test_start_date'] =  $testdate;
	$values['test_level'] =  ucwords($testLevel);
	$values['startspeed'] =  $startspeed;
	$values['testweight'] =  $testweight;
	$values['testMinute'] =  $testMinute;
	$values['testSeconds'] =  $testSeconds;
	$values['numberOfHeartRate'] =  $numberOfHeartRate;
	$values['heartRateOnStart'] =  $heartRateOnStart;
	$values['heartRateOnEnd'] =  $heartRateOnEnd;
	$values['testweight'] =  $testweight;
	$values['heart_rate_start_without_test'] =  $heartRateStartWithoutTest;
	$values['heart_rate_end_without_test'] =  $heartRateEndWithoutTest;
	$testdata = $this->getLoadValueForSpeed($values);
	
	if(!isset($testdata['status']))
	{
		$testTableData = $testdata['test_data'];
		$testParameterTableData = $testdata['test_data_parameter'];
		
		$comparisonField['r_user_id'] = $testTableData['r_user_id'];
		$comparisonField['test_start_date'] = $testTableData['test_start_date'];
		$comparisonField['r_club_id'] = $testTableData['r_club_id'];
		//$comparisonField['test_level'] = $testTableData['test_level'];
		$comparisonField['created_by'] = $testTableData['created_by'];
		$comparisonField['status'] = 3;
		$comparisonField['manual_entry'] = 2;
		$orderBy = 'modified_date';
		$data = $this->getdata('t_user_test',$comparisonField,$orderBy);
		
		//pr($testTableData);die;
		
		if (empty($data))
		{
			$testTableData['manual_entry'] = 2;
			$savedId = $this->add_data('t_user_test',$testTableData);
		}
		else
		{
			unset($testTableData['created_date']);
			$id = $data->user_test_id;
			$this->db->where('user_test_id',$id);
			$this->db->update('t_user_test',$testTableData);
			$this->db->trans_complete();
			$savedId = $id;
		}
		
		if($savedId)
		{
			
			$comparisonParamField['r_user_test_id'] = $savedId;
			$paramdata = $this->getdata('t_user_test_parameter',$comparisonParamField,'user_test_parameter_id');
			$testParameterTableData['r_user_test_id']  = $savedId;
			//echo "<pre>";print_r($paramdata);die;
			if (empty($paramdata))
			{
				$this->add_data('t_user_test_parameter',$testParameterTableData);
			}
			else
			{
				$paramid = $paramdata->user_test_parameter_id;
				$this->db->where('user_test_parameter_id',$paramid);
				$this->db->update('t_user_test_parameter',$testParameterTableData);
				$this->db->trans_complete();
			}
		}
		$resCallBack['status'] = '200';
	}
	
	//die;
	return $resCallBack;
	
}

function getLoadValueForSpeed($comingdata) 
{
	$finalData = array('status'=>'101');
	$startspeed = $comingdata['startspeed'];
	$testweight = $comingdata['testweight'];
	$endspeed = $comingdata['startspeed'] + 25;
	
	$loadobj = array();
	$starttime = 0;
	
	for ($i = $startspeed; $i <= $endspeed; $i = $i + 0.5) 
	{
		$newload = $this->getWattFromSpeed($i,$testweight);
		if ($i == $startspeed) 
		{
			$warmup1 = $newload * 0.7;
			$warmuptimediff1 = 60;
			$starttime += $warmuptimediff1;
			$data = array("speed"=>$i,"time"=>$starttime,"timediff"=> $warmuptimediff1,"load"=>round($warmup1),"loadtype"=>1);
			array_push($loadobj,$data);
			
			$warmup2 = $newload * 0.8;
			$warmuptimediff2 = 60;
			$starttime += $warmuptimediff2;
			$data = array("speed"=>$i,"time"=>$starttime,"timediff"=> $warmuptimediff2,"load"=>round($warmup2),"loadtype"=>1);
			array_push($loadobj,$data);
		   
			$warmup3 = $newload * 0.9;
			$warmuptimediff3 = 30;
			$starttime += $warmuptimediff3;
			$data = array("speed"=>$i,"time"=>$starttime,"timediff"=> $warmuptimediff3,"load"=>round($warmup3),"loadtype"=>1);
			array_push($loadobj,$data);
		}
		$timediff = $this->getTimeForSpeed($i);
		$starttime += $timediff;
		$data = array("speed"=>$i,"time"=>$starttime,"timediff"=> $timediff,"load"=>round($newload),"loadtype"=>2);
		array_push($loadobj,$data);
	}
	
	/*$realClosest = $comingdata['testSeconds'];
	$this->testtimewithwarmup = $realClosest+$this->warmuptime;
	$this->testTime = $this->warmuptime+$comingdata['testSeconds']+$this->cooldowntime;
	$this->coolDownTime = ($this->testtimewithwarmup)-$this->cooldowntime;*/
	
	$realClosest = $comingdata['testSeconds']+300;
	$this->testtimewithwarmup = $realClosest-$this->cooldowntime;
	$this->testTime = $realClosest;
	//$this->coolDownTime = ($this->testtimewithwarmup)-$this->cooldowntime;
	$this->coolDownTime = $this->testtimewithwarmup;
	$closestTime = $this->getClosest($this->testTime,$loadobj);
	$loadArray =  array_filter($loadobj, function($e) use( &$closestTime) {
								return ($e['time'] <= $closestTime);
							}
						);
	$countObj =  count($loadArray);
	$index  = $countObj-2;
	$finalData = array();
	if($loadArray[$index]['load'] >= 60)
	{
		$finalData = $this->createFinalTestData($loadobj,$comingdata);
	}
	else
	{
		$finalData = array('status'=>'101');
	}
	return $finalData;
	die;
}


	function getWattFromSpeed($speed,$weight)
	{
		$getSpeed = ($speed*2.98)+4.25;
		$multiplyByPoint9 = $getSpeed*0.9;
		$subtractWeight = ($multiplyByPoint9*$weight)-320;
		$finalValue = $subtractWeight/11.35;
		$data = round($finalValue);
		return $data;
	}

	function getTimeForSpeed($speed) 
	{
		$speedMultiply =  $speed *1000;
		$finalValue = (200/$speedMultiply)*(60*60);
		$data = round($finalValue);
		return $data ;
	}

	

	function getClosest($search, $arr) {
	   $closest = null;
	   foreach ($arr as $item) 
	   {
		  if ($closest === null || abs($search - $closest) > abs($item['time'] - $search)) 
		  {
			 $closest = $item['time'];
		  }
	   }
	   return $closest;
	}


	function createFinalTestData($load,$data)
	{
		$coolDownTime = $this->coolDownTime;
		$testtime = $this->testTime;
		$testtimewithwarmup =  $this->testtimewithwarmup;
		
		$closestTime = $this->getClosest($testtime,$load);
		
		$loadArray = array();
		$heartrateArray = array();
		
		$heartRateWithoutTestStart =  $data['heart_rate_start_without_test'];
		$heartRateWithoutTestEnd =  $data['heart_rate_end_without_test'];
		
		$lengthWithoutTest = ($heartRateWithoutTestEnd - $heartRateWithoutTestStart + 1) / 150;
		for( $i = 0;$i < 150;$i++)
		{
			$rangeWithoutTest[$i] = floor($lengthWithoutTest * ($i-1) + $heartRateWithoutTestStart)	 ; 
		}
		
		$heartRateTestStart =  $data['heartRateOnStart'];
		$heartRateTestEnd =  $data['heartRateOnEnd'];
		
		//echo $coolDownTime."<br/>";
		//$coolDownTime = 1361;
		$lengthWithTest = ($heartRateTestEnd - $heartRateTestStart + 1) / $coolDownTime;
		for( $i = 150;$i <= $coolDownTime;$i++)
		{
			$rangeWithTest[$i] = floor($lengthWithTest * ($i-1) + $heartRateTestStart)	 ; 
		}
		
		$loadArray =  array_filter($load, function($e) use( &$closestTime) {
									return ($e['time'] <= $closestTime);
								}
							);
		
		$heartRate = new \stdclass();
		$initial = 0;
		$decrementStep = 10;
		$decrementCount = 9;
		$countLoadArray = count($loadArray);
		foreach($loadArray as $key => $lod)
		{
			if($key == 0)
			{
				$startindex = 0;
			}
			else
			{
				$startindex = $loadArray[$key-1]['time'];
				
			}
			$index = 0;
			$res = array();
			for($innerindex = $startindex;$innerindex < $lod['time'];)
			{
				if($lod['time'] <= 150)
				{
					$res[$index]['data'] =  $rangeWithoutTest[$innerindex];
					$res[$index]['time'] =  $innerindex;
				}
				else
				{
					if($innerindex <= $coolDownTime)
					{
						$res[$index]['data'] =  $rangeWithTest[$innerindex];
						$res[$index]['time'] =  $innerindex;
					}
					else
					{
						$res[$index]['data'] =  $heartRateTestEnd;
						$res[$index]['time'] =  $innerindex;
						if($decrementStep == $decrementCount)
						{
							$decrementCount =0;
							$heartRateTestEnd--;
						}
						$decrementCount++;
					}
				}
				$innerindex++;
				$index++;
			}
			$heartRate->$initial = $res;
			if($key == ($countLoadArray-1))
			{
				$loadArray[$key]['loadtype'] =3;
			}
			$initial++;
		}
		
		$workload = json_encode($loadArray,JSON_NUMERIC_CHECK);
		$heartRateValue = json_encode($heartRate,JSON_NUMERIC_CHECK);
		$totalLoad = json_encode($load,JSON_NUMERIC_CHECK);
		
		$date = date('Y-m-d H:m:s');
		
		$saveTestData['test_data']['r_user_id'] = $data['user_id'];
		$saveTestData['test_data']['test_start_date'] = $data['test_start_date'];
		$saveTestData['test_data']['cool_down_start_time'] = $coolDownTime;
		$saveTestData['test_data']['cool_down_stop_time'] = $testtime;
		$saveTestData['test_data']['current_test_time'] = gmdate("H:i:s", $testtime);
		$saveTestData['test_data']['created_by'] = $data['coach_id'];
		$saveTestData['test_data']['weight'] = $data['weight'];
		$saveTestData['test_data']['r_club_id'] = $data['club_id'];
		$saveTestData['test_data']['r_device_id'] = 1;
		$saveTestData['test_data']['test_level'] = $data['test_level'];
		$saveTestData['test_data']['r_test_id'] = 1;
		$saveTestData['test_data']['status'] = 3;
		$saveTestData['test_data']['created_date'] = $date;
		$saveTestData['test_data']['modified_date'] = $date;
		$saveTestData['test_data']['total_load_value'] = $totalLoad;
		$saveTestData['test_data']['load_value'] =  $workload;
		$saveTestData['test_data']['heart_rate_value'] =  $heartRateValue;
		
		$comparisonField['r_user_id'] = $data['user_id'];
		$dobObj = $this->getdata('t_personalinfo',$comparisonField);
		$dob = $dobObj->dob;
		
		$from = new DateTime($dob);
		$to   = new DateTime();
		
		$age =  $from->diff($to)->y;
		
		$saveTestData['test_data_parameter']['r_user_id'] = $data['user_id'];
		$saveTestData['test_data_parameter']['age_on_test'] = $age;
		$saveTestData['test_data_parameter']['weight_on_test'] = $data['weight'];
		$saveTestData['test_data_parameter']['test_prepared_date'] = $data['test_start_date'];
		$saveTestData['test_data_parameter']['fitness_level'] = 5;
		$saveTestData['test_data_parameter']['test_level'] = $data['test_level'];
		$saveTestData['test_data_parameter']['test_date'] = $data['test_start_date'];
		
		return $saveTestData;
		die;
	}
	
	public function getdata($tablename,$comparisonField = array(),$orderby = '')
	{
		$this->db->select('*');
		$this->db->from($tablename);
		if(!empty($comparisonField))
		{
			foreach($comparisonField as $key => $val)
			{
				$this->db->where($key,$val);
			}
		}
		if($orderby != '')
		{
			$this->db->order_by($orderby,'desc');
			$this->db->limit(1);
		}
		$query = $this->db->get();
		$result=$query->row();
		return $result;
		die;
	}
	
	

	
public function get_user_weight($user_id){
		$this->db->select('*');
		$this->db->from('t_user_test');
		$this->db->where('r_user_id',$user_id);
		$this->db->order_by('user_test_id','desc');
		$query = $this->db->get();
		$result=$query->row();
   if(!empty($result))
   {
     
		return $result;
   }
   else{
		return false;
    }
	
}
// Add extra credit

 function insert($table_name,$values)
 {
  $this->db->insert($table_name,$values);
  $db_error = $this->db->error();
  if($db_error['code'] != 0)
  {
   log_message('error',$db_error['message']);
   return FALSE;
  }
  //echo $this->db->last_query();
  if($this->db->affected_rows() == 1) 
  {
      return $this->db->insert_id();
  }
  return FALSE;
 }
}

?>
