<?php 
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Workout_model extends CI_Controller {
	 public function __construct()
		{
		    parent::__construct();
			
		}
	public function week_workout($user_id,$workout,$program_id,$program_type,$action)
	{
		 $insert_id = array();
		$this->db->where('r_user_id', $user_id);
		$this->db->where('r_strength_pgmid', $program_id);
		$this->db->delete('t_workout_week');
		foreach($workout as $key=>$work)
		{
		
			$data = array('r_user_id' => $user_id,
						'r_strength_pgmid' => $program_id,
						'program_type' => $program_type,
						'r_week' => $work->week,
						'series' => $work->series,
						'reps' => $work->reps,
						'time_sec' => $work->time_sec,
						'strength'  => $work->strength,
						'pause' => $work->pause,
						'created_on' => date("Y-m-d")); 
						
			$this->db->insert('t_workout_week', $data);
			$insert_id[] = $this->db->insert_id();
			
			
		}
		if($action == '1' || $action == '5')
		{
			$this->go_to_week_one($user_id);
		}
		
		if($insert_id){
			
			return $insert_id;
		}
		else{
			return false;
		}
		//return $insert_id;
	}

	public function go_to_week_one($user_id)
	{
		$t=date('d-m-Y');
				 $curr_day = date("D",strtotime($t));
				if($curr_day=='Mon')
				 {
					 $data['week_start_date']=$start_date = date('Y-m-d');
					 $data['week_end_date'] = date('Y-m-d',strtotime('+6 day', strtotime($start_date)));
				} 
			 	else{
					
					$date = date('Y-m-d');
					$start_date= date('Y-m-d', strtotime('next monday', strtotime($date)));
					 $data['week_start_date'] = date('Y-m-d', strtotime('next monday', strtotime($date)));
					 $data['week_end_date'] = date('Y-m-d',strtotime('+6 day', strtotime($start_date)));
				}  
				$data['week']=1;
				$this->db->where('r_user_id', $user_id);
				$this->db->update('t_strength_user_test_week', $data);
	}

 }

?> 