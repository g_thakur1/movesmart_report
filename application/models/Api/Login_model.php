<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Login_model extends CI_Model {
	 
	 public function login($email,$password)
	 {
		
		 $this->db->select('t_users.*');
		 $this->db->from('t_users');
		// $this->db->join('t_personalinfo','t_personalinfo.r_user_id=t_users.user_id');
		 $this->db->where('email',$email);
		 $this->db->where('password',$password);
		 $this->db->where('r_usertype_id',1);
		 $this->db->where('r_status_id',1);
		 $this->db->where('is_deleted',0);
		 $query = $this->db->get();
		 if($query->num_rows()>0)
		 {
			 
			return $result = $query->result();
			
		 }
		 else
		 {
			 
			 return false;
		 }
	 }
	 public function member_info($Coach_id)
	 {
		
		  $this->db->select('t_users.*,t_coach_member.r_user_id,t_personalinfo.*');
		 $this->db->from('t_coach_member');
		 $this->db->join('t_users','t_users.user_id=t_coach_member.r_user_id');
		 $this->db->join('t_personalinfo','t_personalinfo.r_user_id=t_coach_member.r_user_id');
		 $this->db->where('r_coach_id',$Coach_id);
		 $query = $this->db->get();
		// echo $this->db->last_query();
		// die;
		 if($query->num_rows()>0)
		 {
			 return $result = $query->result();
			
		 }
		 else
		 {
			 
			 return false;
		 }
	 }
	 
	 public function get_device_id($device_name,$device_code){
		$returndata = array();
		foreach(explode(",",$device_code) as $res)
		 {	
		 $query = $this->db->query("SELECT * FROM `t_device` WHERE `r_device_type_id` =1 AND `device_code`='$res'");
		 if($query->num_rows()>0)
		 {
		 $result = $query->row();
		 $r_device_id = $result->device_id;
		 
		 $device_user=$this->device_user_detail($r_device_id);
		
			 if($device_user==false)
			 {  
				$data['user_data']=(object)array();
				$data['device_id'] = $res;
				$data['is_registered'] = 0;
			 }
			 else{
				 
				
				$user_id = $device_user->user_id;
				$device_user->program=$this->get_program($user_id);
				 $data['user_data'] = $device_user;
				 $data['is_registered'] = 1;
				 $data['device_id']     = $res;
				 
			  }
				
		   }
		   
		  else{
			    $data['user_data']=(object)array();
				$data['device_id'] = $res;
				$data['is_registered'] = 0;
			  
		   }
		   
		   $returndata[] = $data;
		   
		   
		}
		
		return $returndata;
	 } 
	 public function device_user_detail($deviceid){
		
		
		 $this->db->distinct('r_user_id');
		 $this->db->select('t_device_member.r_user_id,t_device_member.r_device_id,t_users.*,t_personalinfo.*');
	     $this->db->from('t_device_member');
		 $this->db->join('t_users','t_device_member.r_user_id = t_users.user_id','inner');
		  $this->db->join('t_personalinfo','t_personalinfo.r_user_id = t_users.user_id','inner');
		 $this->db->where('r_device_id',$deviceid);
		 $query = $this->db->get();
		 if($query->num_rows() > 0)
		 {    
			 return $query->row();	 
		 }
		 else{
			 return false;					 
		 }
		 
	 }
	 
	 
	
	 
	 
	 public function check_id($hr_monitor_id){
		
		  $this->db->select('*');
	     $this->db->from('t_device');
		 $this->db->where('device_code',$hr_monitor_id);
		 $this->db->where('r_device_type_id',1);
		 $query = $this->db->get();
		 
		 if($query->num_rows()>0){
		$result['count'] = $query->num_rows();
		$record = $query->row();
		$result['device_id'] = $record->device_id;
		  return $result;
		  			 
		 }
		 else{
			  return 0;
		 }
	 }
	 public function insert_device($data,$table){
		
		   $this->db->insert($table,$data);
		   $insert_id = $this->db->insert_id();

		   return  $insert_id;
		 
		
	 }
	 public function check_client($insert_id,$client_id)
	 {
		 
		 $this->db->select('*');
	     $this->db->from('t_device_member');
		 $this->db->where('r_device_id',$insert_id);
		 $this->db->where('r_user_id',$client_id);
		 $query = $this->db->get();
		 
		 if($query->num_rows()>0){
			
		$result = $query->num_rows();
	
		  return $result;
		  			 
		 }
		 else{
			  return 0;
		 }
	 }
	 public function check_valid_user($client_id)
	 {
		 
		 $this->db->select('*');
	     $this->db->from('t_users');
		 $this->db->where('user_id',$client_id);
		// $this->db->where('last_name',$last_name);
		 $query = $this->db->get();
		 
		 if($query->num_rows()>0){
			
		$result = $query->num_rows();
	
		  return $result;
		  			 
		 }
		 else{
			  return 0;
		 }
	 }
	 public function check_lastname_user($last_name)
	 {
		
		 $this->db->select('*');
	     $this->db->from('t_users');
		// $this->db->where('user_id',$client_id);
		 $this->db->join('t_personalinfo','t_personalinfo.r_user_id = t_users.user_id','inner');
		 $this->db->where('last_name',$last_name);
		 $query = $this->db->get();
		 $result = $query->result();
		 if(!empty($result)){
			
		  return $result;
		  			 
		 }
		 else{
			  return 0;
		 }
	 }
	 public function device_user($client_id){
		 $this->db->select('*');
	     $this->db->from('t_users');
		  $this->db->join('t_personalinfo','t_personalinfo.r_user_id = t_users.user_id','inner');
		 $this->db->where('user_id',$client_id);
		 $query = $this->db->get();
		 
		 if($query->num_rows()>0){
			
		$result = $query->row();
		$result->program = $this->get_program($client_id);
		  return $result;
		  			 
		 }
		 else{
			  return 0;
		 }
	 }
	 public function get_program($user_id){
		 $data = array('program_name'=>'free',
						'machine_name'=>'Leg Press VR1',
						'machine_id'=>'MY00010',
						'machine_image'=>'',
						'video_url'=>'https://vimeo.com/57910221',
						'image_url'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSWTdr9bsBi5JKqOLQhvYPYfpyvpxpgxdoQwnP3px-B4wg62Bhl',
						'series'=>2,
						'reps'=>40,
						'pause'=>60,
						'time_sec'=>60,
						'strength'=>'30%',
						'pin'=>5,
						);
		return $data;
		 
	 }
	 
	 public function get_workout_info($user_id)
	 {
		
		$query2 = "select * from t_workout_week AS TWW where TWW.r_user_id = $user_id and TWW.r_week = (select week from t_strength_user_test_week where r_user_id=$user_id order by strength_user_test_week_id desc) order by TWW.id desc limit 1";
		$data2 = $this->db->query($query2);
		$result = $data2->row_array();
		
		return $result;
		
	 }
	 
	 public function get_machine_info($user_id)
	 {
		 
		$query = "select * from t_strength_user_test where r_user_id=".$user_id." order by strength_user_test_id desc";
		$result = $this->db->query($query);
		return $result->row_array();
	 }
	 public function get_other_machine_info($program_id,$user_id,$machine_id,$program_type)
	 {  
		date_default_timezone_set('Asia/Kolkata');
		$curr = date('Y-m-d G:i:s');
		$less = array('1'=>'FREE','2'=>'MIX','3'=>'CIRCUIT');
		$sql = $this->db->query('select * from t_time_slots where days = "'.date('l').'" and time >= "'.date('G').':00" and time < "'.date('G',strtotime('+1 hour', strtotime($curr))) .':00" and lesson = "'.$less[$program_type].'" and clubId=(select r_club_id from t_users where user_id = '.$user_id.')'); 
		
		//echo $this->db->last_query();
		$data = $sql->result();
		$current_date = date('Y-m-d');
		$week_sql = $this->db->query('select * from t_strength_user_test_week where r_user_id='.$user_id.' and week_start_date <="'.$current_date.'" and week_end_date >="'.$current_date.'" order by strength_user_test_week_id desc');
		$week_data = $week_sql->row();
		
	 // if($program_id != "" && !empty($data) && !empty($week_data))
		  if($program_id != "")
	  { 
		//$query = "select TSM.*, TBW.position, TBW.weight from t_strength_machine AS TSM LEFT JOIN t_bolck_weight AS TBW ON TSM.strength_machine_id = TBW.strength_machine_id  where TSM.strength_machine_id in (select r_machine_id from t_strengthpgm_map_machine where r_user_id = $user_id and r_strength_pgmid = $program_id )";
		
		/////// $query = "select TSM.*, TBW.position, TBW.weight from t_strength_machine AS TSM LEFT JOIN t_bolck_weight AS TBW ON TSM.strength_machine_id = TBW.strength_machine_id  where TSM.strength_machine_id = $machine_id";
		 ////
		 $query = "select * from t_strength_machine AS TSM where TSM.strength_machine_id = $machine_id";
		$data = $this->db->query($query);
		$result = $data->row_array();
		$result['weight'] = $this->getweight($machine_id);
		$query1 = "select title_english,type,strength_program_id as program_id from t_strength_program AS TSP where TSP.strength_program_id = $program_id";
		$data1 = $this->db->query($query1);
		$result1 = $data1->row_array();
		
		
		/* $query2 = "select reps, series, time_sec, pause, strength from t_workout_week AS TWW where TWW.r_strength_pgmid =$program_id and r_week = (select week from t_strength_user_test_week where r_user_id=$user_id)";
		$data2 = $this->db->query($query2);
		if(empty($data2->result()))
		{ */
			$query2 = "select reps, series, time_sec, pause, strength from t_strength_program_week AS TSPW where TSPW.r_strength_pgmid =$program_id and r_week = (select week from t_strength_user_test_week where r_user_id=$user_id order by strength_user_test_week_id desc limit 1) order by TSPW.r_strength_pgmid desc limit 1";
			$data2 = $this->db->query($query2);
			
		//}
		$result2 = $data2->row_array();
		$total_pin = count($result['weight']);
		$strength = $result2['strength'];
		
		$pin = round(($total_pin*$strength)/100);
		$result['position']=$pin;
		$array['responce'] = array(
						'title_english' => $result1['title_english'],
						'program_type' => $result1['type'],
						'program_id'=> $result1['program_id'],
						'program_week' => $result2,
						'machine_info' => $result,
						
		
		);
		$array['status'] = 1;
		return $array;
		 } else {
	 
		 $empty['responce'] = (object)array();
		 $empty['status'] = 0;
		 return $empty;
	 } 
	 }
	 
	 
	 public function getweight($machine_id){
		 $query = "select * from t_bolck_weight where strength_machine_id=".$machine_id;
		$result = $this->db->query($query);
		$result = $result->result();
		if(!empty($result)){
			return $result;
		}
		else{
			return false;
		}
		//return $result->row_array();
		 
	 }
	 
	  
 }
 ?>