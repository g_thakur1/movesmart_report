<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mind_switch_model extends CI_Model {

function insert_data($post_data)
{
	
  $this->db->insert('t_testing_measuring_transaction', $post_data);
   $insert_id = $this->db->insert_id();

   return  $insert_id;
 
}
/*************for overall result inserttion of correct level******/
public function insert_result($data)
{
  $this->db->insert('t_test_overall_result_level', $data);
   $insert_id = $this->db->insert_id();

   return  $insert_id;
 
}
/**********for check if result exist ************/
public function get_result($userId)
{
	 $this->db->select('*');
	 $this->db->from('t_test_overall_result_level');
	 $this->db->where('r_user_id',$userId);
	  $this->db->order_by('t_test_overall_result_level_id','desc');
	$query = $this->db->get();
	$result = $query->row();
	if(!empty($result))
	{
		return $result;
	}
	else
	{
		return false;
	}
	
}
/*************Updating Personal Goal***************/ 
public function personal_goal($userId,$target)
	{ 
	
	//$this->db->query('SELECT MAX(user_test_parameter_id) FROM t_user_test_parameter WHERE r_user_id=$userId;');
	//$query = $this->db->get();
	//$max_result = $query->row();
	//$this->db->select(MAX(user_test_parameter_id));
	 
	 $this->db->select_max('user_test_parameter_id');
	 $this->db->from('t_user_test_parameter');
	 $this->db->where('r_user_id',$userId);
	 $querys = $this->db->get();
	 $max_result = $querys->row();

	 $result = $this->db->query("UPDATE t_user_test_parameter SET target='$target' WHERE user_test_parameter_id=$max_result->user_test_parameter_id");
	    return $result;
	} 
	/*****************for checking user exist in the mind table ************/
	public function get_mind_status($user_id,$create_date)
	{
		$this->db->select('*');
		$this->db->from('t_mind_activity');
		$this->db->where('user_id',$user_id);
		//$this->db->where('mind_status',1);
		$this->db->where('c_date',$create_date);
		$query = $this->db->get();
		//$result = $query->num_rows();
		$result = $query->row();
		if($result>0)
		{
			return $result;
		}
		else
		{
			return 0;
		}
	
	}
	/**************save status for mind tab**********/
public function save_mind_status($data)
{
  $this->db->insert('t_mind_activity', $data);
   $insert_id = $this->db->insert_id();

   return  $insert_id;
 
}	
	
	public function get_user_details($user_id)
	{
		$this->db->select('t_mind_activity.*,tu.first_name,tu.last_name,tu.associated_company_id,tu.email as email,tu1.email as coachemail,tu1.first_name as coachfirst_name,tu1.last_name as coachlastname');
		$this->db->from('t_mind_activity');
		$this->db->join('t_users as tu','tu.user_id=t_mind_activity.user_id','inner');
		$this->db->join('t_users as tu1','tu1.user_id=t_mind_activity.coach_id','inner');
		   $this->db->where('t_mind_activity.user_id',$user_id);
		   $this->db->order_by('t_mind_activity.id','desc');
		   $query = $this->db->get();
		$result = $query->result();
		
		if(!empty($result))
		{
		
			return $result[0];
		}
		else
		{
			return 0;
		}
	}
	public function update_mind_activities($data){
				
		$this->db->select('*');
		$this->db->from('t_mind_activity');
		$this->db->where('user_id',$data['user_id']);
		$this->db->where('mind_status',1);
		$query = $this->db->get();
		$rows = $query->num_rows();
		if($rows > 0)
		{
			$eta['mind_status'] = 0; 
		$this->db->where('user_id',$data['user_id']);
		$this->db->where('coach_id',$data['coach_id']);
		$result = $this->db->update('t_mind_activity',$eta);
		//echo '<pre>';print_r($result);die;
		return 1;
		}
		else
		{
			return 0;
		}
			
		
	}

}

?>
