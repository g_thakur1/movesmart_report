<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mail_send_model extends CI_Model {
	
	public function getMailTemplate($slug)
	{
		$this->db->select('subject,content');
		$this->db->from('t_email_templates');
		$this->db->where('slug',$slug);
		$query = $this->db->get();
		$result = $query->result();
		if(!empty($result))
		{
		
			return $result[0];
		}
		else
		{
			return 0;
		}
	}
	
	
	public function getCompanyData($id)
	{
		$this->db->select('company_name,company_phone_number,company_address,logo');
		$this->db->from('t_associated_companies');
		$this->db->join('t_associated_companies_designs', 't_associated_companies.company_id = t_associated_companies_designs.associated_company_id', 'left');
		$this->db->where('company_id',$id);
		$query = $this->db->get();
		$result = $query->result();
		if(!empty($result))
		{
		
			return $result[0];
		}
		else
		{
			return 0;
		}
	}
}

?>
