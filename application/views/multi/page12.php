<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Move Smart</title>
    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
        <style>
    	body{font-family: 'Open Sans', sans-serif;
    	color: #646464;}
    </style>
</head>
<body>
	<div class="container">
	<div class="row">
		<?php 
			/*if($company == 'feelgood') {
				$dynamic = array();
	            $dynamic['company_color']	= '#bb61b7';
	            $dynamic['inner_logo']   	= 'images/innerlogo.png';
	            $dynamic['company_name']   	= 'FeelGood';
	            $dynamic['level_old']['page12'] = 'images/movesmart_level' .$eat_mind_old['Mind_switch_total_level'].'.png';
	            $dynamic['level_new']['page12'] = 'images/movesmart_level' .$mind_new['Mind_switch_total_level'].'.png';
			} elseif ($company == 'movesmart') {
				$dynamic = array();
	            $dynamic['company_color']  	= '#0095ac';
	            $dynamic['inner_logo']   	= 'moveimage/innerlogo.png';
	            $dynamic['company_name']   	= 'MoveSmart';
	            $dynamic['level_old']['page12'] = 'moveimage/movesmart_level' .$eat_mind_old['Mind_switch_total_level'].'.png';
	            $dynamic['level_new']['page12'] = 'moveimage/movesmart_level' .$mind_new['Mind_switch_total_level'].'.png';
			}*/
		?>
		<div class="col-md-12">
			<div style="border: 1px solid <?php echo $dynamic['company_color'] ?>; border-radius: 10px;float: left;width: 220px;">
				<img style="width:10%;" style="width: 100%;" src="<?php echo $dynamic['inner_logo'] ?>" alt="Logo">
			</div>
			<div style=" text-align: center; width: 176px;border: 1px solid <?php echo $dynamic['company_color'] ?>;border-radius: 10px;float: right;padding: 10px 7px;">
				<h2 style="font-size: 13px;margin: 0 0 4px;font-family: 'Open Sans', sans-serif;font-weight:bold;color:#646464;"><?php echo $dynamic['company_name'] ?> consult</h2>
				<p style="font-size: 13px;margin: 0 0 3px;font-family: 'Open Sans', sans-serif;font-weight:300;color:#646464;"><?php echo $user_detail->first_name; ?> &nbsp;&nbsp;<?php echo $user_detail->last_name; ?></p>
				<p style="font-size: 13px;margin: 0;font-family: 'Open Sans', sans-serif;color:#646464;">Testdatum <?php echo $latest_test_date; ?></p>
			</div>
<div style="float:left;width:100%;margin-top:3%;border: 1px solid <?php echo $dynamic['company_color'] ?>;border-radius: 10px;padding:3%">
	<h1 style="font-weight:300;margin-top:0px; color:<?php echo $dynamic['company_color'] ?>;">MIND level</h1>
	<div style="float:left;width:45%;text-align:center;">
		<div style="margin-top:2%;">
		<p style="margin:0;">Vorige Testdatum </p>
		<p style="margin:0;"><?php echo $eat_mind_old['old_test_date']; ?></p>
			<img style="width:50%"; src="<?php echo $dynamic['level_old']['page12'] ?>"> 
		</div>
	</div>
	<div style="float:left;width:45%;text-align:center;">
		<div style="margin-top:2%;">
		<p style="margin:0;">Laatste Testdatum </p>
		<p style="margin:0;"><?php echo $latest_test_date; ?></p>
			<img style="width:50%"; src="<?php echo $dynamic['level_new']['page12'] ?>"> 
		</div>
	</div>
	
	<div style="float:left;width:100%;">
		<p><?php echo $mind_comparision; ?></p>

	</div>
</div>
				<div style="float:left;width:100%;margin-top:1.5%">
				<h2 style="font-weight:300; margin:0px;color:<?php echo $dynamic['company_color'] ?>;">Credits</h2>  
				<p style="margin-top:0;">
				In het vorige consult heb je met je coach een doelstelling geformuleerd.Die was: <?php echo isset($goal)? $goal : 0;?>
				</p>
				<p style="">Je hebt de doelstelling behaald, want je bent gestegen op het domein waar je doelstelling lag. Als beloning dat je een level bent gestegen hebben we de credits die je de afgelopen weken ‘in thecloud’ hebt gespaard uitgekeerd ‘in the pocket’. Deze credits kun gebruiken om leuke en handige spullen te kopen. 

Met je coach heb je een nieuw doel gesteld en ga je de komende 12 weken opnieuw credits sparen en verder te werken aan een gezonde leestijl. Veel succes daarbij!
</p>
				</div>
			<div style="float:left;width:100%;margin-top:1%">
				<div style="float:left;width:13%;">
					<?php if (file_exists($coach_image)) { ?>
					<img style="width:60%;" src="http://movesmart.offshoresolutions.nl/backoffice/images/uploads/movesmart/profile/<?php echo $coach_image;?>"> 
					<?php }
					else{ ?>
						<img style="width:60%;" src="<?php echo base_url(); ?>/images/coach.png"> 
				<?php	} ?>	 
				</div>
				<div style="margin-left:3%;float:left;width:84%;">
				<h2 style="font-weight:300;color:<?php echo $dynamic['company_color'] ?>;margin-top:0;margin-bottom:10px;">Veel succes en trainingsplezier!</h2>
				<p style="margin:0;"><?php echo $coach_detail->first_name.' '.$coach_detail->last_name; ?></p>
				<p style="margin:0;"><?php echo $dynamic['company_name'] ?> Coach</p>
				</div>
			</div>

		</div>
	</div>
	</div>
</body>
</html>