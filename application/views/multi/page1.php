<!DOCTYPE html>
<html lang="en">
  <head>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Move Smart</title>
    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
<style>
.footer_image > img {float: left;width: 100%;}
.col{-md-12 {float: left;padding-bottom: 135px;position: relative;width: 100%;}
body{font-family: 'Open Sans', sans-serif;}
</style>
  </head>
  <body>

    <div class="container">
	<div class="row">
        <?php 
            // if($company == 'feelgood') {
            //     $dynamic = array();
            //     $dynamic['company_name']   = 'FeelGood';
            //     $dynamic['logo_image']     = 'images/logo.png';
            //     $dynamic['banner_image']   = 'images/banner.png';
            //     $dynamic['bg_color']       = 'rgb(162, 101, 145)';
            //     $dynamic['box_color']      = '#BB61B7';
            //     $dynamic['box_p_color']    = '#c161af;';
            //     $dynamic['box_h2_color']   = '#595B60';
            // } elseif ($company == "movesmart") {
            //     $dynamic = array();
            //     $dynamic['company_name']   = 'MoveSmart';
            //     $dynamic['logo_image']     = 'moveimage/logo.png';
            //     $dynamic['banner_image']   = 'moveimage/banner.png';
            //     $dynamic['bg_color']       = 'rgb(0, 149, 172)';
            //     $dynamic['box_color']      = '#68C4E3';
            //     $dynamic['box_p_color']    = '#0095ac;';
            //     $dynamic['box_h2_color']   = '#68C4E3';
            // }
        ?>
		<div class="col-md-12">
        	<div style="float:left;width:100%;text-align:center;" class="logo">
				<img style="width:40%;" src="<?php echo $dynamic['logo_image'] ?>" alt="logo">
            </div>
			<div class="banner_image" style="float:left;width:100%;">
            	<img style="width:100%;" src="<?php echo $dynamic['banner_image'] ?>" alt="banner">
            	<div style="font-weight:300;font-family:'Open Sans',sans-serif;margin: -72px 0 0;bottom:0;opacity: 0.7;color:#fff;background: <?php echo $dynamic['bg_color'] ?>;text-align:center;width:100%;" class="color-div"><p style="margin: 0;
    padding: 15px 0;font-size:32px;"><?php echo $dynamic['company_name'] ?> consult</p></div>
            </div>
            <div class="user-info" style="margin:0 auto ;width:40%;">
            	<div class="user-box" style="text-align: center;padding: 10px 20px; border:1px solid <?php echo $dynamic['box_color'] ?>;text-align::center;border-radius: 9px;float: left; width: 100%;margin-top: 60px;margin-bottom: 60px;">
                	<p style="color: <?php echo $dynamic['box_p_color'] ?>;font-size: 25px;margin:10px;font-family: 'Open Sans', sans-serif;">Rapport</p>
                    <h2 style="color: <?php echo $dynamic['box_h2_color'] ?>;font-size:18px;margin:10px;font-family: 'Open Sans', sans-serif;font-weight:300;"><?php echo $user_detail->first_name; ?> &nbsp;&nbsp;<?php echo $user_detail->last_name; ?></h2>
                    <p style="color:#646464;font-size:13px;margin:10px;font-family: 'Open Sans', sans-serif;">Testdatum <?php echo $latest_test_date; ?></p>
                </div>
            </div>

		</div>
	</div>
</div>
 </body>
</html>