<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Move Smart</title>
    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet">
    <style>
    	body{font-family: 'Open Sans', sans-serif;}
    </style>
</head>
<body>
	<div class="container">
	<div class="row">
		<?php 
			// if($company == 'feelgood') {
			// 	$dynamic = array();
	  //           $dynamic['company_color']	= '#bb61b7';
	  //           $dynamic['inner_logo']   	= 'images/innerlogo.png';
	  //           $dynamic['company_name']   	= 'FeelGood';
	  //           $dynamic['fresh']   		= 'images/fresh.png';
			// } elseif ($company == 'movesmart') {
			// 	$dynamic = array();
	  //           $dynamic['company_color']  	= '#0095ac';
	  //           $dynamic['inner_logo']   	= 'moveimage/innerlogo.png';
	  //           $dynamic['company_name']   	= 'MoveSmart';
	  //           $dynamic['fresh']   		= 'moveimage/fresh.png';
			// }
		?>
		<div class="col-md-12">
			<div style="border: 1px solid <?php echo $dynamic['company_color'] ?>; border-radius: 10px;float: left;width: 220px;">
				<img style="width:10%;" style="width: 100%;" src="<?php echo $dynamic['inner_logo'] ?>" alt="Logo">
			</div>
			<div style=" text-align: center; width: 176px;border: 1px solid <?php echo $dynamic['company_color'] ?>;border-radius: 10px;float: right;padding: 10px 7px;">
				<h2 style="font-size: 13px;margin: 0 0 4px;font-family: 'Open Sans', sans-serif;font-weight:bold;color:#646464;"><?php echo $dynamic['company_name'] ?> consult</h2>
				<p style="font-size: 13px;margin: 0 0 3px;font-family: 'Open Sans', sans-serif;font-weight:300;color:#646464;"><?php echo $user_detail->first_name; ?> &nbsp;&nbsp;<?php echo $user_detail->last_name; ?></p>
				<p style="font-size: 13px;margin: 0;font-family: 'Open Sans', sans-serif;color:#646464;">Testdatum <?php echo $latest_test_date; ?></p>
			</div>
				<div style="float:left;width:100%;margin-top:10%">
				<img style="width:50%;" src="<?php echo $dynamic['fresh'] ?>" alt="mindswitch">	
				</div>
			<div style="float:left;width:100%;margin-top:3%">
				<div style="float:left;width:13%;">
					<div style=" text-align:center;background: <?php echo $dynamic['company_color'] ?> none repeat scroll 0 0;border-radius: 50%;color: #fff;float: left;height: 100px;text-align: center;width: 100px;">
						<h1 style="font-size: 30px;margin-top:28px;"><?php echo $eat_new['body_composition'];?></h1>
					</div>
					<div style="text-align:center;color:#646464;">
						<p style="margin:0;">Lichaamssamenstelling</p>
					</div>
				</div>
				<div style="margin-left:3%;float:left;width:84%;color:#646464;">
				<p><?php echo $body_string;?></p>
				</div>
			</div>
			<div style="float:left;width:100%;margin-top:3%">
				<div style="float:left;width:13%;">
					<div style=" text-align:center;background: <?php echo $dynamic['company_color'] ?> none repeat scroll 0 0;border-radius: 50%;color: #fff;float: left;height: 100px;text-align: center;width: 100px;">
						<h1 style="font-size: 30px;margin-top:28px;"><?php echo $eat_new['viscreal_fat%'];?></h1>
					</div>
					<div style="text-align:center;color:#646464;">
						<p style="margin:0;">Visceraal<br>vet</p>
					</div>
				</div>
			
				<div style="margin-left:3%;float:left;width:84%;color:#646464;">
				<p><?php echo $visceral_string;?></p>
				</div>
			</div>
		</div>
	</div>
	</div>
</body>
</html>