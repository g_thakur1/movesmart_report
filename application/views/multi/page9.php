<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Move Smart</title>
    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet">
    <style>
    	body{font-family: 'Open Sans', sans-serif;}
    </style>
</head>
<body>
	<div class="container">
	<div class="row">
		<?php 
			/*if($company == 'feelgood') {
				$dynamic = array();
	            $dynamic['company_color']	= '#bb61b7';
	            $dynamic['inner_logo']   	= 'images/innerlogo.png';
	            $dynamic['company_name']   	= 'FeelGood';
	            $dynamic['default_coach']  	= base_url(). '/images/coach.png';
	            $dynamic['level_old']['page9'] = 'images/movesmart_level' .$movesmart_old['movesmart_total_level'].'.png';
	            $dynamic['level_new']['page9'] = 'images/movesmart_level' .$movesmart_new['movesmart_total_level'].'.png';
			} elseif ($company == 'movesmart') {
				$dynamic = array();
	            $dynamic['company_color']  	= '#0095ac';
	            $dynamic['inner_logo']   	= 'moveimage/innerlogo.png';
	            $dynamic['company_name']   	= 'MoveSmart';
	            $dynamic['default_coach']  	= base_url(). '/moveimage/coach.png';
	            $dynamic['level_old']['page9'] = 'moveimage/movesmart_level' .$movesmart_old['movesmart_total_level'].'.png';
	            $dynamic['level_new']['page9'] = 'moveimage/movesmart_level' .$movesmart_new['movesmart_total_level'].'.png';
			}*/
		?>
		<div class="col-md-12">
			<div style="border: 1px solid <?php echo $dynamic['company_color'] ?>; border-radius: 10px;float: left;width: 220px;">
				<img style="width:10%;" style="width: 100%;" src="<?php echo $dynamic['inner_logo'] ?>" alt="Logo">
			</div>
			<div style=" text-align: center; width: 176px;border: 1px solid <?php echo $dynamic['company_color'] ?>;border-radius: 10px;float: right;padding: 10px 7px;">
				<h2 style="font-size: 13px;margin: 0 0 4px;font-family: 'Open Sans', sans-serif;font-weight:bold;color:#646464;"><?php echo $dynamic['company_name'] ?> consult</h2>
				<p style="font-size: 13px;margin: 0 0 3px;font-family: 'Open Sans', sans-serif;font-weight:300;color:#646464;"><?php echo $user_detail->first_name; ?> &nbsp;&nbsp;<?php echo $user_detail->last_name; ?></p>
				<p style="font-size: 13px;margin: 0;font-family: 'Open Sans', sans-serif;color:#646464;">Testdatum <?php echo $latest_test_date; ?></p>
			</div>
<div style="float:left;width:100%;margin-top:3%;border: 1px solid <?php echo $dynamic['company_color'] ?>;border-radius: 10px;padding:3%">
	<h1 style="margin-top:0px; color:<?php echo $dynamic['company_color'] ?>;font-weight:300;">MOVE level</h1>
	<div style="float:left;width:45%;text-align:center;">
		<div style="margin-top:2%;">
		<p style="margin:0;color:#646464;">Vorige Testdatum </p>
		<p style="margin:0;color:#646464;"><?php echo $eat_mind_old['old_test_date']; ?></p>
			<img style="width:50%"; src="<?php echo $dynamic['level_old']['page9'] ?>"> 
		</div>
	</div>
	<div style="float:left;width:45%;text-align:center;">
		<div style="margin-top:2%;">
		<p style="margin:0;color:#646464;">Laatste Testdatum </p>
		<p style="margin:0;color:#646464;"><?php echo $latest_test_date; ?></p>
			<img style="width:50%"; src="<?php echo $dynamic['level_new']['page9'] ?>"> 
		</div>
	</div>
	
	<div style="float:left;width:100%;">
		<p><?php echo $move_comparision; ?></p>

	</div>
</div>
		</div>
	</div>
	</div>
</body>
</html>