<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Move Smart</title>
    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet">
    <style>
    	body{font-family: 'Open Sans', sans-serif;}
    </style>
</head>
<body>
	<div class="container">
	<div class="row">
		<?php 
			/*if($company == 'feelgood') {
				$dynamic = array();
	            $dynamic['company_color']	= '#bb61b7';
	            $dynamic['inner_logo']   	= 'images/innerlogo.png';
	            $dynamic['company_name']   	= 'FeelGood';
	            $dynamic['default_coach']  	= base_url(). '/images/coach.png';
			} elseif ($company == 'movesmart') {
				$dynamic = array();
	            $dynamic['company_color']  	= '#0095ac';
	            $dynamic['inner_logo']   	= 'moveimage/innerlogo.png';
	            $dynamic['company_name']   	= 'MoveSmart';
	            $dynamic['default_coach']  	= base_url(). '/moveimage/coach.png';
			}*/
		?>
		<div class="col-md-12">
			<div style="border: 1px solid <?php echo $dynamic['company_color'] ?>; border-radius: 10px;float: left;width: 220px;">
				<img style="width:10%;" style="width: 100%;" src="<?php echo $dynamic['inner_logo'] ?>" alt="Logo">
			</div>
			<div style=" text-align: center; width: 176px;border: 1px solid <?php echo $dynamic['company_color'] ?>;border-radius: 10px;float: right;padding: 10px 7px;">
				<h2 style="font-size: 13px;margin: 0 0 4px;font-family: 'Open Sans', sans-serif;font-weight:bold;color:#646464;"><?php echo $dynamic['company_name'] ?> consult</h2>
				<p style="font-size: 13px;margin: 0 0 3px;font-family: 'Open Sans', sans-serif;font-weight:300;color:#646464;"><?php echo $user_detail->first_name; ?> &nbsp;&nbsp;<?php echo $user_detail->last_name; ?></p>
				<p style="font-size: 13px;margin: 0;font-family: 'Open Sans', sans-serif;color:#646464;">Testdatum <?php echo $latest_test_date; ?></p>
			</div>
<div style="float:left;width:100%;margin-top:3%;border: 1px solid <?php echo $dynamic['company_color'] ?>;border-radius: 10px;padding:3%">
<h1 style="margin-top:0px;color:<?php echo $dynamic['company_color'] ?>;font-weight:300;">Jouw resultaat</h1>
<p style="color:#646464;">
Je zelfscore is samengesteld uit jouw antwoorden op de vragen over bewegen, eten en ontspannen en de gege

vens uit je ‘fitdagboek’. Aan de score kun je in 1 oogopslag zien hoe je ervoor staat en waar je aan kunt werken. Het 
systeem is simpel: groen is gezond, oranje betekent dat er aandacht nodig is en rood is een signaal dat er echt iets 
moet gebeuren. Je coach gaat je daar bij begeleiden.
</p>
<p style="color:#646464;">Daarnaast hebben we opnieuw een aantal metingen gedaan, om te kijken hoe je er nu voor staat op het gebied van 
leefstijl en vitaliteit. Door die te vergelijken met de uitkomsten van de vorige meting(en) wordt duidelijk of je jouw 
persoonlijke doelstelling hebt gehaald. Je hebt die resultaten al besproken met je coach, in dit rapport kun je ze 
teruglezen. </p>
<p style="color:#646464;">Hieronder vind je de vergelijking met de vorige meting.</p>
</div>			
<div style="float:left;width:100%;margin-top:3%;border: 1px solid <?php echo $dynamic['company_color'] ?>;border-radius: 10px;padding:3%">
	<h1 style="font-weight:300;margin-top:0px; color:<?php echo $dynamic['company_color'] ?>;"><?php echo $dynamic['company_name'] ?> score</h1>
	<div style="float:left;width:45%;text-align:center;">
		<div style="margin-top:2%;">
		<p style="margin:0;color:#646464;">Vorige Testdatum </p>
		<p style="margin:0;color:#646464;"><?php echo $eat_mind_old['old_test_date']; ?></p>
			<img style="width:50%"; src="chart/<?php echo $old_score_image; ?>"> 
		</div>
	</div>
	<div style="float:left;width:45%;text-align:center;">
		<div style="margin-top:2%;">
		<p style="margin:0;color:#646464;">Laatste Testdatum </p>
		<p style="margin:0;color:#646464;"><?php echo $latest_test_date; ?></p>
			<img style="width:50%"; src="chart/<?php echo $follow_score_image; ?>"> 
		</div>
	</div>
	
	<div style="float:left;width:100%;color:#000;">
		<p style="color:#646464;">Jouw <?php echo $dynamic['company_name'] ?> score is van &#60;&#60;<?php echo $score['average_old_score']; ?>&#62;&#62; naar &#60;&#60;<?php echo $score['average_new_score']; ?>&#62;&#62; gegaan. Dat betekent dat je leefstijl nauwelijks is veran
derd. Misschien heb je wel actief geprobeerd bepaalde onderdelen te verbeteren, maar is dat ten koste gegaan 
van de andere gebieden. Kijk eens goed naar de verhouding tussen MOVESMART, EATFRESH! en MINDSWITCH?, en 
bespreek met je coach hoe je ze in balans kunt brengen. </p>
	</div>
</div>
		</div>
	</div>
	</div>
</body>
</html>