<!DOCTYPE html>
<html lang="en">
  <head>
  	    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Move Smart</title>
    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <style>
    body{font-family: 'Open Sans', sans-serif;}
    </style>
</head>
<body>
	<div class="container"> 
	<div class="row"> 
		<?php 
			// if($company == 'feelgood') {
			// 	$dynamic = array();
	  //           $dynamic['company_color']	= '#bb61b7';
	  //           $dynamic['inner_logo']   	= 'images/innerlogo.png';
	  //           $dynamic['company_name']   = 'FeelGood';
	  //           $dynamic['circle1']   		= 'images/imagecircle1.png';
	  //           $dynamic['circle2']   		= 'images/imagecircle2.png;';
	  //           $dynamic['circle3']   		= 'images/imagecircle3.png';
	  //           $dynamic['circle4']   		= 'images/imagecircle4.png';
	  //           $dynamic['circle5']   		= 'images/imagecircle5.png';
	  //           $dynamic['circle6']   		= 'images/imagecircle6.png';
	  //           $dynamic['circle7']   		= 'images/imagecircle7.png';
	  //           $dynamic['move_smart']   	= 'images/movesmart.png';
			// } elseif ($company == 'movesmart') {
			// 	$dynamic = array();
	  //           $dynamic['company_color']  = '#0095ac';
	  //           $dynamic['inner_logo']   	= 'moveimage/innerlogo.png';
	  //           $dynamic['company_name']   = 'MoveSmart';
	  //           $dynamic['circle1']   		= 'moveimage/imagecircle1.png';
	  //           $dynamic['circle2']   		= 'moveimage/imagecircle2.png;';
	  //           $dynamic['circle3']   		= 'moveimage/imagecircle3.png';
	  //           $dynamic['circle4']   		= 'moveimage/imagecircle4.png';
	  //           $dynamic['circle5']   		= 'moveimage/imagecircle5.png';
	  //           $dynamic['circle6']   		= 'moveimage/imagecircle6.png';
	  //           $dynamic['circle7']   		= 'moveimage/imagecircle7.png';
	  //           $dynamic['move_smart']   	= 'moveimage/movesmart.png';
			// }
		?>
		<div class="col-md-12">
			<div style="border: 1px solid <?php echo $dynamic['company_color'] ?>; border-radius: 10px;float: left;width: 220px;">
				<img style="width:10%;" style="width: 100%;" src="<?php echo $dynamic['inner_logo'] ?>" alt="Logo">
			</div>
			<div style=" text-align: center; width: 176px;border: 1px solid <?php echo $dynamic['company_color'] ?>;border-radius: 10px;float: right;padding: 10px 7px;">
				<h2 style="font-size: 13px;margin: 0 0 4px;font-family: 'Open Sans', sans-serif;font-weight:bold;color:#646464;"><?php echo $dynamic['company_name'] ;?> consult</h2>
				<p style="font-size: 13px;margin: 0 0 3px;font-family: 'Open Sans', sans-serif;font-weight:300;color:#646464;"><?php echo $user_detail->first_name; ?> &nbsp;&nbsp;<?php echo $user_detail->last_name; ?></p>
				<p style="font-size: 13px;margin: 0;font-family: 'Open Sans', sans-serif;color:#646464;">Testdatum <?php echo $latest_test_date; ?></p>
			</div>
			<div style=" border: 1px solid <?php echo $dynamic['company_color'] ?>;border-radius: 10px;float: left;margin-top: 4%;padding: 1% 2%;width: 96%;">
				<div style="color:<?php echo $dynamic['company_color'] ?>;"><p style="margin: 0 0 0 0;font-size:25px;font-weight:300;">Beste <?php echo $user_detail->first_name; ?>,</p></div>
				<p style="color:#646464;" >Op <?php echo $latest_test_date; ?>  ben je op vervolgconsult geweest bij je <?php// echo $club->club_name; ?>FeelGood coach. Er zijn nieuwe metingen gedaan, en jullie hebben je voortgang besproken. Wat lukt er wel? Wat niet? Waar moet er worden bijge stuurd? Je MOVE -, EAT-  en MIND levels zijn opnieuw bepaald en je hebt nieuwe doelstellingen opgesteld. De aftrap voor de volgende stap op weg naar vitaler leven!</p>			
			</div>
			<div style="text-align:center;width:100%;float:left;margin-top:2%">
				<img style="width:10%;" style="width:10%;" src="<?php echo $dynamic['circle1'] ?>" alt="imagecircle1">
				<img style="width:10%;" src="<?php echo $dynamic['circle7'] ?>" alt="imagecircle7">
				<img style="width:10%;" src="<?php echo $dynamic['circle5'] ?>" alt="imagecircle5">
				<img style="width:10%;" src="<?php echo $dynamic['circle4'] ?>" alt="imagecircle4">
				<img style="width:10%;" src="<?php echo $dynamic['circle3'] ?>" alt="imagecircle3">
				<img style="width:10%;" src="<?php echo $dynamic['circle2'] ?>" alt="imagecircle2">
				<img style="width:10%;" src="<?php echo $dynamic['circle6'] ?>" alt="imagecircle6">
			</div>
			<div style="float:left;width:100%;margin-top:4%">
				<img style="width:50%;" src="<?php echo $dynamic['move_smart'] ?>" alt="movesmarth">	
			</div>
			<div style="float:left;width:100%;margin-top:4%">
				<div style="float:left;width:13%;">
					<div style=" text-align:center;background: <?php echo $dynamic['company_color'] ?> none repeat scroll 0 0;border-radius: 50%;color: #fff;float: left;height: 100px;text-align: center;width: 100px;">
						<h1 style="font-size: 30px;margin-top:28px;"><?php echo $movesmart_new['fitness']; ?></h1>
					</div>
					<div style="text-align:center;color:#646464;">
						<p style="margin:0;">Conditie</p>
					</div>
				</div>
				<div style="margin-left:3%;float:left;width:84%;color:#646464;">
				<p><?php echo $movesmart_new['fitness_desc']; ?></p>
				</div>
			</div>
						<div style="float:left;width:100%;margin-top:2%">
				<div style="float:left;width:13%;">
					<div style=" text-align:center;background: <?php echo $dynamic['company_color'] ?> none repeat scroll 0 0;border-radius: 50%;color: #fff;float: left;height: 100px;text-align: center;width: 100px;">
						<h1 style="font-size: 30px;margin-top:28px;"><?php echo $movesmart_new['flexbility'];?></h1>
					</div>
					<div style="text-align:center;color:#646464;">
						<p style="margin:0;">Lenigheid</p>
					</div>
				</div>
				<div style="margin-left:3%;float:left;width:84%;color:#646464;">
				<p><?php echo $movesmart_new['flexbility_desc'];?></p>
				</div>
			</div>
		</div>
	</div>
	</div>
</body>
</html>