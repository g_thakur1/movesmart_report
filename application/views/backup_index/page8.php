<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Move Smart</title>
    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet">
    <style>
    	body{font-family: 'Open Sans', sans-serif;}
    </style>
</head>
<body>
	<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div style="border: 1px solid #bb61b7; border-radius: 10px;float: left;width: 220px;">
				<img style="width:10%;" style="width: 100%;" src="images/innerlogo.png" alt="Logo">
			</div>
			<div style=" text-align: center; width: 176px;border: 1px solid #bb61b7;border-radius: 10px;float: right;padding: 10px 7px;">
				<h2 style="font-size: 13px;margin: 0 0 4px;font-family: 'Open Sans', sans-serif;font-weight:bold;color:#646464;"><?php //echo $club->club_name ;?>FeelGood consult</h2>
				<p style="font-size: 13px;margin: 0 0 3px;font-family: 'Open Sans', sans-serif;font-weight:300;color:#646464;"><?php echo $user_detail->first_name; ?> &nbsp;&nbsp;<?php echo $user_detail->last_name; ?></p>
				<p style="font-size: 13px;margin: 0;font-family: 'Open Sans', sans-serif;color:#646464;">Testdatum <?php echo $latest_test_date; ?></p>
			</div>
						<div style="float:left;width:100%;margin-top:3%;border: 1px solid #bb61b7;border-radius: 10px;padding:3% 0">
						<h1 style="color:#bb61b7;font-weight:300;"><?php //echo $club->club_name; ?>FeelGood score</h1>
				<div style="float:left;width:40%;text-align:center;">
				<div style="margin-top:7%;">
					<img src="chart/<?php echo $score_image; ?>"> 
					
					</div>
				</div>
				<div style="float:left;width:55%;color:#646464;">
				<h1 style="margin-top:0px;color:#bb61b7;font-weight:300;">Jouw resultaat</h1>
<p style="color:#646464;">Je zelfscore is samengesteld uit jouw antwoorden op de vragen over bewegen, eten en ontspannen en de gegevens uit
je MOVE diary. Aan de score kun je in 1 oogopslag zien hoe je
ervoor staat en waar je aan kunt werken. Het systeem is simpel: groen is gezond, oranje betekent dat er aandacht nodig
is en rood is een signaal dat er echt iets moet gebeuren. Je
coach gaat je daar bij begeleiden.</p>
<p style="color:#646464;">Naast deze score hebben we ook een aantal metingen gedaan. De resultaten daarvan, je MOVE-, EAT- en MIND level,
kun je in dit rapport teruglezen. Als je de zelfscore vergelijkt
met deze levels, zie je of je huidige activiteiten effectief zijn en
het gewenste resultaat hebben opgeleverd.</p>
				</div>
			</div>
				<div style="float:left;width:100%;margin-top:5%">
				<h2 style="color:#bb61b7;font-weight:300; margin:0px;">Credits</h2>
				<p style="margin-top:0;">
				Als beloning voor het beginnen met het programma krijg je de credits die je hebt gespaard met je eerste stappen
‘in the pocket’. Blijkt bij het volgende consult dat je het doel hebt gehaald en een level gestegen bent, dan komen
de credits die je de komende 12 weken spaart daar bij.
				</p>
				
				</div>

			
			<div style="float:left;width:100%;margin-top:3%">
				<div style="float:left;width:13%;">
					<?php if (file_exists($coach_image)) { ?>
					<img style="width:60%;" src="http://movesmart.offshoresolutions.nl/backoffice/images/uploads/movesmart/profile/<?php echo $coach_image;?>"> 
					<?php }
					else{ ?>
						<img style="width:60%;" src="<?php echo base_url(); ?>/images/coach.png"> 
				<?php	} ?>
				</div>
				<div style="margin-left:3%;float:left;width:84%;color:#000;">
				<h1 style="color:#bb61b7;margin-top:0;margin-bottom:10px;font-weight:300;">Veel succes en trainingsplezier!</h1>
				<p style="margin:0;color:#646464;"><?php echo $coach_detail->first_name.' '.$coach_detail->last_name; ?></p>
				<p style="margin:0;color:#646464;"><?php //echo $club->club_name; ?>FeelGood  Coach</p>
				</div>
			</div>

		</div>
	</div>
	</div>
</body>
</html>