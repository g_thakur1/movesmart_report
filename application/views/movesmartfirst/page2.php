<!DOCTYPE html>
<html lang="en">
  <head>
  	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Move Smart</title>
    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
     <style>
    body{font-family: 'Open Sans', sans-serif;}
    </style>
</head>
<body>
	<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div style="border: 1px solid #0095ac; border-radius: 10px;float: left;width: 220px;">
				<img style="width:10%;" style="width: 100%;" src="moveimage/innerlogo.png" alt="Logo">
			</div>
			<div style=" text-align: center; width: 176px;border: 1px solid #0095ac;border-radius: 10px;float: right;padding: 10px 7px;">
				<h2 style="font-size: 13px;margin: 0 0 4px;font-family: 'Open Sans', sans-serif;font-weight:bold;color:#646464;">Vitality consult</h2>
				<p style="font-size: 13px;margin: 0 0 3px;font-family: 'Open Sans', sans-serif;font-weight:300;color:#646464;"><?php echo $user_detail->first_name; ?> &nbsp;&nbsp;<?php echo $user_detail->last_name; ?></p>
				<p style="font-size: 13px;margin: 0;font-family: 'Open Sans', sans-serif;color:#646464;">Testdatum <?php echo $latest_test_date; ?></p>
			</div>
			<div style=" border: 1px solid #0095ac;border-radius: 10px;float: left;margin-top: 4%;padding: 1% 2%;width: 96%;">
				<div style="color:#0095ac;"><p style="margin: 0 0 0 0;font-size:25px;font-weight:300;">Beste <?php echo $user_detail->first_name; ?>,</p></div>
				<p style="color:#646464" >Op <?php echo $latest_test_date; ?> heb je een <?php //echo $club->club_name; ?>MoveSmart consult gedaan bij je <?php //echo $club->club_name; ?>MoveSmart coach. Daar is gemeten hoe het er voor staat met jouw leefstijl en vitaliteit, besproken wat er beter kan en hoe en hebben jullie samen doelen gesteld. De resultaten van de ‘harde metingen’ bepalen je MOVE, EAT en MIND level. Op basis van die levels, jouw antwoorden op vragen over je leefstijl en je MOVE diary krijg je een persoonlijk advies hoe jij aan je vitaliteit kunt werken. Nu kun je van start met het programma. Om je extra te motiveren krijg je van ons credits zodra je een doelstelling hebt gehaald. Zet hem op! </p>			
			</div>
			<div style="text-align:center;width:100%;float:left;margin-top:2%">
				<img style="width:10%;" style="width:10%;" src="moveimage/imagecircle1.png" alt="imagecircle1">
				<img style="width:10%;" src="moveimage/imagecircle7.png" alt="imagecircle7">
				<img style="width:10%;" src="moveimage/imagecircle5.png" alt="imagecircle5">
				<img style="width:10%;" src="moveimage/imagecircle4.png" alt="imagecircle4">
				<img style="width:10%;" src="moveimage/imagecircle3.png" alt="imagecircle3">
				<img style="width:10%;" src="moveimage/imagecircle2.png" alt="imagecircle2">
				<img style="width:10%;" src="moveimage/imagecircle6.png" alt="imagecircle6">
			</div>
			<div style="float:left;width:100%;margin-top:20%">
				<img style="width:50%;" src="moveimage/movesmart.png" alt="movesmarth">	
			</div>
			<div style="float:left;width:100%;margin-top:4%">
				<div style="float:left;width:13%;">
					<div style=" text-align:center;background: #0095ac none repeat scroll 0 0;border-radius: 50%;color: #fff;float: left;height: 100px;text-align: center;width: 100px;">
						<h1 style="font-size: 30px;margin-top:28px;"><?php echo $movesmart['fitness']; ?></h1>
					</div>
					<div style="text-align:center;color:#646464;">
						<p style="margin:0;">Conditie</p>
					</div>
				</div>
				<div style="margin-left:3%;float:left;width:84%;color:#646464;">
				<p><?php echo $movesmart['fitness_desc']; ?></p>
				</div>
			</div>
		</div>
	</div>
	</div>
</body>
</html>