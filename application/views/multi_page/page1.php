<!DOCTYPE html>
<html lang="en">
  <head>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Move Smart</title>
    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
<style>
.footer_image > img {float: left;width: 100%;}
.col{-md-12 {float: left;padding-bottom: 135px;position: relative;width: 100%;}
body{font-family: 'Open Sans', sans-serif;}
</style>
  </head>
  <body>

    <div class="container">
	<div class="row">
		<div class="col-md-12">
        	<div style="float:left;width:100%;text-align:center;" class="logo">
				<img style="width:40%;" src="images/logo.png" alt="logo">
            </div>
			<div class="banner_image" style="float:left;width:100%;">
            	<img style="width:100%;" src="images/banner.png" alt="banner">
            	<div style="font-weight:300;font-family:'Open Sans',sans-serif;margin: -72px 0 0;bottom:0;opacity: 0.7;color:#fff;background: rgb(162, 101, 145);text-align:center;width:100%;" class="color-div"><p style="margin: 0;
    padding: 15px 0;font-size:32px;">FeelGood consult</p></div>
            </div>
            <div class="user-info" style="margin:0 auto ;width:40%;">
            	<div class="user-box" style="text-align: center;padding: 10px 20px; border:1px solid #BB61B7;text-align::center;border-radius: 9px;float: left; width: 100%;margin-top: 60px;margin-bottom: 60px;">
                	<p style="color: #c161af;font-size: 25px;margin:10px;font-family: 'Open Sans', sans-serif;">Rapport</p>
                    <h2 style="color:#595B60;font-size:18px;margin:10px;font-family: 'Open Sans', sans-serif;font-weight:300;"><?php echo $user_detail->first_name; ?> &nbsp;&nbsp;<?php echo $user_detail->last_name; ?></h2>
                    <p style="color:#646464;font-size:13px;margin:10px;font-family: 'Open Sans', sans-serif;">Testdatum <?php echo $latest_test_date; ?></p>
                </div>
            </div>

		</div>
	</div>
</div>
 </body>
</html>