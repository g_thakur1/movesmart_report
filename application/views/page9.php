<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Move Smart</title>
    <meta name="description" content="">
    <meta name="author" content="">
     <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet">
    <style>
    	body{font-family: 'Open Sans', sans-serif;}
    </style>

</head>
<body >
	<div class="container">
	<div class="row">
		<?php 
			// if($company == 'feelgood') {
			// 	$dynamic = array();
	  //           $dynamic['company_color']	= '#bb61b7';
	  //           $dynamic['inner_logo']   	= 'images/innerlogo.png';
	  //           $dynamic['company_name']   = 'FeelGood';
	  //           $dynamic['bottomcircle']  	= 'images/bottomcircle.png';
	  //           $dynamic['bottomcircle1']  = 'images/bottomcircle1.png';
	  //           $dynamic['bottomcircle2']  = 'images/bottomcircle2.png';
	  //           $dynamic['bottomcircle3']  = 'images/bottomcircle3.png';
	  //           $dynamic['bottomcircle4']  = 'images/bottomcircle4.png';
	  //           $dynamic['level']   		= base_url(). 'images/movesmart_level' . $movesmart['movesmart_total_level'] . '.png';
			// } elseif ($company == 'movesmart') {
			// 	$dynamic = array();
	  //           $dynamic['company_color']  = '#0095ac';
	  //           $dynamic['inner_logo']   	= 'moveimage/innerlogo.png';
	  //           $dynamic['company_name']   = 'MoveSmart';
	  //           $dynamic['bottomcircle']  	= 'moveimage/bottomcircle.png';
	  //           $dynamic['bottomcircle1']  = 'moveimage/bottomcircle1.png';
	  //           $dynamic['bottomcircle2']  = 'moveimage/bottomcircle2.png';
	  //           $dynamic['bottomcircle3']  = 'moveimage/bottomcircle3.png';
	  //           $dynamic['bottomcircle4']  = 'moveimage/bottomcircle4.png';
	  //           $dynamic['level']   		= base_url(). 'moveimage/movesmart_level' . $movesmart['movesmart_total_level'] . '.png';
			// }
		?>
		<div class="col-md-12">
			<div style="border: 1px solid <?php echo $dynamic['company_color'] ?>; border-radius: 10px;float: left;width: 220px;">
				<img style="width:10%;" style="width: 100%;" src="<?php echo $dynamic['inner_logo'] ?>" alt="Logo">
			</div>
			<div style=" text-align: center; width: 176px;border: 1px solid <?php echo $dynamic['company_color'] ?>;border-radius: 10px;float: right;padding: 10px 7px;">
				<h2 style="font-size: 13px;margin: 0 0 4px;font-family: 'Open Sans', sans-serif;font-weight:bold;color:#646464;"><?php echo $dynamic['company_name'] ;?> consult</h2>
				<p style="font-size: 13px;margin: 0 0 3px;font-family: 'Open Sans', sans-serif;font-weight:300;color:#646464;"><?php echo $user_detail->first_name; ?> &nbsp;&nbsp;<?php echo $user_detail->last_name; ?></p>
				<p style="font-size: 13px;margin: 0;font-family: 'Open Sans', sans-serif;color:#646464;">Testdatum <?php echo $latest_test_date; ?></p>
			</div>
			<div style="float:left;width:100%;">
				<h3 style="float:left;color:<?php echo $dynamic['company_color'] ?>;margin:10px 0 0 0;">Trainingsschema</h3>
			</div>
			<div style="border: 1px solid <?php echo $dynamic['company_color'] ?>; border-radius: 10px;float: left;width: 100%;">
				<div style="float: left;width: 39%;padding:5px 0 0 5px">
					<div style="float: left;width: 50%;">
						<p>Fitheidsniveau:</p>
					</div>
					<div style="float: left;width: 50%;">
						<img style="width:60%;" src="<?php echo $dynamic['level']['page9'] ?>"> 
					</div>
				</div>
				<div style="float: right;width: 45%;">
					<div style="float: left;width: 100%;margin: 10px 0 5px 0;">
						<p style="float: left;width:150px;margin: 0;">Persoonlijke doelstelling:</p>
						<p style="width:150px; float: left;border: 1px solid <?php echo $dynamic['company_color'] ?>;border-radius: 15px;margin: 0 0 0 15px;padding: 5px;"><?php echo isset($goal)? $goal : 0;?></p>
					</div>
					<!--div style="float: left;width: 100%;margin: 10px 0 5px 0;">
						<p style="float: left;width:150px;margin: 0;">Medische beperking:</p>
						<p style="width:150px; float: left;border: 1px solid <?php echo $dynamic['company_color'] ?>;border-radius: 15px;margin: 0 0 0 15px;padding: 5px;">Geen</p>
					</div-->
					<div style="float: left ;width: 100%;margin: 10px 0 5px 0;">  
						<p style="float: left;width:150px;margin: 0;">IAND - HF:</p>
						<p style="width:150px; float: left;border: 1px solid <?php echo $dynamic['company_color'] ?>;border-radius: 15px;margin: 0 0 0 15px;padding: 5px;"><?php echo isset($target_credits['iant_hr'])? $target_credits['iant_hr'] : 0;?></p>
					</div>
					<div style="float: left;width: 100%;margin: 10px 0 5px 0;">
						<p style="float: left;width:150px;margin: 0;">IAND - P</p>
						<p style="width:150px; float: left;border: 1px solid <?php echo $dynamic['company_color'] ?>;border-radius: 15px;margin: 0 0 0 15px;padding: 5px;"><?php echo isset($target_credits['iant_p'])? $target_credits['iant_p'] : 0;?> Watt</p>
					</div>
				</div>
			</div>
			<div style="border: 1px solid <?php echo $dynamic['company_color'] ?>; border-radius: 10px;float: left;width: 94%;margin-top:1%;padding:0 2% 2% 2%">
				<h3 style="width:100%;float:left;color:<?php echo $dynamic['company_color'] ?>;margin:10px 0 0 0;">Trainingswaardes</h3>
				<table style="border-collapse:collapse;width: 100%;text-align:center;">
				<tr>
					<th style="width: 25%; border-bottom: 1px solid <?php echo $dynamic['company_color'] ?>;padding:5px 0;">% IAND P</th>
					<th style="width: 25%; border-bottom: 1px solid <?php echo $dynamic['company_color'] ?>;padding:5px 0;">Doel </th>
					<th style="width: 25%; border-bottom: 1px solid <?php echo $dynamic['company_color'] ?>;padding:5px 0;">Fietsen HF</th>
					<th style="width: 25%; border-bottom: 1px solid <?php echo $dynamic['company_color'] ?>;padding:5px 0;"> Watt</th>
				</tr>
				<tr>
					<td style="width: 25%;padding:5px 0;">-70%</td>
					<td style="width: 25%;padding:5px 0;">Herstel </td>
					<td style="width: 25%;padding:5px 0;">-<?php echo $cycle_heart_zone_70; ?></td>
					<td style="width: 25%;padding:5px 0;">-<?php echo $iand_70; ?></td>
				</tr>
				<tr>
					<td style="width: 25%;padding:5px 0;">70%-80%</td>
					<td style="width: 25%;padding:5px 0;">Vetverbranding</td>
					<td style="width: 25%;padding:5px 0;"><?php echo $cycle_heart_zone_70; ?>-<?php echo $cycle_heart_zone_80; ?></td>
					<td style="width: 25%;padding:5px 0;"><?php echo $iand_70; ?>-<?php echo $iand_80; ?></td>
				</tr>
				<tr style="">
					<td style="color:#fff;padding:5px 0;width: 25%;background:#AC41A1;border-bottom-left-radius:10px!important;border-top-left-radius:10px!important;">80%-90%</td>
					<td style="color:#fff;padding:5px 0;width: 25%;background:#AC41A1;">Basisuithouding</td>
					<td style="color:#fff;padding:5px 0;width: 25%;background:#AC41A1;"><?php echo $cycle_heart_zone_80; ?>-<?php echo $cycle_heart_zone_90; ?></td>
					<td style="color:#fff;padding:5px 0;width: 25%;background:#AC41A1;border-top-right-radius:10px!important;border-bottom-right-radius: 10px!important;"><?php echo $iand_80; ?>-<?php echo $iand_90; ?></td>
				</tr>
				<tr>
					<td style="padding:5px 0;width: 25%;">90%-97%</td>
					<td style="padding:5px 0;width: 25%;">Tempo /weerstand</td>
					<td style="padding:5px 0;width: 25%;"><?php echo $cycle_heart_zone_90; ?>-<?php echo $cycle_heart_zone_97; ?></td>
					<td style="padding:5px 0;width: 25%;"><?php echo $iand_90; ?>-<?php echo $iand_97; ?></td>
				</tr>
				</table>
			</div>
			<div style="border: 1px solid <?php echo $dynamic['company_color'] ?>; border-radius: 10px;float: left;width: 94%;margin-top:2%;padding:0 2% 2% 2%">
			<h3 style="width:100%;float:left;color:<?php echo $dynamic['company_color'] ?>;margin:10px 0 0 0;">Te behalen credits</h3>
				<table style="width: 100%;text-align:center;">
					<tr>
						<td style="width: 9%;">Weken</td>
						<td style="width: 7%;padding:5px 0"><p style="background: <?php echo $dynamic['company_color'] ?> none repeat scroll 0 0;    border-radius: 18px;color: #fff;margin: 0;padding: 4px 5px;width: 100px;">1</p></td>
						<td style="width: 7%;padding:5px 0"><p style="background: <?php echo $dynamic['company_color'] ?> none repeat scroll 0 0;    border-radius: 18px;color: #fff;margin: 0;padding: 4px 5px;width: 20px;">2</p></td>
						<td style="width: 7%;padding:5px 0"><p style="background: <?php echo $dynamic['company_color'] ?> none repeat scroll 0 0;    border-radius: 18px;color: #fff;margin: 0;padding: 4px 5px;width: 20px;">3</p></td>
						<td style="width: 7%;padding:5px 0"><p style="background: <?php echo $dynamic['company_color'] ?> none repeat scroll 0 0;    border-radius: 18px;color: #fff;margin: 0;padding: 4px 5px;width: 20px;">4</p></td>
						<td style="width: 7%;padding:5px 0"><p style="background: <?php echo $dynamic['company_color'] ?> none repeat scroll 0 0;    border-radius: 18px;color: #fff;margin: 0;padding: 4px 5px;width: 20px;">5</p></td>
						<td style="width: 7%;padding:5px 0"><p style="background: <?php echo $dynamic['company_color'] ?> none repeat scroll 0 0;    border-radius: 18px;color: #fff;margin: 0;padding: 4px 5px;width: 20px;">6</p></td>
						<td style="width: 7%;padding:5px 0"><p style="background: <?php echo $dynamic['company_color'] ?> none repeat scroll 0 0;    border-radius: 18px;color: #fff;margin: 0;padding: 4px 5px;width: 20px;">7</p></td>
						<td style="width: 7%;padding:5px 0"><p style="background: <?php echo $dynamic['company_color'] ?> none repeat scroll 0 0;    border-radius: 18px;color: #fff;margin: 0;padding: 4px 5px;width: 20px;">8</p></td>
						<td style="width: 7%;padding:5px 0"><p style="background: <?php echo $dynamic['company_color'] ?> none repeat scroll 0 0;    border-radius: 18px;color: #fff;margin: 0;padding: 4px 5px;width: 20px;">9</p></td>
						<td style="width: 7%;padding:5px 0"><p style="background: <?php echo $dynamic['company_color'] ?> none repeat scroll 0 0;    border-radius: 18px;color: #fff;margin: 0;padding: 4px 5px;width: 20px;">10</p></td>
						<td style="width: 7%;padding:5px 0"><p style="background: <?php echo $dynamic['company_color'] ?> none repeat scroll 0 0;    border-radius: 18px;color: #fff;margin: 0;padding: 4px 5px;width: 20px;">11</p></td>
						<td style="width: 7%;padding:5px 0"><p style="background: <?php echo $dynamic['company_color'] ?> none repeat scroll 0 0;    border-radius: 18px;color: #fff;margin: 0;padding: 4px 5px;width: 20px;">12</p></td>
						</tr>
					<tr>
						<td style="width: 9%;">Credits</td>
						<td style="width: 7%;"><?php echo $week_per_credits['week_1']->f_points;  ?></td>
						<td style="width: 7%;"><?php echo $week_per_credits['week_2']->f_points;  ?></td>
						<td style="width: 7%;"><?php echo $week_per_credits['week_3']->f_points;  ?></td>
						<td style="width: 7%;"><?php echo $week_per_credits['week_4']->f_points;  ?></td>
						<td style="width: 7%;"><?php echo $week_per_credits['week_5']->f_points;  ?></td>
						<td style="width: 7%;"><?php echo $week_per_credits['week_6']->f_points;  ?></td>
						<td style="width: 7%;"><?php echo $week_per_credits['week_7']->f_points;  ?></td>
						<td style="width: 7%;"><?php echo $week_per_credits['week_8']->f_points;  ?></td>
						<td style="width: 7%;"><?php echo $week_per_credits['week_9']->f_points;  ?></td>
						<td style="width: 7%;"><?php echo $week_per_credits['week_10']->f_points;  ?></td>
						<td style="width: 7%;"><?php echo $week_per_credits['week_11']->f_points;  ?></td>
						<td style="width: 7%;"><?php echo $week_per_credits['week_12']->f_points;  ?></td>
					</tr>
				</table>
			</div>
			<div style="border: 1px solid <?php echo $dynamic['company_color'] ?>; border-radius: 10px;float: left;width: 94%;margin-top:2%;padding:0 2% 2% 2%">
				<h1 style="width:100%;float:left;color:<?php echo $dynamic['company_color'] ?>;margin:10px 0 0 0;">Credits per activiteit</h1>
				<table style="width: 100%;text-align:center;" cellspacing="10">
					<tr>
						<td style="width: 20%;">
							<div><img style="width: 75px;" src="<?php echo $dynamic['bottomcircle'] ?>"></div>
						</td>
						<td style="width: 20%;">
							<p><img style="width: 75px;" src="<?php echo $dynamic['bottomcircle1'] ?>"></p>
						</td>
						<td style="width: 20%;">
							<p><img style="width: 75px;"  src="<?php echo $dynamic['bottomcircle2'] ?>"></p>
						</td>
						<td style="width: 20%;">
							<p><img style="width: 75px;" src="<?php echo $dynamic['bottomcircle3'] ?>"></p>
						<td style="width: 20%;">
							<p><img style="width: 75px;"  src="<?php echo $dynamic['bottomcircle4'] ?>"></p>
						</td>
					</tr>
					<tr>
						<td style="width: 20%;">Rennen</td>
						<td style="width: 20%;">Fietsen MTB</td>
						<td style="width: 20%;">Zwemmen</td>
						<td style="width: 20%;">Wandelen</td>
						<td style="width: 20%;">Roeien</td>
					</tr>
					<tr>
					<td style="width: 20%;border: 1px solid <?php echo $dynamic['company_color'] ?>; border-radius: 10px;font-size:10px;float: left;"><?php echo $hr_run['points_rate'];?> credits<br>per 10 minuten<br>HSZ: <?php echo $hr_run['hr_zone_a']; ?></td>
					<td style="width: 20%;border: 1px solid <?php echo $dynamic['company_color'] ?>; border-radius: 10px;font-size:10px;float: left;"><?php echo $hr_cycle['points_rate'];?> credits<br>per 10 minuten<br>HSZ: <?php echo $hr_cycle['hr_zone_a']; ?></td>
					<td style="width: 20%;border: 1px solid <?php echo $dynamic['company_color'] ?>; border-radius: 10px;font-size:10px;float: left;"><?php echo $hr_swim['points_rate'];?> credits<br>per 10 minuten<br>HSZ: <?php echo $hr_swim['hr_zone_a']; ?></td>
					<td style="width: 20%;border: 1px solid <?php echo $dynamic['company_color'] ?>; border-radius: 10px;font-size:10px;float: left;"><?php echo $hr_walk['points_rate'];?> credits<br>per 10 minuten<br>HSZ: <?php echo $hr_walk['hr_zone_a']; ?></td>
					<td style="width: 20%;border: 1px solid <?php echo $dynamic['company_color'] ?>; border-radius: 10px;font-size:10px;float: left;"><?php echo $hr_row['points_rate'];?> credits<br>per 10 minuten<br>HSZ: <?php echo $hr_row['hr_zone_a']; ?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	</div>
</body>
</html>