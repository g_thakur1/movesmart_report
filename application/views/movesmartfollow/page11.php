<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Move Smart</title>
    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
        <style>
    	body{font-family: 'Open Sans', sans-serif;color: #646464;}
    </style>
</head>
<body>
	<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div style="border: 1px solid #0095ac; border-radius: 10px;float: left;width: 220px;">
				<img style="width:10%;" style="width: 100%;" src="moveimage/innerlogo.png" alt="Logo">
			</div>
			<div style=" text-align: center; width: 176px;border: 1px solid #0095ac;border-radius: 10px;float: right;padding: 10px 7px;">
				<h2 style="font-size: 13px;margin: 0 0 4px;font-family: 'Open Sans', sans-serif;font-weight:bold;color:#646464;"><?php //echo $club->club_name ;?>Vitality consult</h2>
				<p style="font-size: 13px;margin: 0 0 3px;font-family: 'Open Sans', sans-serif;font-weight:300;color:#646464;"><?php echo $user_detail->first_name; ?> &nbsp;&nbsp;<?php echo $user_detail->last_name; ?></p>
				<p style="font-size: 13px;margin: 0;font-family: 'Open Sans', sans-serif;color:#646464;">Testdatum <?php echo $latest_test_date; ?></p>
			</div>
<div style="float:left;width:100%;margin-top:3%;border: 1px solid #0095ac;border-radius: 10px;padding:3%">
	<h1 style="margin-top:0px; color:#0095ac;font-weight:300;">EAT level</h1>
	<div style="float:left;width:45%;text-align:center;">
		<div style="margin-top:2%;">
		<p style="margin:0;">Vorige Testdatum </p>
		<p style="margin:0;"><?php echo $eat_mind_old['old_test_date']; ?></p>
			<img style="width:50%"; src="moveimage/movesmart_level<?php echo $eat_mind_old['total_eat_level'];?>.png"> 
		</div>
	</div>
	<div style="float:left;width:45%;text-align:center;">
		<div style="margin-top:2%;">
		<p style="margin:0;">Laatste Testdatum </p>
		<p style="margin:0;"><?php echo $latest_test_date; ?></p>
			<img style="width:50%"; src="moveimage/movesmart_level<?php echo $eat_new['eat_total_level'];?>.png"> 
		</div>
	</div>
	
	<div style="float:left;width:100%;">
		<p><?php echo $eat_comparision; ?></p>

	</div>
</div>							
		</div>
	</div>
	</div>
</body>
</html>