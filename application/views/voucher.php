<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div class="main_con" style="float: left;margin: auto;width: 100%;">
	<div class="top_logo" style="float:left;width:100%;text-align: center;">
		<img src="images/logo.png" style="width:260px;padding:1px 0px;">
	</div>
			<div class="feel_good" style=" float: left;margin: 0 0 -93px;position: relative;text-align: center;top: 0;width: 100%;z-index: 555;">
			<h2 style="color: #fff;font-size: 28px;font-weight: 500;left: 40%;margin: 0;padding: 8px 0;position: absolute;">Feelgood cheque</h2>
		</div>
	<div class="banner" style="float:left;width:100%;position: relative;">

		<img src="http://movesmart.offshoresolutions.nl/movesmart_dev/reporting/image/banner.png" style="width:100%;">

	</div>
	<div class="feel_good_bttm" style="float: left;margin: -60px 0 0;position: relative;text-align: center;width: 100%;z-index: 555;">
			<h2 style="color: #fff;font-size: 28px;font-weight: 500;margin: 0;padding: 8px 0;"><?php echo $v_credit; ?> credits</h2>
		</div>
	<div class="janssen" style="float:left;width:100%; padding: 23px 0;position: relative;">
		<div class="pert_one" style="float:left;width:48%;">
			<div class="jans_cont" style="float:left;width:100%;text-align: center; border: 2px solid #de79a5;border-radius: 15px;padding: 17px 0;">
				<p style="color: #585067;font-size: 12px;margin: 0;">Deze cheque geeft</p>
				<h1 style="font-size: 14px;margin: 0;color: #585067;">Bert Janssen</h1>
				<p style="color: #585067;font-size: 12px;margin: 0;">recht op</p>
				<h1 style="font-size: 14px;margin: 0;color: #585067;"><?php echo $v_credit; ?></h1>    
				<p style="color: #585067;font-size: 12px;margin: 0;">credits</p>
			</div>
			<div class="code" style="background: #b2358b none repeat scroll 0 0;float: left;margin-top: 18px;padding: 8px 0;text-align: center;width: 100%;padding: 17px 0;">
				<h2 style="color: #fff;font-size: 12px;font-style: normal;font-weight: 100; margin: 0;">veiligheidscode</h2>
				<p style="color: #fff;font-size: 14px;margin: 0;">AK-23587-15B</p>
			</div>
			<div class="deze" style=" border: 2px solid #de79a5;border-radius: 15px;float: left;margin-top: 25px;padding: 4px 0px;width: 100%;">
				<p style="color: rgb(88, 80, 103); font-size: 12px; padding: 0 23px;margin: 10px 0;">Deze waardevoucher is in te leveren bij</p>
				<h2 style="color: rgb(88, 80, 103); font-size: 16px;padding: 0 22px;">Feelgood Waalwijk<br/>
				Elzenweg 23<br/>
				5144 MB Waalwijk</h2>
				<p style="color: rgb(88, 80, 103); font-size: 12px; padding: 0 22px; margin: 10px 0;">Voorwaarden:<br/>
				De cheque is niet overdraagbaar en orem ipsum dtetur
				adipiscing elit. Vivamus libero leo,
				pellentesque ornare, adipiscing vitae, rhoncus commodo,
				nulla. Fusce quis ipsum. Nulla neque massa,</p>
				<h2 class="spel" style="color: rgb(88, 80, 103); font-size: 12px;color: rgb(88, 80, 103);text-decoration: none;
					padding: 0 8px;"><a href="www.MOVESMART.company/spelregels">www.MOVESMART.company/spelregels</a></h2>
			</div>  
		</div>
		<div class="jans" style="float: left; width: 48%;margin-left: 12px;">
			<div class="spin" style=" border: 2px solid #de79a5;border-radius: 15px;float: left;padding: 8px 13px; width: 97%;">
				<p style="color: rgb(88, 80, 103); font-size: 12px; margin: 1px 0px;">
				Heeft u al kennisgemaakt met EATFRESH?
				Probeer eens wat anders, eet spinazie in de
				meest pure vorm. Vers, lekker en makkelijk:</p>
				<h2 style="color: #585067;font-size: 16px;margin: 7px 0;">Spinaziesalade</h2>
				<ul style="color: rgb(88, 80, 103);font-size:12px; margin: 3px 0;">Ingrediënten:</h2>
					<li>200 gr verse, gewassen spinazie</li>
					<li>100 gr zongedroogde tomaatjes</li>
					<li>200 gr fetakaas</li>
					<li>50 gr pijnboompitten</li>
				</ul>
				<p style="color: rgb(88, 80, 103); font-size: 12px;margin: 7px 0;">Bereiding:<br>
				Laat de zongedroogde tomaatjes uitlekken boven
				een schaaltje. Bewaar de opgevangen olie.
				Snijd de feta in stukjes. Rooster de pijnboompitten
				in een koekenpan totdat ze iets verkleuren.
				Doe de spinazie in een slaschaal en verdeel de
				kaas, tomaatjes en geroosterde pijnboompitten
				erover.</p>
				<p style="color: rgb(88, 80, 103); font-size: 12px;margin: 0;">Schenk de opgevangen olijfolie van de zongedroogde
				tomaatjes er overheen. Serveer direct.</p>
			</div>
			<div class="back_img" style="float: left; width: 100%; margin: 10px 8px;">
				<img src="image/moos.png" style="border-radius: 15px;";>
			</div>
		</div>
	</div>
</div>
</body>
</html>
